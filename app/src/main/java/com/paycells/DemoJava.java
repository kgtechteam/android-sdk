package com.paycells;

import androidx.multidex.MultiDexApplication;

import com.paycell.PaycellSdkApp;
import com.paycell.SdkRunMode;

public class DemoJava extends MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        PaycellSdkApp.init(getApplicationContext(), SdkRunMode.PROD);
    }
}
