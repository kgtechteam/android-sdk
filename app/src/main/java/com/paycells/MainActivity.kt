package com.paycells

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.webkit.CookieManager
import android.webkit.CookieSyncManager
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.paycell.PaycellSdkApp
import com.paycell.customviews.CVEditText
import com.paycell.extensions.toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val openSdkBtn: Button = findViewById(R.id.openSdkBtn)
        val exit: Button = findViewById(R.id.exit)

        val edit = findViewById<CVEditText>(R.id.edit)
        edit.setHintAndText(hint = "Telefon numaranızı girin")
        openSdkBtn.setOnClickListener {
            otomatikgecis(edit.getText())
        }
        exit.setOnClickListener {
            PaycellSdkApp.logout()
            toast("Çıkış yaptınız")
        }
        otomatikgecis("5077994875")
        //  otomatikgecis("5355129551")
//clearCookies(this)
    }

    fun otomatikgecis(phone: String) {
        val intent = Intent(this, BipActivity::class.java)
        intent.putExtra("PHONE", phone)
        startActivity(intent)

    }

    @SuppressWarnings("deprecation")
    fun clearCookies(context: Context?) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null)
            CookieManager.getInstance().flush()
        } else if (context != null) {
            val cookieSyncManager = CookieSyncManager.createInstance(context)
            cookieSyncManager.startSync()
            val cookieManager: CookieManager = CookieManager.getInstance()
            cookieManager.removeAllCookie()
            cookieManager.removeSessionCookie()
            cookieSyncManager.stopSync()
            cookieSyncManager.sync()
        }
    }

}