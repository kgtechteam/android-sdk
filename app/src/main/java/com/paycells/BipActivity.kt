package com.paycells

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.paycell.ui.sendmoney.DetailFragment

class BipActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_frame)
        fragmentReplace()

    }
    fun fragmentReplace() {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(com.paycellsdk.R.id.framelayout, DetailFragment.newInstance(intent.getStringExtra("PHONE")!!)).commitAllowingStateLoss()
    }
}