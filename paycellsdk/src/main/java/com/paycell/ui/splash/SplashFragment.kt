package com.paycell.ui.splash

import android.view.View
import com.paycell.base.BindingFragment
import com.paycell.helper.IPaycellSdkListener
import com.paycell.remote.model.AuthState
import com.paycell.remote.model.base.GlobalData
import com.paycell.remote.model.init.InitRequest
import com.paycell.remote.model.init.InitResponse
import com.paycell.ui.dialog.FullScreenDialogFragment
import com.paycell.ui.onboardingloginstep.EntryPhoneNumberFragment.Companion.phoneNumber
import com.paycell.ui.onboardingloginstep.ILoginStepPresenter
import com.paycell.ui.onboardingloginstep.LoginStepViewModel
import com.paycell.ui.sendmoney.DetailFragment
import com.paycell.ui.sendmoney.DetailFragment.Companion.INTERNET
import com.paycell.ui.sendmoney.DetailFragment.Companion.LOGIN
import com.paycell.ui.sendmoney.DetailFragment.Companion.mDetailFrafment
import com.paycell.utils.HashGenerator
import com.paycell.utils.NetworkUtils.getConnectionType
import com.paycell.utils.PrefUtils
import com.paycell.utils.RequestHeaderManager
import com.paycellsdk.R
import com.paycellsdk.databinding.FragmentSplashBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class SplashFragment : BindingFragment<FragmentSplashBinding>(), ILoginStepPresenter,
    IPaycellSdkListener {

    private val viewModel by viewModel<LoginStepViewModel>()

    lateinit var mFragment: DetailFragment

    override val layoutBindId: Int
        get() = R.layout.fragment_splash

    override fun initPresenter() {
        viewModel.setPresenter(this)
    }

    companion object {
        var onResume = false
        fun newInstance(): SplashFragment {
            return SplashFragment().apply {}
        }
    }

    override fun initUI(view: View) {
        mFragment = mDetailFrafment
        requestInit()

        hideKeyboardFrom(requireContext(), requireView())//todo fragment
    }

    override fun initListener() {
      //  viewModel.getPresenter()?.showLoading()
    }

    override fun succesApiGateway() {
        // requestInit()
    }

    private fun requestInit() {

        var request = InitRequest(
            requestHeader = RequestHeaderManager.getRequestHeader(),
            connectionType = requireContext().getConnectionType(),
            hashData = "",
            language = "tr", forwardId = null
        )
        var hashGenerator = HashGenerator()
        var hash = hashGenerator.generate(request, PrefUtils.authToken)
        request.hashData = hash

        viewModel.getInit(request) {
           // viewModel.getPresenter()?.hideLoading()
            mFragment.calculatePage(INTERNET,"")
        }
    }

    override fun succes() {
        requestInit()
    }

    override fun initResponse(initResponse: InitResponse) {
        GlobalData.initResponse = initResponse
        PrefUtils.authToken = GlobalData.initResponse.authToken
        if (!DetailFragment().getPhone()
                .isNullOrEmpty() && !DetailFragment().newPhoneSet() && initResponse.authState == AuthState.WAITING_OTP_VALIDATION
        ) {
            phoneNumber = DetailFragment().getPhone().toString()
            GlobalData.initResponse.authState = AuthState.STEP_OTP
            startLogin("true")
        } else if (initResponse.authState == AuthState.WAITING_REGISTRATION) {
            viewModel.getAccount { startLogin("false") }
        } else if (initResponse.authState == AuthState.WAITING_PIN_VALIDATION) {
            viewModel.getAccount { startLogin("true") }
        } else {
            startLogin("true")
        }
    }

    fun startLogin(isLogin: String) {
      //  viewModel.getPresenter()?.hideLoading()
        mFragment.calculatePage(LOGIN, isLogin)
    }

}
