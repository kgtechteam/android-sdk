package com.paycell.ui.dialog

import android.os.Bundle
import com.paycell.base.BindingBaseDialogFragment
import com.paycell.ui.onboardingloginstep.ILoginStepPresenter
import com.paycell.ui.onboardingloginstep.LoginStepViewModel
import com.paycell.ui.sendmoney.DetailFragment
import com.paycell.ui.sendmoney.DetailFragment.Companion.mDetailFrafment
import com.paycellsdk.R
import com.paycellsdk.databinding.DialogSuspendedWarningBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class SuspendedDialogFragment : BindingBaseDialogFragment<DialogSuspendedWarningBinding>(),
    ILoginStepPresenter {


    private val viewModel by viewModel<LoginStepViewModel>()


    override val layoutBindId: Int
        get() = R.layout.dialog_suspended_warning

    override val fullScreen: Boolean
        get() = false

    override fun initPresenter() {
        viewModel.setPresenter(this)
    }

    companion object {
        fun newInstance(): SuspendedDialogFragment {
            return SuspendedDialogFragment().apply {
                var args = Bundle()
                arguments = args
            }
        }
    }

    override fun initUI() {
    }

    override fun initListener() {
        binding.exitBtnSuspendedWarning.setOnClickListener {
            viewModel.logout()
        }
        binding.renewPasswordBtnSuspendedWarning.setOnClickListener {
             ForgotPasswordDialogFragment.newInstance().show(parentFragmentManager, null)
        }
        binding.closeBtnSuspendedWarningIv.setOnClickListener {
            dismiss()
        }
    }

    override fun succesLogout() {
        mDetailFrafment.newPhoneSet(true)
        mDetailFrafment.calculatePage(DetailFragment.SPLASH, "")
        dismiss()
    }

}