package com.paycell.ui.dialog

import android.os.Bundle
import android.view.View
import com.paycell.base.BindingBottomSheetFragment
import com.paycell.extensions.hideKeyboard
import com.paycellsdk.R
import com.paycellsdk.databinding.DialogConditionTermsBinding

class ConditionAndTermsDialogFragment : BindingBottomSheetFragment<DialogConditionTermsBinding>() {

    override val layoutBindId: Int
        get() = R.layout.dialog_condition_terms

    companion object {
        fun newInstance(url: String?, title: String): ConditionAndTermsDialogFragment {
            return ConditionAndTermsDialogFragment().apply {
                var args = Bundle()
                args.putString("url", url)
                args.putString("title", title)
                arguments = args
                setStyle(STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)
            }
        }
    }

    override fun initUI(view: View) {
        activity?.hideKeyboard()
        binding.mainHeaderConditionTerms.text=requireArguments().getString("title")
        requireArguments().getString("url")?.let { binding.webView.initialize(it,{}) }

    }

    override fun initListener() {
        binding.closeBtnConditionTerms.setOnClickListener {
            this.dialog?.dismiss()
        }
    }
}