package com.paycell.ui.dialog

import android.content.DialogInterface
import android.os.Bundle
import com.paycell.base.BindingBaseDialogFragment
import com.paycell.extensions.openLink
import com.paycell.extensions.visible
import com.paycell.helper.IPaycellSdkListener
import com.paycell.ui.onboardingloginstep.ILoginStepPresenter
import com.paycell.ui.onboardingloginstep.LoginStepViewModel
import com.paycell.ui.sendmoney.DetailFragment
import com.paycell.utils.Constant
import com.paycellsdk.R
import com.paycellsdk.databinding.DialogKycBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class KycDialogFragment : BindingBaseDialogFragment<DialogKycBinding>(),
    ILoginStepPresenter {


    private val viewModel by viewModel<LoginStepViewModel>()

     override val layoutBindId: Int
        get() = R.layout.dialog_kyc

    override val fullScreen: Boolean
        get() = false

    override fun initPresenter() {
        viewModel.setPresenter(this)
    }

    companion object {
        fun newInstance(cancelable:Boolean): KycDialogFragment {
            return KycDialogFragment().apply {
                var args = Bundle()
                args.putBoolean("cancel",cancelable)
                isCancelable = false
                arguments=args
             }
        }
    }

    override fun initUI() {
        if (requireArguments().getBoolean("cancel")) {
            binding.closeBtnSuspendedWarningIv.visible()
        }
    }

    override fun initListener() {
        binding.renewPasswordBtnSuspendedWarning.setOnClickListener {
            requireActivity().openLink(Constant.KYC_DEEPLINK)
        }
        binding.closeBtnSuspendedWarningIv.setOnClickListener {
            requireDialog().dismiss()
            DetailFragment.mDetailFrafment.calculatePage(DetailFragment.DETAIL, "anasayfa")
        }


    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
       println("")
    }

}