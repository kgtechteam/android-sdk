package com.paycell.ui.dialog

import android.os.Bundle
import android.view.View
import com.paycell.base.BindingBaseDialogFragment
import com.paycell.extensions.makeLinks
import com.paycell.remote.model.ForgotPinRequest
import com.paycell.remote.model.base.GlobalData
import com.paycell.ui.onboardingloginstep.ILoginStepPresenter
import com.paycell.ui.onboardingloginstep.LoginStepViewModel
import com.paycell.utils.RequestHeaderManager
import com.paycellsdk.R
import com.paycellsdk.databinding.DialogForgotCheckPasswordBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class  PasswordCheckDialogFragment : BindingBaseDialogFragment<DialogForgotCheckPasswordBinding>(),ILoginStepPresenter{
    /***  E postanızı kontrol edin dialog ekranı**/

    private val viewModel by viewModel<LoginStepViewModel>()

    var globalData = GlobalData

    override val layoutBindId: Int
        get() = R.layout.dialog_forgot_check_password

    override val fullScreen: Boolean
        get() = false

    override fun initPresenter() {
        viewModel.setPresenter(this)
    }
    companion object {
        fun newInstance(): PasswordCheckDialogFragment {
            return PasswordCheckDialogFragment().apply {
                var args = Bundle()
                arguments = args
            }
        }
    }

    override fun initUI() {
        setSpannableDescription()

        binding.closeBtnForgotPassIv.setOnClickListener { dismiss() }

        forgotPinObserve()
    }

    override fun initListener() {
        binding.btnClose.setOnClickListener {
            dismiss()
        }
    }

    private fun setSpannableDescription() {
        var fullstring = "{mail} mailine şifre oluşturma linki gönderilmiştir. Mail size ulaşmadı mı?".replace("{mail}",globalData.getAccountResponse.maskedEmail)

        binding.descriptionForgotPassTv.text = fullstring

        binding.emailTekrargonder.text="Tekrar Gönder"

        binding.emailTekrargonder .makeLinks(
            Triple(
                "Tekrar Gönder",
                View.OnClickListener { },
                "Tekrar Gönder"
            ), context = requireContext(), isUnderlined = true
        )

    }
    fun forgotPinObserve() {
        val forgotPinRequest = ForgotPinRequest(requestHeader = RequestHeaderManager.getRequestHeader())
        viewModel.forgotPinRequest(forgotPinRequest)
    }

}