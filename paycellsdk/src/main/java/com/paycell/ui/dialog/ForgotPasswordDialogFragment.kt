package com.paycell.ui.dialog

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.paycell.base.BindingBaseDialogFragment
import com.paycell.extensions.makeLinks
import com.paycell.remote.model.ForgotPinRequest
import com.paycell.remote.model.base.GlobalData
import com.paycell.ui.onboardingloginstep.ILoginStepPresenter
import com.paycell.ui.onboardingloginstep.LoginStepViewModel
import com.paycell.utils.RequestHeaderManager
import com.paycellsdk.R
import com.paycellsdk.databinding.DialogForgotPasswordBinding
import org.koin.androidx.viewmodel.ext.android.viewModel


class ForgotPasswordDialogFragment :
    BindingBaseDialogFragment<DialogForgotPasswordBinding>(), ILoginStepPresenter {
    /***  Şifremi Unuttum dialog ekranı**/

    private val viewModel by viewModel<LoginStepViewModel>()

    var globalData = GlobalData

    override val layoutBindId: Int
        get() = R.layout.dialog_forgot_password

    override val fullScreen: Boolean
        get() = false

    override fun initPresenter() {
        viewModel.setPresenter(this)

    }
    companion object {
        fun newInstance(): ForgotPasswordDialogFragment {
            return ForgotPasswordDialogFragment().apply {
                var args = Bundle()
                arguments = args
            }
        }
    }

    override fun initUI() {
        setSpannableDescription()

        binding.closeBtnForgotPassIv.setOnClickListener { dismiss() }

        binding.emailTvForgotPass.text =  globalData.getAccountResponse.maskedEmail
        forgotPinObserve()
    }

    override fun initListener() {
        binding.sendEmailBtnForgotPass.setOnClickListener {
            val forgotPinRequest =
                ForgotPinRequest(requestHeader = RequestHeaderManager.getRequestHeader())
            viewModel.forgotPinRequest(forgotPinRequest)
        }
    }

    private fun setSpannableDescription() {
        binding.mainHeaderForgotPassTv.text = "Şifremi Unuttum"
        var fullstring =
            "Aşağıdaki adrese şifre sıfırlama için e-posta gönderilecektir. E-posta adresinizi değiştirmek için  {phone}’ı arayabilirsiniz."

        binding.descriptionForgotPassTv.text = fullstring

        binding.descriptionForgotPassTv.makeLinks(
            Triple(
                "{phone}",
                View.OnClickListener { callNumber() },
                " 0850 622 60 60"
            ), context = requireContext(), isUnderlined = true
        )
    }

    fun forgotPinObserve() {
        viewModel.forgotResponse.observe(this, Observer {
            dismiss()
            if (it.responseHeader.responseCode.equals("0")) {
              PasswordCheckDialogFragment.newInstance().show(parentFragmentManager, null)
            }

        })
    }

    fun callNumber() {
        val dialIntent = Intent(Intent.ACTION_DIAL)
        dialIntent.data = Uri.parse("tel:" + "0850 622 60 60")
        startActivity(dialIntent)
    }
}