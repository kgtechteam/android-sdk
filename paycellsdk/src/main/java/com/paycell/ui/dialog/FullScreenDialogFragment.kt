package com.paycell.ui.dialog

import android.os.Bundle
import android.view.View
import com.paycell.PaycellSdkApp
import com.paycell.base.BindingFragment
import com.paycell.helper.IPaycellSdkListener
import com.paycell.ui.onboardingloginstep.ILoginStepPresenter
import com.paycell.ui.onboardingloginstep.LoginStepViewModel
import com.paycell.utils.NetworkUtils
import com.paycellsdk.R
import com.paycellsdk.databinding.DialogInternetConnectionBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class FullScreenDialogFragment : BindingFragment<DialogInternetConnectionBinding>(),
    ILoginStepPresenter {


    private val viewModel by viewModel<LoginStepViewModel>()


    override val layoutBindId: Int
        get() = R.layout.dialog_internet_connection


    lateinit var iIPaycellSdkListener: IPaycellSdkListener

    override fun initPresenter() {
        viewModel.setPresenter(this)
    }

    companion object {
        fun newInstance(iIPaycellSdkListener: IPaycellSdkListener): FullScreenDialogFragment {
            return FullScreenDialogFragment().apply {
                var args = Bundle()
                arguments = args
                this.iIPaycellSdkListener = iIPaycellSdkListener
            }
        }
    }

    override fun initUI(view: View) {
    }

    override fun initListener() {
        binding.renewPasswordBtnSuspendedWarning.setOnClickListener {
            if (NetworkUtils.hasInternet(PaycellSdkApp.context)) {
                iIPaycellSdkListener.succes()
            }
        }
    }
}