package com.paycell.ui.dialog

import android.os.Bundle
import android.view.View
import com.paycell.base.BindingBaseDialogFragment
import com.paycell.extensions.ContactItemViewState
import com.paycell.extensions.ContactPopup
import com.paycell.extensions.formatedNumber
import com.paycell.helper.GenericAdapter
import com.paycell.helper.ListItemViewModel
import com.paycell.ui.onboardingloginstep.ILoginStepPresenter
import com.paycell.ui.sendmoney.DetailFragment
import com.paycellsdk.R
import com.paycellsdk.databinding.DialogChoosePhoneBinding

class ChoosePhoneDialogFragment : BindingBaseDialogFragment<DialogChoosePhoneBinding>(),
    ILoginStepPresenter {


    override val layoutBindId: Int
        get() = R.layout.dialog_choose_phone

    override val fullScreen: Boolean
        get() = false

    var name: String = ""
    var phones: ArrayList<ContactPopup> = arrayListOf()

    private val genericAdapter by lazy {
        GenericAdapter<ContactPopup>(R.layout.row_alotof_user_phone)
    }

    override fun initPresenter() {

    }

    lateinit var fragment: DetailFragment

    companion object {
        fun newInstance(
            name: String,
            phones: ContactItemViewState,
            fragment: DetailFragment
        ): ChoosePhoneDialogFragment {
            return ChoosePhoneDialogFragment().apply {
                var args = Bundle()
                this.name = name
                this.fragment=fragment
                phones.numberArray?.forEachIndexed { index, s ->
                    this.phones.add(ContactPopup(s.formatedNumber(), s))
                }
                arguments = args
            }
        }
    }

    override fun initUI() {
        binding.tvDecsription.text =
            "X kişisinin işleme devam etmek istediğiniz numarasını seçiniz.".replace("X", name)
        binding.rv.adapter = genericAdapter
        genericAdapter.addItems(phones)
    }

    override fun initListener() {
        listenerItem()
        binding.closeBtnEmailVerifyIv.setOnClickListener {
            dismiss()
        }
    }

    fun listenerItem() {
        genericAdapter.setOnListItemViewClickListener(object :
            GenericAdapter.OnListItemViewClickListener {
            override fun onClick(view: View, position: Int, model: ListItemViewModel) {
                var result = (model as ContactPopup)
                openDetailPage(result.serviceNumber, name)
            }
        })
    }

    fun openDetailPage(number: String, name: String) {
        this.dismiss()
        fragment.openDetailSendMoney(number,name,"")
    }
}