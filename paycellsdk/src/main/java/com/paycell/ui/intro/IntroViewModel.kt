package com.paycell.ui.intro

import androidx.annotation.DrawableRes
import com.paycell.base.BaseViewModel
import com.paycell.di.IDataManager
import com.paycellsdk.R


class IntroViewModel(dataManager: IDataManager) : BaseViewModel<IIntroPresenter>(dataManager) {
    class IntroModel(
        var title: String? = null,
        var subTitle: String? = null,
        @DrawableRes
        var drawable: Int
    )

    fun initIntroData(): ArrayList<IntroModel> {
        val items = arrayListOf<IntroModel>()

        val title1 = "Paycell'e Hoşgeldin"
        val subTitle1 = ""

        val title2 = "Tek hesapla her işlem"
        val subTitle2 = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy."

        val title3 = "Her yerde anında kullan"
        val subTitle3 = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy."

        val title4 = "Kullandıkça kazan"
        val subTitle4 = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy."

        items.add(IntroModel(title1, subTitle1, R.drawable.icpaycelllogo))
        items.add(IntroModel(title2, subTitle2, R.drawable.icstepone))
        items.add(IntroModel(title3, subTitle3, R.drawable.icsteptwo))
        items.add(IntroModel(title4, subTitle4, R.drawable.icstepthree))

        return items
    }
}