package com.paycell.ui.intro

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.paycellsdk.databinding.PagerIntroAdapterBinding

class IntroAdapter(
    val context: Context,
    private val items: List<IntroViewModel.IntroModel>
) : PagerAdapter() {

    override fun isViewFromObject(view: View, obj: Any): Boolean = view === obj

    override fun getCount(): Int = items.size

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) =
        container.removeView(obj as View?)

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater = LayoutInflater.from(context)
        val binding = PagerIntroAdapterBinding.inflate(inflater, container, false)
        binding.vm = items[position]
        container.addView(binding.root)
        return binding.root
    }

}