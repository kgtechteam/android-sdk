package com.paycell.ui.intro


import android.view.View
import com.paycell.base.BindingActivity
import com.paycell.extensions.launchActivity
import com.paycell.extensions.makeLinks
import com.paycell.extensions.onPageSelected
import com.paycell.extensions.setVectorForPreLollipop
import com.paycell.helper.ViewPagerTransformation
import com.paycell.ui.login.LoginFragment
import com.paycellsdk.R
import com.paycellsdk.databinding.ActivityIntroBinding
import org.koin.androidx.viewmodel.ext.android.viewModel


class IntroActivity : BindingActivity<ActivityIntroBinding>(), IIntroPresenter {

    private val viewModel by viewModel<IntroViewModel>()

    private var currentPage = 0
    private val lastPage = 4


    override val layoutBindId: Int
        get() = R.layout.activity_intro

    override fun initPresenter() {
        viewModel.setPresenter(this)
    }

    override fun initUI() {
        binding.vm = viewModel
        initAdapter()
    }

    private fun initAdapter() {
        val introAdapter = IntroAdapter(
            this,
            viewModel.initIntroData()
        )

        binding.vpIntro.adapter = introAdapter
        binding.vpIntro.setPageTransformer(true, ViewPagerTransformation())
        binding.indicatorIntro.setViewPager(binding.vpIntro)
        binding.btnNext.setBackgroundDrawable(setVectorForPreLollipop(R.drawable.back_aqua_outline))
        setPayCellUserBtn()
    }

    private fun setPayCellUserBtn() {
        val payCellUserString = "Paycell kullanıcısıyım"
        binding.tvIusingpaycell.text = payCellUserString
        binding.tvIusingpaycell.makeLinks(
            Triple(
                payCellUserString,
                View.OnClickListener { /**sayfayı aç**/ }, payCellUserString
            ), context = applicationContext, colorId = R.color.white, isUnderlined = true
        )
    }

    override fun initListener() {
        binding.vpIntro.onPageSelected { position ->
            currentPage = position
            binding.btnNext.text = "Üye Ol"
        }

        binding.btnNext.setOnClickListener {
             launchActivity<LoginFragment> { putExtra("isLogin", false) }
             // if (currentPage < lastPage && currentPage != 2) {
            //     binding.vpIntro.currentItem = currentPage + 1
            // } else {
            //     // go to auth
            // }
        }
        binding.tvIusingpaycell.setOnClickListener {
            launchActivity<LoginFragment> { putExtra("isLogin", true) }
            //  PrefUtils.firstRun=true
            //  if (isSupportedFaceOrFinger()) {
            //      launchActivity<RegisteredUserFingerLoginActivity> { }
            //      finish()
            //  } else {
            //      launchActivity<RegisteredUserPhoneAndPaswwordActivity> { }
            //      finish()

            //  }
        }


    }


}

