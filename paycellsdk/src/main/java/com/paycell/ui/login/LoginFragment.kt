package com.paycell.ui.login


import android.view.View
import android.view.WindowManager
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.paycell.base.BindingFragment
import com.paycell.customviews.CVToolbar
import com.paycell.remote.model.AuthState
import com.paycell.remote.model.base.GlobalData
import com.paycell.ui.onboardingloginstep.*
import com.paycell.ui.sendmoney.CardVerificationFragment
import com.paycell.ui.sendmoney.DetailFragment
import com.paycell.ui.sendmoney.DetailFragment.Companion.MAINPAGE
import com.paycell.ui.sendmoney.DetailFragment.Companion.mDetailFrafment
import com.paycellsdk.R
import com.paycellsdk.databinding.FragmentLoginBinding
import org.koin.androidx.viewmodel.ext.android.viewModel


class LoginFragment : BindingFragment<FragmentLoginBinding>(), ILoginStepPresenter {

    private val viewModel by viewModel<LoginStepViewModel>()


    var globalData = GlobalData
    var totalPage: Int = 6

    lateinit var mFragment: DetailFragment
    var isLoginChageFllow: Boolean = false
    var isCardValidationFllow: Boolean = false
    var passwordUpdate: Boolean = false
    var data: String = ""
    var isLogin: Boolean = false

    companion object {
        fun newInstance(data: String): LoginFragment {
            return LoginFragment().apply {
                this.data = data
                this.isLogin = data == "true"

            }
        }

        const val ENTRY_PHONE_NUMBER = 0
        const val VALIDATE_PHONE_NUMBER = 1
        const val CREATE_PASSWORD = 2
        const val CREATE_EMAIL_AND_CONDITION = 3
        const val ENTRY_PASSWORD = 4
        const val CARD_VALIDATION = 5
        var current_page = 0
        var isCardValidationVerify: Boolean = false
    }

    override val layoutBindId: Int
        get() = R.layout.fragment_login

    override fun initPresenter() {
        viewModel.setPresenter(this)
    }

    override fun initUI(view: View) {
        mFragment=mDetailFrafment
        binding.vm = viewModel
        initAdapter()
        viewModel.getClientPramas()
    }

    private fun initAdapter() {
        setUpAdapter()
    }

    fun setUpAdapter() {
        val pagerAdapter = ScreenSlidePagerAdapter(childFragmentManager)
        binding.vpLogin.adapter = pagerAdapter

     }

    override fun initListener() {
        //requireActivity().window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)

        binding.toolbar.backButton().setOnClickListener {
            calculateBackPage()
        }

        redirectWithAuthState()

    }

    fun changePagerPosition(position: Int) {
        current_page = position
        binding.vpLogin.currentItem = position
    }

    private inner class ScreenSlidePagerAdapter(fm: FragmentManager) :
        FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
        override fun getCount(): Int = totalPage

        override fun getItem(position: Int): Fragment {
            return pageCalculate(position)
        }
    }

    fun calculateBackPage() {
        if (current_page == CREATE_EMAIL_AND_CONDITION) {
            changePagerPosition(CREATE_PASSWORD)
        } else if ((current_page == CREATE_PASSWORD && isCardValidation())) {
            changePagerPosition(CARD_VALIDATION)
        } else if ((current_page == CREATE_PASSWORD && !isCardValidation()) || current_page == ENTRY_PASSWORD || current_page == ENTRY_PHONE_NUMBER || current_page == VALIDATE_PHONE_NUMBER || current_page == CARD_VALIDATION) {
            mFragment.calculatePage(MAINPAGE, "")
        }
    }

    fun pageCalculate(position: Int): Fragment {
        when (position) {
            ENTRY_PHONE_NUMBER -> return EntryPhoneNumberFragment.newInstance(this@LoginFragment)
            VALIDATE_PHONE_NUMBER -> return ValidatePhoneNumberFragment.newInstance(this@LoginFragment)
            CREATE_PASSWORD -> return CreatePasswordFragment.newInstance(this@LoginFragment)
            ENTRY_PASSWORD -> return EntryPasswordFragment.newInstance(this@LoginFragment)
            CREATE_EMAIL_AND_CONDITION -> return CreateEmailAndConditionFragment.newInstance(this@LoginFragment)
            else -> return CardVerificationFragment.newInstance(this@LoginFragment)
        }

    }

    // override fun onBackPressed() {  //todo fragment
    //     calculateBackPage()
    // }

    fun redirectWithAuthState() {
        if (globalData.initResponse.authState == null) {
            return
        }
        if (globalData.initResponse.allowMsisdnChange) {
            binding.vpLogin.currentItem = CREATE_PASSWORD
        } else {
            when (globalData.initResponse.authState) {
                AuthState.WAITING_CARD_VALIDATION -> {
                    isCardValidation(true)
                    changePagerPosition(CARD_VALIDATION)
                }
                AuthState.WAITING_REGISTRATION -> {
                    if (GlobalData.getAccountResponse.doCardValidation) {
                        isCardValidation(true)
                        changePagerPosition(CARD_VALIDATION)
                    } else
                        changePagerPosition(CREATE_PASSWORD)
                }
                AuthState.WAITING_EMAIL_VALIDATION -> {
                }
                AuthState.WAITING_OTP_VALIDATION -> {

                }
                AuthState.WAITING_PIN_VALIDATION -> {
                    changePagerPosition(ENTRY_PASSWORD)

                }
                AuthState.AUTHENTICATED -> {
                }
                AuthState.STEP_OTP -> {
                    changePagerPosition(VALIDATE_PHONE_NUMBER)
                }
            }
        }
    }

    fun isLoginFollow(): Boolean = data.equals("true")
    fun setIsLogin() {
        isLoginChageFllow = true
    }

    fun isCardValidation(state: Boolean? = null): Boolean {
        state?.let {
            isCardValidationFllow = it
        }
        return isCardValidationFllow
    }
    fun isPasswwordUpdate(state: Boolean? = null): Boolean {
        state?.let {
            passwordUpdate = it
        }
        return passwordUpdate
    }

    fun getIsLogin() = isLoginChageFllow

    fun backButtonVisiblity(visile: Boolean) {
        var view = requireView().findViewById<CVToolbar>(R.id.toolbar)
        if (visile) {
            view.backButtonVisible()
        } else {
            view.backButtonGone()
        }
    }
}


