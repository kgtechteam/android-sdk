package com.paycell.ui.sendmoney

import android.view.View
import androidx.fragment.app.Fragment
import com.paycell.base.BaseWebFragment
import com.paycell.base.BaseWebFragment.Companion.urlRefState
import com.paycell.base.BindingFragment
import com.paycell.extensions.phoneValidationList
import com.paycell.helper.IPaycellSdkListener
import com.paycell.remote.model.UrlRefState
import com.paycell.remote.model.base.GlobalData
import com.paycell.ui.dialog.FullScreenDialogFragment
import com.paycell.ui.login.LoginFragment
import com.paycell.ui.onboardingloginstep.ILoginStepPresenter
import com.paycell.ui.onboardingloginstep.LoginStepViewModel
import com.paycell.ui.splash.SplashFragment
import com.paycellsdk.R
import com.paycellsdk.databinding.FragmentDetailBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailFragment : BindingFragment<FragmentDetailBinding>(), ILoginStepPresenter,
    IPaycellSdkListener {


    private val viewModel by viewModel<LoginStepViewModel>()
    private var mIsLogin: Boolean = false
    override val layoutBindId: Int
        get() = R.layout.fragment_detail

    companion object {
        lateinit var mDetailFrafment: DetailFragment
        fun newInstance(phone: String): DetailFragment {
            mDetailFrafment = DetailFragment().apply {
                phoneNumber = phone
            }
            return mDetailFrafment
        }

        fun isInit() = this::mDetailFrafment.isInitialized
        internal var SPLASH = "SPLASH"
        internal var LOGIN = "LOGIN"
        internal var LOGIN_COMPLETED = "LOGIN_COMPLETED"
        internal var MAINPAGE = "MAINPAGE"
        internal var CONTACT = "CONTACT"
        internal var DETAIL = "DETAIL"
        internal var DETAIL_SENDMONEY_STEPONE = "DETAIL_SENDMONEY_STEPONE"
        internal var QR_SCAN = "QR_SCAN"
        internal var QR_TAB_PAGE = "QR_TAB_PAGE"
        internal var KAYDET = "KAYDET"
        internal var SIM_CHECK_PROGRESS = "SIM_CHECK_PROGRESS"
        internal var SIM_CHECK_DIFFERENT_MSISDN = "SIM_CHECK_DIFFERENT_MSISDN"
        internal var SIM_CHECK_WIFI = "SIM_CHECK_WIFI"
        internal var INTERNET = "INTERNET"
        internal var phoneNumber: String = ""
        var newPhone: Boolean = false

    }

    var current = ""
    override fun initPresenter() {
        viewModel.setPresenter(this)
    }

    override fun initUI(view: View) {
        if (!isInit()) {
            mDetailFrafment = DetailFragment()
        }
    }

    override fun initListener() {
        calculatePage(MAINPAGE, "")
    }

    fun calculatePage(nerden: String, data: String) {
        current = nerden
        when (nerden) {
            SPLASH -> {
                fragmentReplace(SplashFragment.newInstance())
            }
            LOGIN -> {
                fragmentReplace(LoginFragment.newInstance(data))
            }
            LOGIN_COMPLETED -> {
                thanLoginWhichPage(data)
            }
            MAINPAGE -> {
                fragmentReplace(MainPageFragment.newInstance())
            }
            CONTACT -> {
                fragmentReplace(ContactSelectFragment.newInstance())
            }
            DETAIL -> {
                fragmentReplace(DetailPageFragment.newInstance(data))
            }
            DETAIL_SENDMONEY_STEPONE -> {
                fragmentReplace(DetailPageFragment.newInstance(data))
            }
            QR_SCAN -> {
                fragmentReplace(QrScanFragment.newInstance(data))
            }
            QR_TAB_PAGE -> {
                fragmentReplace(QrCodeFragment.newInstance())
            }
            SIM_CHECK_PROGRESS -> {
                fragmentReplace(SimCheckProgressFragment.newInstance(data = data))
            }
            SIM_CHECK_DIFFERENT_MSISDN -> {
                fragmentReplace(SimCheckDifferentMsisdnFragment.newInstance())
            }
            SIM_CHECK_WIFI -> {
                fragmentReplace(SimCheckWifiFragment.newInstance())
            }
            INTERNET -> {
                fragmentReplace(FullScreenDialogFragment.newInstance(this))
            }
        }
    }

    fun fragmentReplace(fragment: Fragment) {
        val transaction = parentFragmentManager.beginTransaction()
        transaction.add(R.id.framelayout, fragment).commitAllowingStateLoss()
    }

    fun currentPage() = current

    fun getPhone(): String = phoneNumber
    fun newPhoneSet(boolean: Boolean? = null): Boolean {
        boolean?.let {
            newPhone = true
        }
        return newPhone
    }

    fun thanLoginWhichPage(data: String) {
        if (BaseWebFragment.urlRefState == UrlRefState.SEND_MONEY) {
            fragmentReplace(ContactSelectFragment.newInstance())
        } else if (BaseWebFragment.urlRefState == UrlRefState.UPLOAD_MONEY) {
            BaseWebFragment.calculateUrlUploadMoney(false)//false verdimki boş string koysun diye
            fragmentReplace(DetailPageFragment.newInstance("detay"))
        } else if (BaseWebFragment.urlRefState == UrlRefState.HAZIR_LIMIT) {
            BaseWebFragment.calculateUrlUploadMoney(isLogin())
            fragmentReplace(DetailPageFragment.newInstance("detay"))
        } else if (BaseWebFragment.urlRefState == UrlRefState.QR_ODE) {
            calculatePage(QR_SCAN, "qrOde")
        } else if (BaseWebFragment.urlRefState == UrlRefState.IBAN_TRANSFER) {
            BaseWebFragment.calculaterIbanTransfer(false)
            fragmentReplace(DetailPageFragment.newInstance("detay"))
        } else if (BaseWebFragment.urlRefState == UrlRefState.EYEICON) {
            BaseWebFragment.calculaterMainPage(false)
            fragmentReplace((DetailPageFragment.newInstance("anasayfa")))
        }

    }

    override fun succes() {
        calculatePage(SPLASH, "")
    }

    fun openDetailSendMoney(phone: String, name: String, qrCode: String?) {
        if (phone.isPhoneNumberValid() || !qrCode.isNullOrEmpty()) {
            BaseWebFragment.calculateUrlSendMoney(phone, name, qrCode)
            calculatePage(DETAIL_SENDMONEY_STEPONE, "detay")
        } else {
            viewModel.getPresenter()?.onWarning(
                "Seçtiğiniz numara uygun değildir. Kontrol edip lütfen tekrar deneyin",
                ""
            )
        }

    }

    fun openKycPopup(): Boolean {
        return !GlobalData.getAccountResponse.customerKycStatus && (urlRefState === UrlRefState.SEND_MONEY || urlRefState === UrlRefState.QR_ODE)
    }

    fun String.isPhoneNumberValid(): Boolean {
        var numberArray = phoneValidationList()
        if (this.isNullOrEmpty())
            return false
        else
            return numberArray.filter { it.equals(this.substring(0, 3)) }.isNotEmpty()
    }

    fun isLogin(state: Boolean? = null): Boolean {
        state?.let {
            mIsLogin = it
        }
        return mIsLogin
    }
}