package com.paycell.ui.sendmoney


import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.provider.Settings
import android.text.InputFilter
import android.text.InputType
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.paycell.base.BindingFragment
import com.paycell.extensions.*
import com.paycell.helper.GenericAdapter
import com.paycell.helper.ListItemViewModel
import com.paycell.remote.model.base.GlobalData
import com.paycell.ui.dialog.ChoosePhoneDialogFragment
import com.paycell.ui.dialog.KycDialogFragment
import com.paycell.ui.sendmoney.DetailFragment.Companion.CONTACT
import com.paycell.ui.sendmoney.DetailFragment.Companion.DETAIL
import com.paycell.ui.sendmoney.DetailFragment.Companion.LOGIN_COMPLETED
import com.paycell.ui.sendmoney.DetailFragment.Companion.QR_SCAN
import com.paycell.ui.sendmoney.DetailFragment.Companion.mDetailFrafment
import com.paycellsdk.R
import com.paycellsdk.databinding.ActivityContactSelectBinding
import org.koin.androidx.viewmodel.ext.android.viewModel


class ContactSelectFragment : BindingFragment<ActivityContactSelectBinding>(), GeneralPresenter,
    QrScanFragment.IQrScanCallBack {


    private val viewModel by viewModel<GeneralViewModel>()

    override val layoutBindId: Int
        get() = R.layout.activity_contact_select

    private val genericAdapter by lazy {
        GenericAdapter<ContactItemViewState>(R.layout.row_user_contact)
    }
    var arrayList: ArrayList<ContactItemViewState> = arrayListOf()

    private val CONTACTS_READ_REQ_CODE = 100
    lateinit var mFragment: DetailFragment
    var openSettings: Boolean = false

    companion object {
        fun newInstance(): ContactSelectFragment {
            return ContactSelectFragment().apply {
            }
        }
    }

    override fun onResume() {
        super.onResume()
        mFragment = mDetailFrafment
        if (openSettings) {
            openContact()
            openSettings = false
        }
    }

    override fun initPresenter() {
        viewModel.setPresenter(this)
    }

    override fun initUI(view: View) {
        mFragment = mDetailFrafment
        binding.vm = viewModel
        binding.rv.adapter = genericAdapter

    }

    override fun initListener() {
        openContact()
        listenerItem()
        searchViewListener()
        qrResultObserver()

        binding.tvIptal.setOnClickListener {
            if (mFragment.currentPage().equals(LOGIN_COMPLETED))
                mFragment.calculatePage(DETAIL, "anasayfa")
            else if (mFragment.currentPage().equals(CONTACT)) {
                mFragment.calculatePage(DETAIL, "anasayfa")
            }

        }
        binding.imageView.setOnClickListener {
            if (mFragment.currentPage().equals(LOGIN_COMPLETED))
                mFragment.calculatePage(DETAIL, "anasayfa")
            else if (mFragment.currentPage().equals(CONTACT)) {
                mFragment.calculatePage(DETAIL, "anasayfa")
            }
        }
        binding.ivNoFoundUser.setOnClickListener {
            openDetailPage(binding.searcView.text.toString(), binding.searcView.text.toString())
        }
        binding.ivSendQr.setOnClickListener {
            mFragment.calculatePage(QR_SCAN, "contact")
        }
        binding.tvSettigs.setOnClickListener {
            openSettings = true
            startActivity(Intent(Settings.ACTION_SETTINGS))
        }

    }

    private fun searchViewListener() {
        binding.searcView.afterTextChanged { query ->
            if (query.isNullOrEmpty()) {
                if (hasPermission()) {
                    binding.rv.visible()
                    binding.tvNotFoundUser.gone()
                    binding.ivNoFoundUser.gone()
                    genericAdapter.addItems(requireActivity().tamirCalucalate())
                }
                query.pgoneNumberParsing().phoneFormat()
            } else {
                filter(query.pgoneNumberParsing())
            }
        }
    }

    fun filter(query: String) {
        var filter = arrayList.filter {
            it.name.toLowerCase().contains(query.toLowerCase()) ||
                    it.number.clearSpaces().toLowerCase().contains(query.toLowerCase())
        }

        if (filter.isEmpty()) {
            query.phoneFormat()
            binding.rv.gone()
            binding.tvNotFoundUser.visible()
            query.ivNoFoundUserVisiblty()
        } else {
            binding.rv.visible()
            binding.tvNotFoundUser.gone()
            binding.ivNoFoundUser.gone()
            genericAdapter.addItems(filter as ArrayList<ContactItemViewState>)
            binding.searcView.filters = arrayOf(InputFilter.LengthFilter(30))
        }
    }

    fun String.phoneFormat(): Boolean {
        var numberArray = phoneValidationList()
        var isValidatePhone =
            if (this.length > 3) numberArray.filter { it.equals(this.substring(0, 3)) }.isNotEmpty()
            else false
        if (this.length == 10 && isValidatePhone) {
            binding.ivNoFoundUser.tintSrc(R.color.aqua)
            binding.ivNoFoundUser.isClickable = true
            return true
        } else {
            binding.ivNoFoundUser.tintSrc(R.color.light_gray)
            binding.ivNoFoundUser.isClickable = false
            return false
        }
    }

    fun String.pgoneNumberParsing(): String {
        var phone: String = if (this.isNotEmpty() && this.first().toString() == "0") {
            this.replaceFirst("0", "")
        } else if (this.length >= 3 && this.substring(0, 3) == "+90") {
            this.replaceFirst("+90", "")
        } else this
        return phone
    }

    fun String.ivNoFoundUserVisiblty() {
        if (this.isNumeric()) {
            binding.ivNoFoundUser.visible()
            //  binding.searcView.filters = arrayOf(InputFilter.LengthFilter(10))
        } else
            binding.ivNoFoundUser.gone()
    }

    fun listenerItem() {
        genericAdapter.setOnListItemViewClickListener(object :
            GenericAdapter.OnListItemViewClickListener {
            override fun onClick(view: View, position: Int, model: ListItemViewModel) {
                var result = (model as ContactItemViewState)
                equeslUser(result)
            }
        })
    }

    fun equeslUser(contact: ContactItemViewState) {
        if (contact.numberArray!!.size > 1) {
            ChoosePhoneDialogFragment.newInstance(
                contact.name,
                contact,
                mFragment
            )
                .show(parentFragmentManager, null)
        } else {
            openDetailPage(contact.number, contact.name)
        }
    }

    fun openDetailPage(number: String, name: String) {
        mFragment.openDetailSendMoney(number, name, "")
    }

    override fun qrResult(resultText: String) {
        println("QR_RESULT_SUCCES" + resultText)
        mFragment.openDetailSendMoney("", "", resultText)

    }

    fun qrResultObserver() {
        viewModel.qrResult.observe(this, Observer {
            print(it)
        })
    }

    fun openContact() {
        var status = GlobalData.getAccountResponse.customerKycStatus
        if (status) {
            if (hasPermission()) {
                arrayList.addAll(requireActivity().tamirCalucalate())
                genericAdapter.addItems(arrayList)
                binding.rv.visible()
                binding.izinVerilmedi.gone()
            } else {
                requestPermissionWithRationale(
                    Manifest.permission.READ_CONTACTS,
                    CONTACTS_READ_REQ_CODE
                )
            }
        } else {
            if (mFragment.openKycPopup()) {
                KycDialogFragment.newInstance(true).show(parentFragmentManager, null)
            }
        }

    }

    fun hasPermission(): Boolean {
        return ContextCompat.checkSelfPermission(
            requireContext(), Manifest.permission.READ_CONTACTS
        ) == PackageManager.PERMISSION_GRANTED
    }

    fun requestPermissionWithRationale(permission: String, requestCode: Int) {
        this@ContactSelectFragment.requestPermissions(arrayOf(permission), requestCode)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == CONTACTS_READ_REQ_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            arrayList.addAll(requireActivity().tamirCalucalate())
            genericAdapter.addItems(arrayList)
            binding.rv.visible()
            binding.izinVerilmedi.gone()
        } else {
            /*** Eğer izin verilmezse input typi değiştir**/
            binding.searcView.inputType = InputType.TYPE_CLASS_NUMBER
            binding.rv.gone()
            binding.izinVerilmedi.visible()
        }
    }

}