package com.paycell.ui.sendmoney

import com.paycell.base.BaseWebFragment
import com.paycell.ui.sendmoney.DetailFragment.Companion.SPLASH
import com.paycell.ui.sendmoney.DetailFragment.Companion.isInit
import com.paycell.ui.sendmoney.DetailFragment.Companion.mDetailFrafment

class MainPageFragment : BaseWebFragment(), BaseWebFragment.IListener {

    override lateinit var mFragment: DetailFragment

    companion object {
        var onResume = false
        fun newInstance(): MainPageFragment {
            return MainPageFragment().apply {}
        }
    }

    override fun initUI() {
        callWebView()
    }

    fun callWebView() {
        mFragment = mDetailFrafment
        initWeb(getMainUrl(mFragment.getPhone()!!), this)

    }

    override fun openSplash(ok: Boolean) {
        if (ok) {
            mFragment.calculatePage(SPLASH, "")
        }
    }

    override fun onResume() {
        super.onResume()
        onResume = false
    }

    override fun onStop() {
        super.onStop()
        onResume = true

    }


}