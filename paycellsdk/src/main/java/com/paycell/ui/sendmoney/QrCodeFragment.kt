package com.paycell.ui.sendmoney

import android.view.View
import androidx.fragment.app.Fragment
import com.paycell.base.BindingFragment
import com.paycell.extensions.color
import com.paycell.ui.sendmoney.DetailFragment.Companion.CONTACT
import com.paycell.helper.segmentbutton.SegmentedButtonGroup
import com.paycell.ui.sendmoney.DetailFragment.Companion.mDetailFrafment
import com.paycellsdk.R
import com.paycellsdk.databinding.FragmentQrCodeBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class QrCodeFragment : BindingFragment<FragmentQrCodeBinding>(), GeneralPresenter,
    SegmentedButtonGroup.OnPositionChangedListener, QrScanFragment.IQrScanCallBack {

    private val viewModel by viewModel<GeneralViewModel>()

    override val layoutBindId: Int get() = R.layout.fragment_qr_code


    lateinit var mFragment: DetailFragment

    companion object {
        fun newInstance(): QrCodeFragment {
            return QrCodeFragment().apply {
              }
        }
    }

    override fun initPresenter() {
        viewModel.setPresenter(this)
    }

    override fun initUI(view: View) {
        mFragment = mDetailFrafment
        binding.vm = viewModel

    }

    override fun initListener() {
       // binding.buttonGroup.onPositionChangedListener = this

        fragmentReplace(QrShowFragment.newInstance())

        binding.backBtnQrScreen.setOnClickListener {
            mFragment.calculatePage(CONTACT, "")
        }
        binding.tvCancel.setOnClickListener {
            mFragment.calculatePage(CONTACT, "")
        }

    }

    override fun onPositionChanged(position: Int) {
        when (position) {
            0 -> {
                fragmentReplace(QrShowFragment.newInstance())
                binding.container.setBackgroundColor(requireContext().color(R.color.white))
            }
            1 -> {
                fragmentReplace(QrScanFragment.newInstance("qrCode"))
                binding.container.setBackgroundColor(requireContext().color(R.color.black))
            }
        }
    }

    fun fragmentReplace(fragment: Fragment) {
        val transaction = requireActivity().supportFragmentManager.beginTransaction()
        transaction.replace(R.id.framelayoutQrCode, fragment).addToBackStack(null).commit()

    }
    override fun qrResult(resultText: String) {

    }
}