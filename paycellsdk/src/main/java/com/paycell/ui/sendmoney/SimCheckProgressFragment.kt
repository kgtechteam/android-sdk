package com.paycell.ui.sendmoney


import android.view.View
import com.paycell.base.BindingFragment
import com.paycell.extensions.setVectorForPreLollipop
import com.paycell.remote.model.base.GlobalData.Companion.findClientParams
import com.paycell.ui.sendmoney.DetailFragment.Companion.LOGIN_COMPLETED
import com.paycell.ui.sendmoney.DetailFragment.Companion.SIM_CHECK_DIFFERENT_MSISDN
import com.paycell.ui.sendmoney.DetailFragment.Companion.SIM_CHECK_WIFI
import com.paycell.ui.sendmoney.DetailFragment.Companion.mDetailFrafment
import com.paycell.utils.Constant.SIM_CONTROL
import com.paycell.utils.NetworkUtils.getConnectionType
import com.paycell.utils.PrefUtils
import com.paycellsdk.R
import com.paycellsdk.databinding.FragmentSimCheckProgressBinding
import org.koin.androidx.viewmodel.ext.android.viewModel


class SimCheckProgressFragment : BindingFragment<FragmentSimCheckProgressBinding>(),
    GeneralPresenter {

    private val viewModel by viewModel<GeneralViewModel>()
    override val layoutBindId: Int get() = R.layout.fragment_sim_check_progress
    lateinit var mFragment: DetailFragment
    var data: String = ""

    companion object {
        fun newInstance(data: String): SimCheckProgressFragment {
            return SimCheckProgressFragment().apply {
                this.data = data
            }
        }
    }

    override fun initPresenter() {
        viewModel.setPresenter(this)
    }

    override fun initUI(view: View) {
        mFragment = mDetailFrafment
        binding.vm = viewModel
        startFllow()
     }

    fun startFllow() {
        var simControl = "true"
        try {
            simControl = SIM_CONTROL.findClientParams()[0]
        } catch (e: Exception) {
        }
        if (simControl.equals("true")&&isSimCheckVerify()) {
            if (data.equals("tekrardene")) {
                viewModel.getMsisdnProd()
            } else {
                wifiCalculate()
            }
        } else {
            mFragment.calculatePage(LOGIN_COMPLETED, "")
        }
    }

    override fun initListener() {
        binding.container.setBackgroundDrawable(requireContext().setVectorForPreLollipop(R.drawable.ic_paycell_background_notlogo))

    }
    fun wifiCalculate() {
        if (requireContext().getConnectionType() == "WIFI") {
            mFragment.calculatePage(SIM_CHECK_WIFI, "")
        } else {
            mFragment.calculatePage(DetailFragment.SIM_CHECK_PROGRESS, "tekrardene")
        }
    }

    override fun succesMsisdn(msisdn: String?) {
        if (msisdn == mFragment.getPhone()) {
            mFragment.calculatePage(LOGIN_COMPLETED, "")
            PrefUtils.simControlVerify= DetailFragment().getPhone().toString()
        } else {
            mFragment.calculatePage(SIM_CHECK_DIFFERENT_MSISDN, "")
        }
    }

    fun isSimCheckVerify()=PrefUtils.simControlVerify!=DetailFragment().getPhone()

}

