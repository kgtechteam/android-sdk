package com.paycell.ui.sendmoney


import android.view.View
import com.paycell.base.BindingFragment
import com.paycell.extensions.setVectorForPreLollipop
import com.paycell.ui.sendmoney.DetailFragment.Companion.SPLASH
import com.paycell.ui.sendmoney.DetailFragment.Companion.mDetailFrafment
import com.paycell.utils.PrefUtils
import com.paycellsdk.R
import com.paycellsdk.databinding.FragmentSimCheckDifferentMsisdnBinding
import org.koin.androidx.viewmodel.ext.android.viewModel


class SimCheckDifferentMsisdnFragment : BindingFragment<FragmentSimCheckDifferentMsisdnBinding>(), GeneralPresenter {

    private val viewModel by viewModel<GeneralViewModel>()
    override val layoutBindId: Int get() = R.layout.fragment_sim_check_different_msisdn
    lateinit var mFragment: DetailFragment

    companion object {
        fun newInstance(): SimCheckDifferentMsisdnFragment {
            return SimCheckDifferentMsisdnFragment().apply {
            }
        }
    }

    override fun initPresenter() {
        viewModel.setPresenter(this)
    }

    override fun initUI(view: View) {
        mFragment = mDetailFrafment
        binding.vm = viewModel
        binding.btnNext.setBackgroundDrawable(requireContext().setVectorForPreLollipop(R.drawable.back_aqua_outline))
        binding.container.setBackgroundDrawable(requireContext().setVectorForPreLollipop(R.drawable.ic_paycell_background_notlogo))

    }
    override fun initListener() {
        binding.btnNext.setOnClickListener {
            PrefUtils.authToken=""
            mDetailFrafment.newPhoneSet(true)
            mFragment.calculatePage(SPLASH,"")
         }

    }





}

