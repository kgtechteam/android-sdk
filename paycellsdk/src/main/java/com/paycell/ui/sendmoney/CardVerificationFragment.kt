package com.paycell.ui.sendmoney

import android.text.SpannableString
import android.view.View
import android.view.WindowManager
import androidx.lifecycle.Observer
import com.paycell.base.BindingFragment
import com.paycell.extensions.*
import com.paycell.helper.CardValidationAdapter
import com.paycell.helper.CardValidationAdapterCallback
import com.paycell.remote.model.Card
import com.paycell.remote.model.base.GlobalData
import com.paycell.ui.login.LoginFragment
import com.paycell.ui.login.LoginFragment.Companion.CREATE_PASSWORD
import com.paycell.ui.login.LoginFragment.Companion.isCardValidationVerify
import com.paycell.ui.onboardingloginstep.ILoginStepPresenter
import com.paycell.ui.onboardingloginstep.LoginStepViewModel
import com.paycellsdk.R
import com.paycellsdk.databinding.FragmentCardVerificationBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class CardVerificationFragment :
    BindingFragment<FragmentCardVerificationBinding>(), ILoginStepPresenter,
    CardValidationAdapterCallback {

    lateinit var fragment: LoginFragment
    var verifyButtonIsEnable: Boolean = false

    companion object {
        fun newInstance(fragment: LoginFragment): CardVerificationFragment {
            return CardVerificationFragment().apply {
                this.fragment = fragment
            }
        }
    }

    private val viewModel by viewModel<LoginStepViewModel>()
    override val layoutBindId: Int get() = R.layout.fragment_card_verification

    override fun initPresenter() {
        viewModel.setPresenter(this)
    }

    override fun initUI(view: View) {
        binding.vm = viewModel
        binding.thisCardsNotBelongToMeTv.text = spanneable()
        binding.background.setBackgroundDrawable(requireContext().setVectorForPreLollipop(R.drawable.ic_paycell_background_white))
    }

    override fun initListener() {
        binding.thisCardsNotBelongToMeTv.setOnClickListener {
            viewModel.cancelAccount {
                isCardValidationVerify = true
                fragment.changePagerPosition(CREATE_PASSWORD)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        fragment.backButtonVisiblity(false)
      //  requireActivity().window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        viewModel.getCard()
        cardObserve()
    }

    fun cardObserve() {
        viewModel.cardResponse.observe(this, Observer {
            var cardValidateAdapter = CardValidationAdapter(
                it.card, this,
                GlobalData.initResponse.paycellCardBinList
            )
            binding.cardsListRvCardVerification.adapter = cardValidateAdapter

        })
    }


    fun dummeyData() {
        var list: ArrayList<Card> = arrayListOf(
            Card("1", "345345345345345", true, "sdfsdf"),
            Card("2", "345345345345345", true, "sdfsdf"),
            Card("3", "345345345345345", true, "sdfsdf"),
            Card("4", "345345345345345", true, "sdfsdf")
        )
        var cardValidateAdapter = CardValidationAdapter(
            list, this,
            GlobalData.initResponse.paycellCardBinList
        )
        binding.cardsListRvCardVerification.adapter = cardValidateAdapter


    }


    override fun cardValidationId(id: String) {
        GlobalData.cardValidationId = id
        isCardValidationVerify = true
        fragment.changePagerPosition(CREATE_PASSWORD)
    }

    override fun cardValidation(mont: String, year: String, card: Card) {
        verifyButtonIsEnable = true
        buttonStatusChange(true)
        binding.btnNext.setOnClickListener {
            if (verifyButtonIsEnable) {
                viewModel.validateCard(card = card, month = mont, year = year)
            }
        }

    }

    override fun cardValidationError() {
        verifyButtonIsEnable = false
        buttonStatusChange(false)
    }

    fun spanneable(): SpannableString {
        return spannable {
            underline(
                size(
                    1.2f, color(
                        resources.getColor(R.color.aqua),
                        "Bu kartlar bana ait değil."
                    )
                )
            )
        }
    }

    fun buttonStatusChange(status: Boolean) {
        if (status) {
            binding.btnNext.setBackgroundDrawable(requireContext().setVectorForPreLollipop(R.drawable.back_aqua_outline))
            binding.btnNext.setTextColor(resources.getColor(R.color.charcoal_grey))
        } else {
            binding.btnNext.setBackgroundResource(R.drawable.back_gray_outline)
            binding.btnNext.setTextColor(resources.getColor(R.color.blue_grey))
        }
    }

}

