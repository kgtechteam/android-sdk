package com.paycell.ui.sendmoney


import android.content.Intent
import android.provider.Settings
import android.text.SpannableString
import android.view.View
import com.paycell.base.BindingFragment
import com.paycell.extensions.*
import com.paycell.ui.sendmoney.DetailFragment.Companion.mDetailFrafment
import com.paycell.utils.NetworkUtils.getConnectionType
import com.paycellsdk.R
import com.paycellsdk.databinding.FragmentSimCheckWifiBinding
import org.koin.androidx.viewmodel.ext.android.viewModel


class SimCheckWifiFragment : BindingFragment<FragmentSimCheckWifiBinding>(), GeneralPresenter {

    private val viewModel by viewModel<GeneralViewModel>()
    override val layoutBindId: Int get() = R.layout.fragment_sim_check_wifi
    lateinit var mFragment: DetailFragment

    companion object {

        fun newInstance(): SimCheckWifiFragment {
            return SimCheckWifiFragment().apply {
            }
        }
    }

    override fun initPresenter() {
        viewModel.setPresenter(this)
    }

    override fun initUI(view: View) {
        mFragment = mDetailFrafment
        binding.vm = viewModel
        binding.tvOpenWifi.text = spanabble()
        binding.btnNext.setBackgroundDrawable(requireContext().setVectorForPreLollipop(R.drawable.back_aqua_outline))
        binding.container.setBackgroundDrawable(requireContext().setVectorForPreLollipop(R.drawable.ic_paycell_background_notlogo))


    }

    override fun initListener() {

        binding.tvOpenWifi.setOnClickListener {
            startActivity(Intent(Settings.ACTION_WIFI_SETTINGS))
        }
        binding.btnNext.setOnClickListener {
            if (viewModel.connection())
                if (requireContext().getConnectionType() == "MOBILE") {
                    mFragment.calculatePage(DetailFragment.SIM_CHECK_PROGRESS, "tekrardene")
                }

        }

    }

    fun spanabble(): SpannableString {
        return spannable {
            underline(
                size(
                    1.2f,
                    color(resources.getColor(R.color.aqua), "Wifi Ayarlarına Git")
                )
            )
        }
    }


}

