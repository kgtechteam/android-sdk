package com.paycell.ui.sendmoney


import android.graphics.Color
import android.os.CountDownTimer
import android.text.Spannable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.view.View
import com.paycell.remote.model.base.GlobalData
import com.paycell.base.BindingFragment
import com.paycell.extensions.getTimerOtp
import com.paycell.helper.StringUtils
import com.paycell.utils.QrCodeManager2
import com.paycellsdk.R
import com.paycellsdk.databinding.FragmentQrShowBinding
import org.koin.androidx.viewmodel.ext.android.viewModel


class QrShowFragment : BindingFragment<FragmentQrShowBinding>(), GeneralPresenter {

    private val viewModel by viewModel<GeneralViewModel>()
    override val layoutBindId: Int get() = R.layout.fragment_qr_show
    var globalData = GlobalData
    lateinit var timer: CountDownTimer

    companion object {

        fun newInstance(): QrShowFragment {
            return QrShowFragment()
        }
    }

    override fun initPresenter() {
        viewModel.setPresenter(this)
    }

    override fun initUI(view: View) {
        binding.vm = viewModel
        prepereCodes()
        prepareTexts()
    }


    override fun initListener() {

    }

    private fun prepareTexts() {
        //  descriptionTvQrScreen=subHeaderTv
        binding.descriptionTvQrScreen.text = globalData.languages["paycell_qr_page_desc_text"]
        val name = globalData.getAccountResponse.firstName
        val lastName = globalData.getAccountResponse.lastName
        val nameAndLastName: String
        if (!TextUtils.isEmpty(name) && !TextUtils.isEmpty(lastName)) {
            nameAndLastName = "$name $lastName"
            val limitNameAndLastName = StringUtils.limitString(nameAndLastName, 17)
            binding.headerTv.text = limitNameAndLastName
        } else {
            binding.headerTv.setText(globalData.languages["paycell_my_profile_no_name_text"])
        }
        //  accountIdTv.setText(ResponseSingleton.getInstance().getGetAccountResponse().getAccountId())
    }

    private fun prepereCodes() {
        val qrCodeManager2 = QrCodeManager2.getInstance()
        if (qrCodeManager2.isQrAndBarcodeCreated) {
            setupImages(qrCodeManager2)
        } else {
            val isSuccessful = qrCodeManager2.createQrAndBarcodeBitmapsWithDefaults(
                600,
                600,
                QrCodeManager2.UsageType.MONEY_TRANSFER
            )
            if (isSuccessful) {
                setupImages(qrCodeManager2)
            } else {
                binding.barcodeIvQrScreen.setVisibility(View.GONE)
                binding.qrBarcodeIvQrScreen.setVisibility(View.GONE)
            }
        }
    }

    private fun setupImages(qrCodeManager2: QrCodeManager2) {
        binding.barcodeIvQrScreen.alpha = 0.55f
        binding.barcodeIvQrScreen.setImageBitmap(qrCodeManager2.barcodeBitmap)
        //barcodeCii.setBottomText(ResponseSingleton.getInstance().getGetAccountResponse().getAccountId());
        binding.qrBarcodeIvQrScreen.alpha = 0.55f
        binding.qrBarcodeIvQrScreen.setImageBitmap(qrCodeManager2.qrBitmap)
    }

    private fun startRepetitiveRequestInterval(quickCodeTimeSeconds: Long) {
        timer = object : CountDownTimer(quickCodeTimeSeconds, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                if (getTimerOtp(millisUntilFinished).equals("00 : 00")) {
                }
            }

            override fun onFinish() {
                timerCancel()
            }
        }
        timer.start()
    }

    private fun timerCancel() {
        timer.cancel()
    }

    private fun setTimerText(second: Int) {
        val timerText = globalData.languages.get("paycell_quick_code_second_text")?.replace(
            "[X]",
            (second).toString()
        ) + " " + globalData.languages.get("paycell_quick_qr_code_timer_label")
        val timerTextSps = SpannableString(timerText)
        binding.codeRenewAtTvQrScreen.text = setTimerTextWhiteColor(second, timerTextSps)
        binding.codeRenewAtTvQrScreen.visibility = View.VISIBLE
    }

    fun setTimerTextWhiteColor(second: Int, finalText: SpannableString): SpannableString {
        val firstBlueStart = 0
        val firstBlueEnd =
            globalData.languages.get("paycell_quick_code_second_text")!!.length + Integer.toString(
                second
            ).length - 2
        finalText.setSpan(
            ForegroundColorSpan(Color.parseColor("#2f7dfa")),
            firstBlueStart,
            firstBlueEnd,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        return finalText
    }

}

