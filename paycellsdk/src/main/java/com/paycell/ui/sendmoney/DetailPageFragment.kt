package com.paycell.ui.sendmoney

import com.paycell.base.BaseWebFragment
import com.paycell.ui.sendmoney.DetailFragment.Companion.CONTACT
import com.paycell.ui.sendmoney.DetailFragment.Companion.DETAIL
import com.paycell.ui.sendmoney.DetailFragment.Companion.KAYDET
import com.paycell.ui.sendmoney.DetailFragment.Companion.QR_SCAN
import com.paycell.ui.sendmoney.DetailFragment.Companion.SPLASH
import com.paycell.ui.sendmoney.DetailFragment.Companion.mDetailFrafment

class DetailPageFragment : BaseWebFragment(), BaseWebFragment.IListener {

    override lateinit var mFragment: DetailFragment
    var nerden: String = ""

    companion object {
        fun newInstance(data: String): DetailPageFragment {
            return DetailPageFragment().apply {
                nerden = data
            }
        }
    }

    override fun initUI() {
        mFragment = mDetailFrafment
        if (nerden.equals("anasayfa")) {
            openWebView(loginUrl())
        } else if (nerden.equals("detay")) {
            openWebView(DETAIL_URL)
        }
    }

    fun openWebView(link: String) {
        initWeb(link, this)
    }

    override fun openSplash(ok: Boolean) {
        if (ok) {
            mFragment.calculatePage(SPLASH, "")
        }
    }

    override fun sendMoneyCallBack(ok: Boolean) {
        if (ok) {
            mFragment.calculatePage(CONTACT, KAYDET)
        }
    }

    override fun uploadMoneyCallBack(ok: Boolean) {
        if (ok) {   //todo serviste burda çok hata loglanır büyük ihtimalle
            mFragment.calculatePage(DETAIL, "detay")
        }
    }

    override fun ibanTransferCallBack(ok: Boolean) {
        if (ok) {
            mFragment.calculatePage(DETAIL, "detay")
        }
    }

    override fun openEye(ok: Boolean) {
        if (ok) {
            mFragment.calculatePage(DETAIL, "anasayfa")
        }
    }

    override fun mainCallBack(ok: Boolean) {
        if (ok) {
            mFragment.calculatePage(DETAIL, "anasayfa")
        }
    }

    override fun backToContact(ok: Boolean) {
        if (ok) {
            mFragment.calculatePage(CONTACT, "")
        }
    }

    override fun openQrPage(ok: Boolean) {
        if (ok) {
            mFragment.calculatePage(QR_SCAN, "qrOde")
        }
    }


}