package com.paycell.ui.sendmoney


import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.os.Build
import android.provider.MediaStore
import android.util.Log
import android.view.View
import androidx.core.app.ActivityCompat
import com.google.zxing.BinaryBitmap
import com.google.zxing.MultiFormatReader
import com.google.zxing.RGBLuminanceSource
import com.google.zxing.ResultPoint
import com.google.zxing.common.HybridBinarizer
import com.google.zxing.integration.android.IntentIntegrator
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.paycell.base.BindingFragment
import com.paycell.extensions.*
import com.paycell.helper.PermissionUtil
import com.paycell.remote.model.base.GlobalData
import com.paycell.ui.dialog.KycDialogFragment
import com.paycell.ui.sendmoney.DetailFragment.Companion.CONTACT
import com.paycell.ui.sendmoney.DetailFragment.Companion.mDetailFrafment
import com.paycell.utils.Other
import com.paycellsdk.R
import com.paycellsdk.databinding.FragmentQrScanBinding
import org.koin.androidx.viewmodel.ext.android.viewModel


class QrScanFragment : BindingFragment<FragmentQrScanBinding>(), GeneralPresenter{

    private val viewModel by viewModel<GeneralViewModel>()
    override val layoutBindId: Int get() = R.layout.fragment_qr_scan
    private var GALLERY_REQUEST_CODE = 238

    lateinit var mFragment: DetailFragment
    var data: String = ""

    companion object {
        fun newInstance(data: String): QrScanFragment {
            return QrScanFragment().apply {
                this.data = data
            }
        }
    }

    override fun initPresenter() {
        viewModel.setPresenter(this)
    }

    private val PERMISSION_CALLBACK_CONSTANT = 100
    private val PERMISSIONS_REQUEST_ACCESS_CAMERA = 0
    var barcodeResult: String = ""

    override fun initUI(view: View) {
        mFragment = mDetailFrafment
        binding.vm = viewModel

        if (activity != null) {
            binding.toolbar.visible()
            binding.toolbar.backButton().setOnClickListener {
                back()
            }
            binding.toolbar.iptalButton().setOnClickListener {
                back()
            }
        }
    }

    fun back() {
        if (data == "contact") {
            mFragment.calculatePage(CONTACT, "")
        } else {
            mFragment.calculatePage(DetailFragment.DETAIL, "anasayfa")
        }
    }

    override fun initListener() {
        initQrCodeReader()
        binding.pickFromGalleryBtnQrScreen.setOnClickListener {
            pauseCamera()
            if (requireActivity().hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                this@QrScanFragment.showGallery(GALLERY_REQUEST_CODE)
            } else {
                requestPermissionWithRationale()
            }
        }
        binding.barcodeScanner.setOnClickListener {
            print("")
        }

    }

    fun requestPermissionWithRationale() {
        this@QrScanFragment.requestPermissions(
            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
            PERMISSION_CALLBACK_CONSTANT
        )
    }

    var callback: BarcodeCallback = object : BarcodeCallback {
        override fun barcodeResult(result: BarcodeResult?) {
            if (barcodeResult != result?.text) {
                barcodeResult = result?.text!!
                qrResult(result.text)
            }

        }

        override fun possibleResultPoints(resultPoints: MutableList<ResultPoint>?) {
        }
    }

    fun initQrCodeReader() {
        var status=GlobalData.getAccountResponse.customerKycStatus
        if (status) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !PermissionUtil.hasPermission(requireContext(), Manifest.permission.CAMERA))
             {
                 requestPermissions(arrayOf(Manifest.permission.CAMERA), PERMISSIONS_REQUEST_ACCESS_CAMERA)
             } else {
                binding.barcodeScanner.decodeContinuous(callback);
                binding.barcodeScanner.resume()
                binding.barcodeScanner.statusView.gone()
            }
        }
        else{
            if (mFragment.openKycPopup()){
                KycDialogFragment.newInstance(true).show(parentFragmentManager, null)
            }

        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSIONS_REQUEST_ACCESS_CAMERA) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initQrCodeReader()
            }
        } else if (requestCode == PERMISSION_CALLBACK_CONSTANT) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                resumeCamera()
                this@QrScanFragment.showGallery(GALLERY_REQUEST_CODE)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        var result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        resumeCamera()
        if (result != null) {
            if (result.contents != null) {
                qrResult(result.contents)
            }
        }
        if (requestCode == GALLERY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                binding.barcodeScanner.pause()
                val bitmap = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    ImageDecoder.decodeBitmap(
                        ImageDecoder.createSource(
                            requireContext().contentResolver,
                            data?.data!!
                        )
                    ).copy(Bitmap.Config.RGB_565, false);
                } else {
                    MediaStore.Images.Media.getBitmap(context?.contentResolver, data?.data!!)
                }
                scanQRImage(bitmap)

            } else {
                onCancelledGallery()
            }
        }
    }

    fun scanQRImage(bMap: Bitmap) {
        viewModel.getPresenter()?.showLoading()
        var contents: String = ""
        val intArray = IntArray(bMap.width * bMap.height)
        //copy pixel data from the Bitmap into the 'intArray' array
        bMap.getPixels(intArray, 0, bMap.width, 0, 0, bMap.getWidth(), bMap.height)
        val source = RGBLuminanceSource(bMap.width, bMap.height, intArray)
        val bitmap = BinaryBitmap(HybridBinarizer(source))
        val reader = MultiFormatReader()
        try {
            val result = reader.decode(bitmap)
            contents = result.getText()
            viewModel.getPresenter()?.hideLoading()
        } catch (e: Exception) {
            viewModel.getPresenter()?.hideLoading()
            viewModel.getPresenter()?.onTimeOutError(
                GlobalData.languages["paycell_bip_scan_qr_error_message"].toString(),
                ""
            )
            Log.e("QrTest", "Error decoding barcode", e)

        }

        qrResult(contents)
    }


    override fun onResume() {
        super.onResume()
        resumeCamera()
    }

    override fun onPause() {
        super.onPause()
        pauseCamera()
    }

    fun pauseCamera() {
        binding.barcodeScanner.pause()
    }

    fun resumeCamera() {
        binding.barcodeScanner.resume()
    }

    fun qrResult(content: String) {
        println("QR_RESULT" + content)
        if (content.length > 4 && content.substring(0, 4) == "1000") {
            if (data == "qrOde") {
                viewModel.getAccountDetail(content) {
                    pauseCamera()
                    mFragment.openDetailSendMoney(
                        "",
                        "",
                        content
                    )
                }
            } else {
                pauseCamera()
                mFragment.openDetailSendMoney("", "", content)
            }

        } else {
            Other.showPopupInfo(
                requireContext(),
                GlobalData.languages["paycell_bip_scan_qr_error_message"].toString()
            ) {
                barcodeResult = ""
            }
        }

    }

    private fun onCancelledGallery() {
        resumeCamera()
    }


    interface IQrScanCallBack {
        fun qrResult(resultText: String)
    }
}

