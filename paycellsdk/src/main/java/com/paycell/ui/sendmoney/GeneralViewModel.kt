package com.paycell.ui.sendmoney

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.paycell.PaycellSdkApp
import com.paycell.base.BaseViewModel
import com.paycell.di.IDataManager
import com.paycell.remote.model.*
import com.paycell.remote.network.NetworkState
import com.paycell.utils.NetworkUtils
import com.paycell.utils.RequestHeaderManager
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


class GeneralViewModel(dataManager: IDataManager) : BaseViewModel<GeneralPresenter>(dataManager) {
    var qrResult: MutableLiveData<String> = MutableLiveData()
    var prodForwardMsisdnUrl =
        "http://services.paycell.com.tr/tpay/paycell/services/forwardapprs/forwardServiceApp/forwardMsisdn/"

    fun getAccountDetail(content: String,callback: () -> Unit) = viewModelScope.launch {
        if (connection()) {
            var newContent=content.replaceFirst("1000","")
            var getAccountRequest = GetAccountDetailRequest(
                requestHeader = RequestHeaderManager.getRequestHeader(), accountId = newContent
            )
            dataManager.getAccountDetail(getAccountRequest)
                .collect { state: NetworkState<GetAccountDetailResponse> ->
                    when (state) {
                        is NetworkState.Success -> {
                            if (state.response.responseHeader.responseCode == "0") {
                                if (!state.response.msisdn.isNullOrEmpty()) {
                                    callback.invoke()
                                }else{
                                    getPresenter()?.onTimeOutError(
                                        state.response.responseHeader.displayText,
                                        ""
                                    )
                                }
                            } else
                                getPresenter()?.onTimeOutError(
                                    state.response.responseHeader.displayText,
                                    ""
                                )
                        }
                        is NetworkState.Error -> {

                        }

                    }
                }
        }

    }


    fun getMsisdnProd() = viewModelScope.launch {
        if (connection()) {
            if (PaycellSdkApp.BASE_URL == "https://omccstb.turkcell.com.tr/") {
                getMsisdnByForwardId("9e7e6301-3ca9-414b-82a3-1299a5a6b36a")
            } else {
                dataManager.forwardMsisdnProd("", prodForwardMsisdnUrl)
                    .collect { state: NetworkState<ForwardMsisdnResponse> ->
                        when (state) {
                            is NetworkState.Success -> {
                                state.response.forwardId?.let {
                                    getMsisdnByForwardId(it)
                                }

                            }
                            is NetworkState.Error -> {

                            }

                        }
                    }
            }
        }

    }

    fun getMsisdnByForwardId(forwardId: String) = viewModelScope.launch {
        if (connection()) {
            var request = GetMsisdnByForwardIdRequest(
                requestHeader = RequestHeaderManager.getRequestHeader(),
                forwardId = forwardId
            )
            dataManager.getMsisdnByForwardId(request)
                .collect { state: NetworkState<GetMsisdnByForwardIdResponse> ->
                    when (state) {
                        is NetworkState.Success -> {
                            state.response.let {
                                if (it.responseHeader.responseCode == "0") {
                                    getPresenter()?.succesMsisdn(state.response.msisdn)
                                } else {
                                    getPresenter()?.onTimeOutError(
                                        state.response.responseHeader.displayText,
                                        state.response.responseHeader.responseCode
                                    )
                                }

                            }
                        }
                        is NetworkState.Error -> {
                        }

                    }
                }
        }

    }

    fun connection(): Boolean {
        if (NetworkUtils.hasInternet(PaycellSdkApp.context)) {
            return true
        } else {
            getPresenter()?.onTimeOutError("İnternet bağlantınızı kontrol ediniz", "0")
            return false
        }
    }

}
