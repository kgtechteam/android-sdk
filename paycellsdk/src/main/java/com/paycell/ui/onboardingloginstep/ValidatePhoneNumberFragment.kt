package com.paycell.ui.onboardingloginstep


import android.view.View
import androidx.lifecycle.Observer
import com.paycell.base.BindingFragment
import com.paycell.extensions.backgroundPaycell
import com.paycell.extensions.clearSpaces
import com.paycell.extensions.phoneNumber
import com.paycell.extensions.visible
import com.paycell.remote.model.AuthState
import com.paycell.remote.model.SendOtpRequest
import com.paycell.remote.model.ValidateOtpRequest
import com.paycell.remote.model.base.GlobalData
import com.paycell.ui.login.LoginFragment
import com.paycell.ui.login.LoginFragment.Companion.ENTRY_PHONE_NUMBER
import com.paycell.ui.login.LoginFragment.Companion.VALIDATE_PHONE_NUMBER
import com.paycell.ui.onboardingloginstep.EntryPhoneNumberFragment.Companion.phoneNumber
import com.paycell.ui.sendmoney.DetailFragment
import com.paycell.utils.RequestHeaderManager
import com.paycellsdk.R
import com.paycellsdk.databinding.FragmentValidatePhoneNumberBinding
import org.koin.androidx.viewmodel.ext.android.viewModel


class ValidatePhoneNumberFragment() :
    BindingFragment<FragmentValidatePhoneNumberBinding>(), ILoginStepPresenter {

    private val viewModel by viewModel<LoginStepViewModel>()

    override val layoutBindId: Int
        get() = R.layout.fragment_validate_phone_number

    override fun initPresenter() {
        viewModel.setPresenter(this)
    }

    lateinit var fragment: LoginFragment
    var lockSendOtp: Boolean = false

    companion object {
        fun newInstance(fragment: LoginFragment): ValidatePhoneNumberFragment {
            return ValidatePhoneNumberFragment().apply {
                this.fragment = fragment
            }
        }
    }

    var otpValidationId: String = ""

    override fun initUI(view: View) {
        binding.vm = viewModel
        binding.background.backgroundPaycell()
    }

    var authState: AuthState? = null

    override fun onResume() {
        super.onResume()
        showThisPage()
        fragment.backButtonVisiblity(true)
        LoginFragment.current_page = VALIDATE_PHONE_NUMBER
        DetailFragment.phoneNumber = phoneNumber
    }

    override fun initListener() {
        binding.pinLayout.replyButtonClick {
            sendOtpRequest()
        }
        binding.pinLayout.passwordErrorShortClick {
            sendOtpRequest()
        }
        sendValidateOtpRequest()

        if (fragment.isLogin) {
            binding.ivEditPhone.visible()
        }
        binding.ivEditPhone.setOnClickListener {
            fragment.changePagerPosition(ENTRY_PHONE_NUMBER)
        }

    }

    fun showThisPage() {
        binding.vmPhone = phoneNumber.phoneNumber()
        binding.pinLayout.resetPin()
        binding.pinLayout.focus()

        observeViewModel()
        if (!lockSendOtp) {
            sendOtpRequest()
        }
        if (fragment.isLoginFollow()) {
            binding.ivEditPhone.visible()
        }

    }

    fun sendOtpRequest() {
        var sendOtpRequest = SendOtpRequest(
            requestHeader = RequestHeaderManager.getRequestHeader(),
            msisdn = phoneNumber.clearSpaces()
        )
        viewModel.sendOtp(sendOtpRequest)
    }

    override fun succesSendOtp(lock: Boolean) {
        lockSendOtp = lock
    }

    fun sendValidateOtpRequest() {
        binding.pinLayout.fullPinEnteredListener {
            if (it && !otpValidationId.isNullOrEmpty()) {
                var validateOtpRequest = ValidateOtpRequest(
                    requestHeader = RequestHeaderManager.getRequestHeader(),
                    otp = binding.pinLayout.getPin(),
                    otpValidationId = otpValidationId
                )
                viewModel.validateOtp(validateOtpRequest)
            }
        }
    }

    private fun observeViewModel() {
        viewModel.validateOtpResponse.observe(this, Observer {
            binding.pinLayout.getValidateOtpResponse(
                it.responseHeader.responseCode,
                it.responseHeader.displayText,
                it.remainingTryCount
            )
            if (it.responseHeader.responseCode.equals("0")) {
                authState = it.authState
                GlobalData.validateOtpResponse = it
                getAccount()
            }
            else{
                viewModel.getPresenter()?.hideLoading()
            }

        })
        viewModel.sendOtpResponse.observe(this, Observer {
            binding.pinLayout.getOtpResponse(
                it.responseHeader.responseCode,
                it.reclaimDate,
                it.responseHeader.displayText,
                it.expireDate,
                it.responseHeader.responseDateTime
            )
            if (it.responseHeader.responseCode.equals("0")) {
                otpValidationId = it.otpValidationId
            }

        })

    }

    fun getAccount() {
        viewModel.getAccount {}
    }

    override fun succesGetAccountResponse() {
        viewModel.getPresenter()?.hideLoading()
        if (GlobalData.getAccountResponse.doCardValidation) {
            fragment.isCardValidation(true)
            fragment.changePagerPosition(LoginFragment.CARD_VALIDATION)
        } else if (authState == AuthState.WAITING_PIN_VALIDATION) {
            fragment.setIsLogin()
            fragment.changePagerPosition(LoginFragment.ENTRY_PASSWORD)
        } else if (authState == AuthState.WAITING_REGISTRATION) {
            fragment.changePagerPosition(LoginFragment.CREATE_PASSWORD)
        }

    }


}

