package com.paycell.ui.onboardingloginstep


import android.view.View
import android.view.WindowManager
import com.paycell.base.BindingFragment
import com.paycell.extensions.backgroundPaycell
import com.paycell.extensions.setVectorForPreLollipop
import com.paycell.ui.login.LoginFragment
import com.paycell.ui.login.LoginFragment.Companion.ENTRY_PHONE_NUMBER
import com.paycell.ui.login.LoginFragment.Companion.VALIDATE_PHONE_NUMBER
import com.paycellsdk.R
import com.paycellsdk.databinding.FragmentEntryPhoneNumberBinding
import org.koin.androidx.viewmodel.ext.android.viewModel


class EntryPhoneNumberFragment() : BindingFragment<FragmentEntryPhoneNumberBinding>(),
    ILoginStepPresenter {

    private val viewModel by viewModel<LoginStepViewModel>()
    override val layoutBindId: Int get() = R.layout.fragment_entry_phone_number

    override fun initPresenter() {
        viewModel.setPresenter(this)
    }

    lateinit var fragment: LoginFragment

    companion object {
        var phoneNumber: String = ""
        fun newInstance(fragment: LoginFragment): EntryPhoneNumberFragment {
            return EntryPhoneNumberFragment().apply {
                this.fragment = fragment
            }
        }
    }

    private var isNextPage: Boolean = false


    override fun initUI(view: View) {
        binding.vm = viewModel
        changeNextButtonBack()
        binding.background.backgroundPaycell()

    }

    override fun onResume() {
        super.onResume()
        fragment.backButtonVisiblity(true)
        LoginFragment.current_page = ENTRY_PHONE_NUMBER
    }

    override fun initListener() {
        binding.btnNext.setOnClickListener {
            if (binding.numberEntryEt.getText().isCheckPhoneNumber()) {
                if (isNextPage&&viewModel.connection()) {
                    phoneNumber = binding.numberEntryEt.getText()
                    fragment.changePagerPosition(VALIDATE_PHONE_NUMBER)
                }
            } else {
                binding.numberEntryEt.setError("Hatalı yada eksik numara girdiniz.")
            }

        }
        if (fragment.isLogin)
            binding.numberEntryEt.phoneLoginHint(hint = "5xx xxx xx xx")
        else
            binding.numberEntryEt.setHintAndText(hint = "Telefon numaranızı girin")
    }

    fun changeNextButtonBack() {
        binding.numberEntryEt.listenerPhoneInput {
            if (it.length == 10) {
                isNextPage = true
                binding.btnNext.setBackgroundDrawable(requireContext().setVectorForPreLollipop(R.drawable.back_aqua_outline))
                binding.btnNext.setTextColor(resources.getColor(R.color.charcoal_grey))
            } else {
                isNextPage = false
                binding.btnNext.setBackgroundResource(R.drawable.back_gray_outline)
                binding.btnNext.setTextColor(resources.getColor(R.color.blue_grey))
            }
        }
    }

    fun String.isCheckPhoneNumber(): Boolean {
        if (this.isEmpty()) {
            return false
        } else {
            return this.substring(0, 1).equals("5")
        }
    }
}

