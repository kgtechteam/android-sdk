package com.paycell.ui.onboardingloginstep

import android.view.View
import com.paycell.base.BaseWebFragment
import com.paycell.base.BindingFragment
import com.paycell.extensions.backgroundPaycell
import com.paycell.extensions.makeLinks
import com.paycell.extensions.setVectorForPreLollipop
import com.paycell.extensions.showKeyboard
import com.paycell.remote.model.RegisterRequest
import com.paycell.remote.model.base.GlobalData
import com.paycell.remote.model.base.GlobalData.Companion.getAccountResponse
import com.paycell.ui.dialog.ConditionAndTermsDialogFragment
import com.paycell.ui.login.LoginFragment
import com.paycell.ui.login.LoginFragment.Companion.CREATE_EMAIL_AND_CONDITION
import com.paycell.ui.onboardingloginstep.CreatePasswordFragment.Companion.password
import com.paycell.ui.sendmoney.DetailFragment
import com.paycell.ui.sendmoney.DetailFragment.Companion.SIM_CHECK_PROGRESS
import com.paycell.utils.RequestHeaderManager
import com.paycellsdk.R
import com.paycellsdk.databinding.FragmentVerifyEmailBinding
 import org.koin.androidx.viewmodel.ext.android.viewModel


class CreateEmailAndConditionFragment:
    BindingFragment<FragmentVerifyEmailBinding>(), ILoginStepPresenter {

    private val viewModel by viewModel<LoginStepViewModel>()
    var globalData = GlobalData.languages

    override val layoutBindId: Int
        get() = R.layout.fragment_verify_email


    override fun initPresenter() {
        viewModel.setPresenter(this)
    }

    lateinit var fragment: LoginFragment

    companion object {
        fun newInstance(fragment: LoginFragment): CreateEmailAndConditionFragment {
            return CreateEmailAndConditionFragment().apply {
                this.fragment = fragment
            }
        }
    }

    override fun initUI(view: View) {
        binding.vm = viewModel
        setPersonalDataSpannableText()
        setSpannableAgreement()
        setReceiveMessageSpannableText()
        changeNextButtonStatus()

        binding.verifyEmailEt.setHintAndText(hint = "E-postanızı girin")
        binding.background.backgroundPaycell()
    }

    override fun onResume() {
        super.onResume()
        fragment.backButtonVisiblity(true)
        LoginFragment.current_page =CREATE_EMAIL_AND_CONDITION
        binding.verifyEmailEt.getEditext().requestFocus()
       requireContext().showKeyboard(binding.verifyEmailEt.getEditext())
    }

    private fun setSpannableAgreement() {
        //  val fullstring = "Lütfen A ve B’ni kabul edin."
        val fullstring =
            globalData["paycell_login_email_agreement_1"] + globalData["paycell_login_email_agreement_2"] + globalData["paycell_login_email_agreement_3"] + globalData["paycell_login_email_agreement_4"] + globalData["paycell_login_email_agreement_5"]
        binding.userAgreementTv.text = fullstring
        binding.userAgreementTv.makeLinks(
            Triple(
                globalData["paycell_login_email_agreement_2"].toString(), //değiştirilecek olan
                View.OnClickListener {
                    ConditionAndTermsDialogFragment.newInstance(
                        getAccountResponse.getParameterExtensionForLanguage(),
                        globalData["paycell_engagement_account"].toString()
                    ).show(parentFragmentManager, null)
                }, globalData["paycell_login_email_agreement_2"].toString()  //yerine gelecek olan
            ), Triple(
                globalData["paycell_login_email_agreement_4"].toString(),
                View.OnClickListener {
                    ConditionAndTermsDialogFragment.newInstance(
                        GlobalData.languages["uks"],
                        globalData["paycell_login_email_agreement_4"].toString()
                    )
                        .show(parentFragmentManager, null)
                },
                globalData["paycell_login_email_agreement_4"].toString()
            ), context = requireContext(), isUnderlined = true
        )
    }

    private fun setReceiveMessageSpannableText() {
        // val fullstring = "Kampanya, fırsatlardan ve benzeri elektronik ticari iletilerden burada belirtilen şartlarda haberdar olmak istiyorum."
        val fullstring =
            globalData["paycell_commercial_eula_part_1"] + globalData["paycell_commercial_eula_part_2"] + globalData["paycell_commercial_eula_part_3"]
        binding.receiveMessageTv.text = fullstring
        binding.receiveMessageTv.makeLinks(
            Triple(
                globalData["paycell_commercial_eula_part_2"].toString(), //değiştirilecek olan
                View.OnClickListener {
                    ConditionAndTermsDialogFragment.newInstance(
                       GlobalData.languages["etk"],
                        globalData["paycell_commercial_eula_title"].toString()
                    )
                        .show(parentFragmentManager, null)
                }, globalData["paycell_commercial_eula_part_2"].toString()  //yerine gelecek olan
            ), context = requireContext(), isUnderlined = true
        )
    }


    private fun setPersonalDataSpannableText() {
        // val fullstring = "Kişisel Verilerin Korunması Kanunu Bilgilendirmesi uyarınca gerçekleştiren ilgili “Bilgilendirme” yi okudum ve kişisel verilerimin belirtilen kapsamda işlemsini kabul ederim."
        val fullstring =
            globalData["paycell_personal_data_eula_part_1"] + globalData["paycell_personal_data_eula_part_2"] + globalData["paycell_personal_data_eula_part_3"]
        binding.personalDataTv.text = fullstring
        binding.personalDataTv.makeLinks(
            Triple(
                globalData["paycell_personal_data_eula_part_2"].toString(), //değiştirilecek olan
                View.OnClickListener {
                    ConditionAndTermsDialogFragment.newInstance(
                        GlobalData.languages["kvkk"],
                        globalData["paycell_personal_data_eula_title"].toString()
                    )
                        .show(parentFragmentManager, null)
                }, globalData["paycell_personal_data_eula_part_2"].toString()  //yerine gelecek olan
            ), context = requireContext(), isUnderlined = true
        )
    }

    override fun initListener() {
        binding.btnNext.setOnClickListener {
            if (getAndCheckEmailAndOptions()) {
                viewModel.sendCreateSingleAccountRequest()
            }
        }
        binding.userAgreementCheckBox.setOnCheckedChangeListener { buttonView, isChecked ->
            if (binding.verifyEmailEt.getText().emailValidation()) {
                binding.btnNext.setBackgroundDrawable(requireContext().setVectorForPreLollipop(R.drawable.back_aqua_outline))
                binding.btnNext.setTextColor(resources.getColor(R.color.charcoal_grey))
            } else {
                binding.btnNext.setBackgroundResource(R.drawable.back_gray_outline)
                binding.btnNext.setTextColor(resources.getColor(R.color.blue_grey))
            }
        }
    }

    private fun getAndCheckEmailAndOptions(): Boolean {
        val email = binding.verifyEmailEt.getText()
        return !email.isNullOrEmpty() && email.contains("@") && email.contains(".com") && binding.userAgreementCheckBox.isChecked
    }


    override fun succesSingleRegisterAccount() {
        sendRegisterRequest()
    }
    fun sendRegisterRequest() {
        var registerRequest = RegisterRequest(
            requestHeader = RequestHeaderManager.getRequestHeader(),
            askPinBeforePurchase = 0,
            email = binding.verifyEmailEt.getText(),
            eulaId = getAccountResponse.eulaId,
            pin = password,
            etk = binding.receiveMessageCheckBox.isChecked,
            kvkk = binding.personalDataCheckBox.isChecked,
            eulaSource = "GET_ACCOUNT",
            cardValidationId = GlobalData.cardValidationId
        )
        viewModel.sendRegisterRequest(registerRequest)

    }

    override fun succesRegisterAccount() {
        BaseWebFragment.loginUrl()
        fragment.mFragment.isLogin(false)
        if (getAccountResponse.operatorId==1) {
            fragment.mFragment.calculatePage(SIM_CHECK_PROGRESS, "")
        }
        else{
            fragment.mFragment.calculatePage(DetailFragment.LOGIN_COMPLETED, "")
        }
     }


    private fun changeNextButtonStatus() {
        binding.verifyEmailEt.listenerNormalInput {
            if (it.emailValidation()) {
                binding.btnNext.setBackgroundDrawable(requireContext().setVectorForPreLollipop(R.drawable.back_aqua_outline))
                binding.btnNext.setTextColor(resources.getColor(R.color.charcoal_grey))
            } else {
                binding.btnNext.setBackgroundResource(R.drawable.back_gray_outline)
                binding.btnNext.setTextColor(resources.getColor(R.color.blue_grey))
            }
        }
    }

    fun String.emailValidation(): Boolean {
        return !this.isNullOrEmpty() && this.contains("@") && !this.contains(".@") && this.contains(
            ".com"
        ) && binding.userAgreementCheckBox.isChecked
    }
}