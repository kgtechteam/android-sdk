package com.paycell.ui.onboardingloginstep


import android.text.SpannableString
import android.view.View
import com.paycell.base.BaseWebFragment
import com.paycell.base.BindingFragment
import com.paycell.extensions.*
import com.paycell.remote.model.ValidatePinRequest
import com.paycell.remote.model.ValidatePinResponse
import com.paycell.remote.model.base.GlobalData
import com.paycell.ui.dialog.ForgotPasswordDialogFragment
import com.paycell.ui.dialog.SuspendedDialogFragment
import com.paycell.ui.login.LoginFragment
import com.paycell.ui.login.LoginFragment.Companion.ENTRY_PASSWORD
import com.paycell.ui.sendmoney.DetailFragment
import com.paycell.ui.sendmoney.DetailFragment.Companion.SIM_CHECK_PROGRESS
import com.paycell.utils.RequestHeaderManager
import com.paycellsdk.R
import com.paycellsdk.databinding.FragmentEntryPasswordBinding
import org.koin.androidx.viewmodel.ext.android.viewModel


class EntryPasswordFragment() :
    BindingFragment<FragmentEntryPasswordBinding>(), ILoginStepPresenter {

    private val viewModel by viewModel<LoginStepViewModel>()


    override val layoutBindId: Int
        get() = R.layout.fragment_entry_password

    var passwordLength: Int = 6

    override fun initPresenter() {
        viewModel.setPresenter(this)
    }

    lateinit var fragment: LoginFragment

    companion object {
        var loginPassword=""
        fun newInstance(fragment: LoginFragment): EntryPasswordFragment {
            return EntryPasswordFragment().apply {
                this.fragment = fragment
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (GlobalData.getAccountResponse.lastValidPinLenght == 4) {
            passwordLength = 4
            binding.createPassEt.setMaxlength(passwordLength)
        }
        showThisPage()
        LoginFragment.current_page = ENTRY_PASSWORD
        //fragment.backButtonVisiblity(false)
        changeNextButtonStatus()
    }

    override fun initUI(view: View) {
        binding.vm = viewModel
    }

    private fun showThisPage() {
        var length=if (passwordLength==4) 4 else 6
        binding.createPassEt.setHintAndText(hint = "$length haneli şifrenizi girin")
        binding.createPasswordTitleTv.text = "$length haneli şifrenizi girip Paycell’i kullanmaya başlayın!"
        binding.retryCodeSend.text = case2Spanabble()
        binding.background.backgroundPaycell()
        binding.createPassEt.underLineColor(false)

        if (!GlobalData.suspendMessage.isNullOrEmpty()){
            SuspendedDialogFragment.newInstance().show(parentFragmentManager, null)
        }
    }


    override fun initListener() {
        binding.btnNext.setOnClickListener {
            if (characterCount()) {
                validatePinRequest()
            }
        }
        binding.retryCodeSend.setOnClickListener {
            showForgotPinEmailDialog()
        }
        binding.container.setOnClickListener {
            //arkaplanda tıklayınca nasıl oluyorsa istek atıyor
        }
    }

    fun validatePinRequest() {
        var validatePinRequst = ValidatePinRequest(
            requestHeader = RequestHeaderManager.getRequestHeader(),
            pin = binding.createPassEt.getText()
        )
        viewModel.validatePinRequest(validatePinRequst, parentFragmentManager)
    }


    private fun changeNextButtonStatus() {
        binding.createPassEt.listenerNormalInput {
            if (binding.createPassEt.getText().length == passwordLength) {
                binding.btnNext.setBackgroundDrawable(requireContext().setVectorForPreLollipop(R.drawable.back_aqua_outline))
                binding.btnNext.setTextColor(resources.getColor(R.color.charcoal_grey))
            } else {
                binding.btnNext.setBackgroundResource(R.drawable.back_gray_outline)
                binding.btnNext.setTextColor(resources.getColor(R.color.blue_grey))
            }
        }
    }

    fun characterCount() = binding.createPassEt.getText().length == passwordLength

    fun case2Spanabble(): SpannableString {
        return spannable {
            underline(
                size(
                    1.2f,
                    color(resources.getColor(R.color.aqua), "Şifremi Unuttum")
                )
            )
        }
    }

    override fun succesPin(validatePinResponse: ValidatePinResponse) {
        if (validatePinResponse.responseHeader.responseCode == "0") {
            hideKeyboardFrom(requireContext(), requireView())
            BaseWebFragment.loginUrl()
            fragment.mFragment.isLogin(true)
            if (passwordLength == 4) {
                loginPassword=binding.createPassEt.getText()
                fragment.isPasswwordUpdate(true)
                fragment.changePagerPosition(LoginFragment.CREATE_PASSWORD)
            } else {
                if (GlobalData.getAccountResponse.operatorId == 1) {
                    fragment.mFragment.calculatePage(SIM_CHECK_PROGRESS, "")
                } else {
                    fragment.mFragment.calculatePage(DetailFragment.LOGIN_COMPLETED, "")
                }
            }


        } else if (validatePinResponse.responseHeader.responseCode == "3028") {
            binding.tvError.text = validatePinResponse.responseHeader.displayText
            binding.tvError.setTextColor(requireContext().color(R.color.red))
        }


    }

    private fun showForgotPinEmailDialog() {
        ForgotPasswordDialogFragment.newInstance().show(parentFragmentManager, null)
    }
}