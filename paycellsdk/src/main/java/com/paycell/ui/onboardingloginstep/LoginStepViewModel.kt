package com.paycell.ui.onboardingloginstep

import android.text.TextUtils
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.paycell.PaycellSdkApp
import com.paycell.base.BaseViewModel
import com.paycell.di.IDataManager
import com.paycell.helper.LiveEvent
import com.paycell.remote.model.*
import com.paycell.remote.model.base.BaseRequest
import com.paycell.remote.model.base.BaseResponse
import com.paycell.remote.model.base.GlobalData
import com.paycell.remote.model.init.InitRequest
import com.paycell.remote.model.init.InitResponse
import com.paycell.remote.network.NetworkState
import com.paycell.ui.dialog.SuspendedDialogFragment
import com.paycell.ui.sendmoney.DetailFragment
import com.paycell.utils.NetworkUtils
import com.paycell.utils.RequestHeaderManager
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


class LoginStepViewModel(dataManager: IDataManager) :
    BaseViewModel<ILoginStepPresenter>(dataManager) {

    var validateOtpResponse: MutableLiveData<ValidateOtpResponse> = MutableLiveData()
    var sendOtpResponse: MutableLiveData<SendOtpResponse> = MutableLiveData()
    var forgotResponse: MutableLiveData<ForgotPinResponse> = MutableLiveData()
    var initResponse: MutableLiveData<InitResponse> = MutableLiveData()
    var cardResponse: MutableLiveData<CardsResponse> = MutableLiveData()

    private val pageNumber = LiveEvent<Int>()

    val pageNumbers: LiveEvent<Int>
        get() = pageNumber


    fun sendOtp(sendOtpRequest: SendOtpRequest) = viewModelScope.launch {
        if (connection())
            dataManager.sendOtp(sendOtpRequest)
                .collect { state: NetworkState<SendOtpResponse> ->
                    when (state) {
                        is NetworkState.Success -> {
                            sendOtpResponse.value = state.response
                            if (state.response.responseHeader.responseCode.equals("0")) {
                                getPresenter()?.succesSendOtp(true)
                            } else if (state.response.responseHeader.responseCode.equals("1006")) {
                                getPresenter()?.succesSendOtp(false)
                                getPresenter()?.onTimeOutError(
                                    state.response.responseHeader.displayText,
                                    state.response.responseHeader.responseCode
                                )
                            } else {
                                getPresenter()?.succesSendOtp(false)
                            }

                        }
                        is NetworkState.Error -> {

                        }

                    }
                }
    }

    fun validateOtp(validateOtpRequest: ValidateOtpRequest) = viewModelScope.launch {
        if (connection()) {
            getPresenter()?.showLoading()
            dataManager.validateOtp(validateOtpRequest)
                .collect { state: NetworkState<ValidateOtpResponse> ->
                    when (state) {
                        is NetworkState.Success -> {
                            validateOtpResponse.value = state.response
                        }
                        is NetworkState.Error -> {

                        }

                    }
                }
        }

    }

    fun getAccount(getAccountResponse: (GetAccountResponse) -> Unit) = viewModelScope.launch {
        if (connection()) {
            var getAccountRequest = GetAccountRequest(
                requestHeader = RequestHeaderManager.getRequestHeader(),
                extraParameters = RequestHeaderManager.getExtraParametersForGetAccount()
            )
            dataManager.getAccount(getAccountRequest)
                .collect { state: NetworkState<GetAccountResponse> ->
                    when (state) {
                        is NetworkState.Success -> {
                            var responseCode = state.response.responseHeader.responseCode
                            GlobalData.getAccountResponse = state.response
                            GlobalData.suspendMessage = ""
                            if (responseCode == "0") {
                                getPresenter()?.succesGetAccountResponse()
                                getAccountResponse.invoke(state.response)
                            } else if (responseCode.equals("1006") || responseCode.equals("1002")) {
                                getPresenter()?.onTimeOutError(
                                    state.response.responseHeader.displayText,
                                    state.response.responseHeader.responseCode
                                )
                            } else if (responseCode == "4028") {
                                getPresenter()?.succesGetAccountResponse()
                                getAccountResponse.invoke(state.response)
                                GlobalData.suspendMessage =
                                    state.response.responseHeader.displayText

                            }

                        }
                        is NetworkState.Error -> {

                        }

                    }
                }

        }

    }

    fun sendRegisterRequest(registerRequest: RegisterRequest) = viewModelScope.launch {
        if (connection()) {
            getPresenter()?.showLoading()
            dataManager.sendRegisterRequest(registerRequest)
                .collect { state: NetworkState<RegisterResponse> ->
                    when (state) {
                        is NetworkState.Success -> {
                            getPresenter()?.hideLoading()
                            if (state.response.responseHeader.responseCode == "0") {
                                getPresenter()?.succesRegisterAccount()
                            } else {
                                getPresenter()?.onTimeOutError(
                                    state.response.responseHeader.displayText,
                                    state.response.responseHeader.responseCode
                                )
                            }
                        }
                        is NetworkState.Error -> {

                        }

                    }
                }
        }

    }

    fun sendCreateSingleAccountRequest() = viewModelScope.launch {
        if (connection()) {
            var createSingleAccountRequest = CreateSingleAccountRequest(
                requestHeader = RequestHeaderManager.getRequestHeader(),
                msisdn = DetailFragment.phoneNumber,
                forceCreate = false
            )
            getPresenter()?.showLoading()
            dataManager.createSingleAccount(createSingleAccountRequest)
                .collect { state: NetworkState<BaseResponse> ->
                    when (state) {
                        is NetworkState.Success -> {
                            getPresenter()?.hideLoading()
                            if (state.response.responseHeader.responseCode == "0") {
                                getPresenter()?.succesSingleRegisterAccount()
                            } else {
                                getPresenter()?.onTimeOutError(
                                    state.response.responseHeader.displayText,
                                    state.response.responseHeader.responseCode
                                )
                            }
                        }
                        is NetworkState.Error -> {

                        }

                    }
                }
        }
    }

    fun validatePinRequest(
        validatePinRequest: ValidatePinRequest,
        parentFragmentManager: FragmentManager
    ) = viewModelScope.launch {
        if (connection()) {
            getPresenter()?.showLoading()
            dataManager.login(validatePinRequest)
                .collect { state: NetworkState<ValidatePinResponse> ->
                    when (state) {
                        is NetworkState.Success -> {
                            getPresenter()?.hideLoading()
                            getPresenter()?.succesPin(state.response)
                            if (state.response.responseHeader.responseCode.equals("1006")) {
                                getPresenter()?.onTimeOutError(
                                    state.response.responseHeader.displayText,
                                    state.response.responseHeader.responseCode
                                )
                            } else if (state.response.responseHeader.responseCode.equals("3025")) {
                                SuspendedDialogFragment.newInstance()
                                    .show(parentFragmentManager, null)
                            }
                        }
                        is NetworkState.Error -> {
                            getPresenter()?.hideLoading()
                        }

                    }
                }
        }

    }

    fun forgotPinRequest(forgotPinRequest: ForgotPinRequest) = viewModelScope.launch {
        if (connection())
            dataManager.forgotPin(forgotPinRequest)
                .collect { state: NetworkState<ForgotPinResponse> ->
                    when (state) {
                        is NetworkState.Success -> {
                            forgotResponse.value = state.response
                            if (!state.response.responseHeader.responseCode.equals("0")) {
                                getPresenter()?.onTimeOutError(
                                    state.response.responseHeader.displayText,
                                    state.response.responseHeader.responseCode
                                )
                            }
                        }
                        is NetworkState.Error -> {

                        }

                    }
                }
    }

    fun getInit(
        initRequest: InitRequest,
        callbackError: () -> Unit
    ) = viewModelScope.launch {
        if (NetworkUtils.hasInternet(PaycellSdkApp.context)) {
            dataManager.postInit(initRequest = initRequest)
                .collect { state: NetworkState<InitResponse> ->
                    when (state) {
                        is NetworkState.Success -> {
                            state.response.let {
                                if (it.responseHeader.responseCode == "0") {
                                    getPresenter()?.initResponse(it)
                                    initResponse.value = it
                                    GlobalData().setLanguages(it.globalParameter)
                                }
                                if (state.response.responseHeader.responseCode.equals("1006")) {
                                    getPresenter()?.onTimeOutError(
                                        state.response.responseHeader.displayText,
                                        state.response.responseHeader.responseCode
                                    )
                                }

                            }
                        }
                        is NetworkState.Error -> {
                        }

                    }
                }
        } else {
            callbackError.invoke()
        }

    }

    fun getCard() = viewModelScope.launch {
        if (connection()) {
            getPresenter()?.showLoading()
            var request = BaseRequest(RequestHeaderManager.getRequestHeader())
            dataManager.getCards(request)
                .collect { state: NetworkState<CardsResponse> ->
                    when (state) {
                        is NetworkState.Success -> {
                            getPresenter()?.hideLoading()
                            state.response.let {
                                if (it.responseHeader.responseCode == "0") {
                                    cardResponse.value = it
                                }
                                if (state.response.responseHeader.responseCode.equals("1006")) {
                                    getPresenter()?.onTimeOutError(
                                        state.response.responseHeader.displayText,
                                        state.response.responseHeader.responseCode
                                    )
                                }

                            }
                        }
                        is NetworkState.Error -> {
                        }

                    }
                }
        }

    }

    fun cancelAccount(callback: () -> Unit) = viewModelScope.launch {
        if (connection()) {
            var request = BaseRequest(RequestHeaderManager.getRequestHeader())
            dataManager.cancelAccount(request)
                .collect { state: NetworkState<BaseResponse> ->
                    when (state) {
                        is NetworkState.Success -> {
                            state.response.let {
                                if (it.responseHeader.responseCode == "0") {
                                    callback.invoke()
                                } else {
                                    getPresenter()?.onTimeOutError(
                                        state.response.responseHeader.displayText,
                                        state.response.responseHeader.responseCode
                                    )
                                }

                            }
                        }
                        is NetworkState.Error -> {
                        }

                    }
                }
        }

    }

    fun validateCard(card: Card, month: String, year: String) = viewModelScope.launch {
        if (connection()) {
            var request = ValidateCardRequest()
            request.requestHeader = RequestHeaderManager.getRequestHeader()
            if (card.cardBrand == "PAYE") {
                request.cardId = card.cardId
                request.expDateMonth = ""
                request.expDateYear = ""
                request.payeUIDLast5Digits = ""
                request.cardBrand = "PAYE"

            } else {
                if (!(TextUtils.isEmpty(month) || TextUtils.isEmpty(year))) {
                    request.cardId = card.cardId
                    request.expDateMonth = month
                    request.expDateYear = year
                    request.payeUIDLast5Digits = ""
                    request.cardBrand = ""
                }
            }
            getPresenter()?.showLoading()
            dataManager.validateCard(request)
                .collect { state: NetworkState<ValidateCardResponse> ->
                    when (state) {
                        is NetworkState.Success -> {
                            state.response.let {
                                getPresenter()?.hideLoading()
                                var code = it.responseHeader.responseCode
                                var text = state.response.responseHeader.displayText

                                if (code == "0") {
                                    getPresenter()?.cardValidationId(state.response.cardValidationId)
                                } else if (code == "3025" || code == "3026" || code == "3040" || code == "1006") {
                                    if (code == "3040") {
                                        GlobalData.cardValidationId = ""
                                    }
                                    getPresenter()?.onTimeOutError(text, code)
                                } else {
                                    GlobalData.cardValidationId = ""
                                }


                            }
                        }
                        is NetworkState.Error -> {
                        }

                    }
                }
        }

    }

    fun getClientPramas() = viewModelScope.launch {
        if (connection()) {
            var request = BaseRequest(requestHeader = RequestHeaderManager.getRequestHeader())
            dataManager.postClientParams(request)
                .collect { state: NetworkState<GetClientParamsResponse> ->
                    when (state) {
                        is NetworkState.Success -> {
                            GlobalData.getClientParams.addAll(state.response.tpayParam)
                        }
                        is NetworkState.Error -> {

                        }

                    }
                }
        }

    }

    fun changePin(changePinRequest: ChangePinRequest) = viewModelScope.launch {
        if (connection()) {
            dataManager.changePin(changePinRequest)
                .collect { state: NetworkState<BaseResponse> ->
                    when (state) {
                        is NetworkState.Success -> {
                            state.response.let {
                                var code = it.responseHeader.responseCode
                                if (code == "0")
                                    getPresenter()?.succesChangePin()
                                else
                                    getPresenter()?.onTimeOutError(
                                        state.response.responseHeader.displayText,
                                        state.response.responseHeader.responseCode
                                    )
                            }

                        }
                        is NetworkState.Error -> {

                        }

                    }
                }
        }

    }

    fun logout() = viewModelScope.launch {
        if (connection()) {
            var request = BaseRequest(requestHeader = RequestHeaderManager.getRequestHeader())
            dataManager.logout(request)
                .collect { state: NetworkState<BaseResponse> ->
                    when (state) {
                        is NetworkState.Success -> {
                            getPresenter()?.succesLogout()
                        }
                        is NetworkState.Error -> {

                        }

                    }
                }
        }

    }

    fun connection(): Boolean {
        if (NetworkUtils.hasInternet(PaycellSdkApp.context)) {
            return true
        } else {
            getPresenter()?.onTimeOutError(
                "İnternet bağlantınızı kontrol ediniz",
                "0"
            )
            return false
        }
    }

}
