package com.paycell.ui.onboardingloginstep

import com.paycell.base.IBasePresenter
import com.paycell.remote.model.GetAccountResponse
import com.paycell.remote.model.ValidatePinResponse
import com.paycell.remote.model.init.InitResponse

interface ILoginStepPresenter : IBasePresenter {
    fun succesApiGateway(){}
    fun getAccountResponse(response: GetAccountResponse) {}
    fun succesGetAccountResponse() {}
    fun succesRegisterAccount() {}
    fun succesSingleRegisterAccount() {}
    fun succesPin(validatePinResponse: ValidatePinResponse) {}
    fun forgotPin() {}
    fun succesLogout() {}
    fun initResponse(initResponse: InitResponse){}
    fun cardValidationId(id:String){}
    fun succesSendOtp(lock:Boolean){}
    fun succesChangePin(){}

}