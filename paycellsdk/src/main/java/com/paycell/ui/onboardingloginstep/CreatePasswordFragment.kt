package com.paycell.ui.onboardingloginstep


import android.view.View
import com.paycell.base.BindingFragment
import com.paycell.extensions.backgroundPaycell
import com.paycell.extensions.setVectorForPreLollipop
import com.paycell.helper.PassCodeHelper
import com.paycell.remote.model.ChangePinRequest
import com.paycell.remote.model.base.GlobalData
import com.paycell.ui.login.LoginFragment
import com.paycell.ui.login.LoginFragment.Companion.CREATE_EMAIL_AND_CONDITION
import com.paycell.ui.login.LoginFragment.Companion.CREATE_PASSWORD
import com.paycell.ui.sendmoney.DetailFragment
import com.paycell.utils.RequestHeaderManager
import com.paycellsdk.R
import com.paycellsdk.databinding.FragmentCreatePasswordBinding
import org.koin.androidx.viewmodel.ext.android.viewModel


class CreatePasswordFragment() :
    BindingFragment<FragmentCreatePasswordBinding>(), ILoginStepPresenter {

    private val viewModel by viewModel<LoginStepViewModel>()

    var globalData = GlobalData()
    override val layoutBindId: Int
        get() = R.layout.fragment_create_password


    override fun initPresenter() {
        viewModel.setPresenter(this)
    }

    lateinit var fragment: LoginFragment

    companion object {
        var password: String = ""
        fun newInstance(fragment: LoginFragment): CreatePasswordFragment {
            return CreatePasswordFragment().apply {
                this.fragment = fragment
            }
        }
    }

    override fun onResume() {
        super.onResume()
        showThisPage()
        // requireActivity().window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
        LoginFragment.current_page = CREATE_PASSWORD
        if (LoginFragment.isCardValidationVerify)
            fragment.backButtonVisiblity(true)
        else
            fragment.backButtonVisiblity(false)

        if (fragment.isCardValidation() && !LoginFragment.isCardValidationVerify) {
            fragment.changePagerPosition(LoginFragment.CARD_VALIDATION)
        }
        if (fragment.getIsLogin() && !fragment.isCardValidation()&&!fragment.isPasswwordUpdate()) {
            fragment.changePagerPosition(LoginFragment.ENTRY_PASSWORD)
        }
    }

    override fun initUI(view: View) {
        binding.vm = viewModel
        binding.createPassEt.setHintAndText(hint = "6 haneli şifre belirleyin")
        binding.createPassVerifyEt.setHintAndText("Şifrenizi doğrulayın")
        binding.createPasswordTitleTv.text = "6 haneli şifre belirleyip Paycell’i kullanmaya başlayın!"
        changeNextButtonStatus()
        binding.background.backgroundPaycell()

    }

    private fun showThisPage() {
        binding.createPassEt.underLineColor(false)
        binding.createPassVerifyEt.underLineColor(false)
    }


    override fun initListener() {
        binding.btnNext.setOnClickListener {
            if (checkPassCode()) { nextPage() }
        }
    }

    fun nextPage(){
        password = binding.createPassEt.getText()
        if (fragment.isPasswwordUpdate()) {
            var changePinRequest=ChangePinRequest(requestHeader = RequestHeaderManager.getRequestHeader(),oldPin = EntryPasswordFragment.loginPassword,pin = password)
            viewModel.changePin(changePinRequest)
        } else {
            fragment.changePagerPosition(CREATE_EMAIL_AND_CONDITION)
        }

    }

    override fun succesChangePin() {
        if (GlobalData.getAccountResponse.operatorId==1) {
            fragment.mFragment.calculatePage(DetailFragment.SIM_CHECK_PROGRESS, "")
        }
        else{
            fragment.mFragment.calculatePage(DetailFragment.LOGIN_COMPLETED, "")
        }
    }

    private fun checkPassCode(): Boolean {
        if (binding.createPassEt.getText().length == 6 && binding.createPassVerifyEt.getText().length == 6 && binding.createPassEt.getText() == binding.createPassVerifyEt.getText()) {
            if (PassCodeHelper.isPassCodeValidWithWeakPin(binding.createPassEt.getText())) {
                binding.btnNext.setBackgroundDrawable(requireContext().setVectorForPreLollipop(R.drawable.back_aqua_outline))
                binding.btnNext.setTextColor(resources.getColor(R.color.charcoal_grey))
                return true
            } else {
                binding.createPassEt.setError("Şifreniz ardışık rakamlardan oluşmamalı") // Parola çok basit hatası
                binding.createPassVerifyEt.clearErrorNotActive() // Parola çok basit hatası
                binding.btnNext.setBackgroundResource(R.drawable.back_gray_outline)
                binding.btnNext.setTextColor(resources.getColor(R.color.blue_grey))
                return false
            }
        } else {
            binding.createPassVerifyEt.setError("Girdiğiniz şifreler eşleşmiyor.") // Birbirlerine eşit değil veya 4 karakter değiller
            binding.btnNext.setBackgroundResource(R.drawable.back_gray_outline)
            binding.btnNext.setTextColor(resources.getColor(R.color.blue_grey))
            return false
        }
    }

    private fun changeNextButtonStatus() {
        binding.createPassEt.listenerNormalInput {
            if (binding.createPassEt.getText().length == 6 && binding.createPassVerifyEt.getText().length == 6) {
                binding.btnNext.setBackgroundDrawable(requireContext().setVectorForPreLollipop(R.drawable.back_aqua_outline))
                binding.btnNext.setTextColor(resources.getColor(R.color.charcoal_grey))
            } else {
                binding.btnNext.setBackgroundResource(R.drawable.back_gray_outline)
                binding.btnNext.setTextColor(resources.getColor(R.color.blue_grey))
            }
        }

        binding.createPassVerifyEt.listenerNormalInput {
            if (binding.createPassVerifyEt.getText().length == 6 && binding.createPassEt.getText().length == 6) {
                binding.btnNext.setBackgroundDrawable(requireContext().setVectorForPreLollipop(R.drawable.back_aqua_outline))
                binding.btnNext.setTextColor(resources.getColor(R.color.charcoal_grey))
            } else {
                binding.btnNext.setBackgroundResource(R.drawable.back_gray_outline)
                binding.btnNext.setTextColor(resources.getColor(R.color.blue_grey))
            }
        }
    }
}