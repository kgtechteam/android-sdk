package com.paycell.base

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import com.paycell.helper.ProgressLoadingDialog

class BaseWebView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : WebView(context, attrs, defStyleAttr) {

    private var mPreviewText: String = ""
    var isOpenUrlInBrowser: Boolean = false
    var webViewUrl: String = ""
    lateinit var progress: ProgressLoadingDialog

    lateinit var progressModel: ProgressLoadingDialog.ProgreessModel

    @SuppressLint("SetJavaScriptEnabled")
    fun initialize(url: String, returnUrl: (String) -> Unit?) {
        loadingDialog()
        webViewUrl = url
        webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                    loadUrl(mPreviewText)
                } else {
                    evaluateJavascript(mPreviewText, null)
                }
                progress.release()
            }

            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                returnUrl.invoke(url)
                if (isOpenUrlInBrowser) {
                    //val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                    //mContext.startActivity(intent)
                    return true
                }
                return false
            }

        }

        loadUrl(url)
        settings.javaScriptEnabled = true
        //   settings.userAgentString="Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en) AppleWebKit/420+ (KHTML, like Gecko) Version/3.0 Mobile/1A543a Safari/419.3"
        settings.useWideViewPort = false
        settings.domStorageEnabled = true
        isHorizontalScrollBarEnabled = true
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            settings.allowUniversalAccessFromFileURLs = true
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW)
        }
    }

    fun loadingDialog() {
        progressModel = ProgressLoadingDialog.ProgreessModel(context = context, isProgress = true)
        progress = ProgressLoadingDialog(progressModel)
        progress.toggle(status = true)
    }


}