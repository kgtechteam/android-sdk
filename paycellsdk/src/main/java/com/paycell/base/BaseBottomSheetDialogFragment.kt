package com.paycell.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import com.google.android.material.bottomsheet.BottomSheetDialogFragment


abstract class BaseBottomSheetDialogFragment : BottomSheetDialogFragment() {
    @get:LayoutRes
    protected abstract val layoutId: Int?

    protected abstract fun initUI(view: View)

    protected abstract fun initListener()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (layoutId != null) {
            return inflater.inflate(layoutId!!, container, false)
        } else {
            return initBinding(inflater, container)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI(view)
        initListener()
    }

    open fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): View? {
        return null
    }
}