package com.paycell.base

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

abstract class BindingFragment<T : ViewDataBinding> : BaseFragment() {

    /*
      binding nedeniyle memory leak veya bir sıkıntı olursa;
      binding değişkenini nullable yerine aşağıdaki gibi değiştir
      ****** protected var binding: T by AutoClearedValue() ******
      sonrasında onDestroyView metdonu kaldır.
     */

    // This property is only valid between onCreateView and
    // onDestroyView.
    private var _binding: T? = null

    protected val binding get() = _binding!!

    @get:LayoutRes
    abstract val layoutBindId: Int

    override val layoutId: Int?
        get() = null

    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): View? {
        return DataBindingUtil.inflate<T>(
            inflater,
            layoutBindId,
            container,
            false
        ).apply {
            _binding = this
            lifecycleOwner = viewLifecycleOwner
        }.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}