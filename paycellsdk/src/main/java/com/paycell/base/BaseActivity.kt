package com.paycell.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import com.paycell.helper.ProgressLoadingDialog


abstract class BaseActivity : AppCompatActivity(), IBasePresenter {

    var dispatchStatus = true
    @get:LayoutRes
    protected abstract val layoutId: Int?

    protected abstract fun initPresenter()

    protected abstract fun initUI()

    protected abstract fun initListener()


    lateinit var  progress: ProgressLoadingDialog

    lateinit var progressModel:ProgressLoadingDialog.ProgreessModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (layoutId != null) {
            setContentView(layoutId!!)
        } else {
            initBinding()
        }

        initPresenter()
        initUI()
        initListener()

    }

    open fun initBinding() {

    }

    override fun showLoading() {
        progressModel= ProgressLoadingDialog.ProgreessModel(context = this,isProgress = true)
        progress= ProgressLoadingDialog(progressModel)
        progress.toggle(status = true)

    }

    override fun hideLoading() {
        progress.release()
    }

    override fun onServerError(errorMessage: String, errorCode: String) {
        progressModel= ProgressLoadingDialog.ProgreessModel(context = this,isProgress = false,title = errorMessage)
        progress= ProgressLoadingDialog(progressModel)
    }

    override fun onWarning(errorMessage: String, errorCode: String) {
        progressModel= ProgressLoadingDialog.ProgreessModel(context = this,isProgress = false,title = errorMessage,buttonText = "Devam Et")
        progress= ProgressLoadingDialog(progressModel)
    }
    override fun onTimeOutError(serviceMessage: String, errorCode: String) {
        progressModel= ProgressLoadingDialog.ProgreessModel(context = this,isProgress = false,title = serviceMessage)
        progress= ProgressLoadingDialog(progressModel)
    }

    override fun onServiceSuccess(serviceMessage: String, successCode: String) {
        /*val viewModel = SuccessDialog.ViewModel()
        viewModel.title = resources.getString(R.string.dialog_title_successfull)
        viewModel.description = serviceMessage
        DialogUtils.showSuccessDialog(viewModel, supportFragmentManager.beginTransaction())*/
    }




}