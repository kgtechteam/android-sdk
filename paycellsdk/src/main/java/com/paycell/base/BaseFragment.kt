package com.paycell.base

import android.app.Activity
import android.content.ClipData
import android.content.ClipDescription
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
 import com.paycell.helper.ProgressLoadingDialog


abstract class BaseFragment : Fragment(), IBasePresenter {

    @get:LayoutRes
    protected abstract val layoutId: Int?

    protected abstract fun initPresenter()

    protected abstract fun initUI(view: View)

    protected abstract fun initListener()


    lateinit var progress: ProgressLoadingDialog

    lateinit var progressModel: ProgressLoadingDialog.ProgreessModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        logLifecycleEvents("onCreate")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        initPresenter()
        logLifecycleEvents("onCreateView")
        if (layoutId != null) {
            return inflater.inflate(layoutId!!, container, false)
        } else {
            return initBinding(inflater, container)
        }
    }

    open fun initBinding(inflater: LayoutInflater, container: ViewGroup?): View? {
        return null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        logLifecycleEvents("onViewCreated")
        initUI(view)
        initListener()
    }

    override fun showLoading() {
        progressModel = ProgressLoadingDialog.ProgreessModel(context = requireContext(), isProgress = true)
        progress = ProgressLoadingDialog(progressModel)
        progress.toggle(status = true)
    }

    override fun hideLoading() {
        progress.release()
    }

    override fun onServerError(errorMessage: String, errorCode: String) {
        progressModel = ProgressLoadingDialog.ProgreessModel(
            context = requireContext(),
            isProgress = false,
            title = errorMessage
        )
        progress = ProgressLoadingDialog(progressModel)
    }

    override fun onTimeOutError(serviceMessage: String, errorCode: String) {
        progressModel = ProgressLoadingDialog.ProgreessModel(
            context = requireContext(),
            isProgress = false,
            title = serviceMessage
        )
        progress = ProgressLoadingDialog(progressModel)
    }

    override fun onServiceSuccess(serviceMessage: String, successCode: String) {

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        logLifecycleEvents("onAttach")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        logLifecycleEvents("onActivityCreated")
    }

    override fun onStart() {
        super.onStart()
        logLifecycleEvents("onStart")
    }

    override fun onResume() {
        super.onResume()
        logLifecycleEvents("onResume")
    }

    override fun onPause() {
        super.onPause()
        logLifecycleEvents("onPause")
    }

    override fun onStop() {
        super.onStop()
        logLifecycleEvents("onStop")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        logLifecycleEvents("onDestroyView")
    }

    override fun onDetach() {
        super.onDetach()
        logLifecycleEvents("onDetach")
    }

    private fun logLifecycleEvents(lifeCycleName: String) {
    }


    open fun readFromClipboard(context: Context): String? {
        val clipboard =
            context.getSystemService(Context.CLIPBOARD_SERVICE) as android.content.ClipboardManager
        if (clipboard.hasPrimaryClip()) {
            val description: ClipDescription? = clipboard.getPrimaryClipDescription()
            val data: ClipData? = clipboard.getPrimaryClip()
            if (data != null && description != null && description.hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) return data.getItemAt(
                0
            ).text.toString()
        }
        return null
    }

    open fun hideKeyboardFrom(context: Context, view: View) {
        val imm: InputMethodManager =
            context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
        view.clearFocus()
    }


}