package com.paycell.base

import android.app.Dialog
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import android.webkit.ConsoleMessage
import android.webkit.JavascriptInterface
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.paycell.PaycellSdkApp
import com.paycell.PaycellSdkApp.BASE_URL_WEBVIEW
import com.paycell.extensions.encode
import com.paycell.extensions.openLink
import com.paycell.remote.model.UrlRefState
import com.paycell.ui.sendmoney.DetailFragment
import com.paycell.utils.Constant
import com.paycell.utils.NetworkUtils
import com.paycell.utils.PrefUtils
import com.paycell.utils.RequestHeaderManager
import com.paycellsdk.R


abstract class BaseWebFragment : Fragment() {


    protected abstract fun initUI()

    lateinit var uri: Uri
    open lateinit var mFragment: DetailFragment

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.activity_webview, container, false)
    }

    fun initWeb(url: String, iIListener: IListener) {
        if (connection()) {
            println("GIDEN_URL " + url)
            webview = requireView().findViewById<BaseWebView>(R.id.lWebview)
            webview.initialize(url) { donenUrl ->
                println("DONEN URL " + donenUrl)
                try {
                    //connection()
                    uri = Uri.parse(donenUrl)
                    parseReference()
                    saveToken()
                    iIListener.openSplash(openSplash(donenUrl))
                    iIListener.uploadMoneyCallBack(calculateUploadMoney())
                    iIListener.ibanTransferCallBack(calculateIbanTransfer())
                    iIListener.mainCallBack(mainFailed())
                    iIListener.backToContact(backToContact(donenUrl))
                    iIListener.openQrPage(backToQr(donenUrl))
                    iIListener.openQrPage(calculateQrScan())



                } catch (e: Exception) {
                    println("DONEN_URL_HATA" + e)
                }
            }

            webview.webChromeClient = object : WebChromeClient() {
                override fun onConsoleMessage(message: ConsoleMessage): Boolean {
                    Log.e("WebviewLoglari", message.message())
                    openFaturaPage(message.message() == "FaturaOde")
                    openIstanbulkart(message.message() == "IstanbulKart")
                    openKyc(message.message() == "kycLink")
                    iIListener.backToContact(message.message() == "backToContact")
                    iIListener.openQrPage(isLoginQrOpen(message.message()))
                    iIListener.sendMoneyCallBack(calculateSendMoney(message.message()))

                    return true
                }
            }
            webview.settings.cacheMode = WebSettings.LOAD_NO_CACHE
            webview.addJavascriptInterface(JsObject(), "postMessage")
        }
    }

    fun openSplash(donenUrl: String): Boolean {
        return donenUrl.contains(NO_LOGIN_FAILED) && donenUrl.contains("reloaded=true")
    }

    fun calculateSendMoney(message: String): Boolean {
        urlRefState = UrlRefState.SEND_MONEY
        return message.contains("auth_token") && message.contains("sendmoney")
    }
    //sendmoney?auth_token=iVgzYVCAo%2BekMU1xXni4Yqsbv%2BxXEbB7u4zZxom89B4%3D&msisdn=&name=&deviceManufacturer=Apple&deviceId=F891BEB2-5269-44EE-9930-2BA65FF43B54&deviceOs=ios&deviceModel=64-bit%20Simulator&deviceOsVersion=14.4&qrCode=1000395600

    fun calculateQrScan(): Boolean {
        return uri.toString()
            .contains("failed?ref=qrOde?auth_token=") && uri.getQueryParameter("ref").toString()
            .contains("qrOde")
    }

    fun saveToken() {
        if (!uri.getQueryParameter("auth_token").isNullOrEmpty()) {
            PrefUtils.authToken = uri.getQueryParameter("auth_token").toString()
        }
    }

    fun calculateUploadMoney(): Boolean {
        return !uri.getQueryParameter("auth_token").isNullOrEmpty() && uri.encodedPath.toString()
            .contains("/uploadMoney") && uri.getQueryParameter("deviceId").isNullOrEmpty()
    }

    fun calculateIbanTransfer(): Boolean {
        return !uri.getQueryParameter("auth_token").isNullOrEmpty() && uri.encodedPath.toString()
            .contains("/ibanTransfer") && uri.getQueryParameter("deviceId").isNullOrEmpty()
    }

    fun calculateMainPage(): Boolean {
        return !uri.getQueryParameter("auth_token").isNullOrEmpty() && uri.encodedPath.toString()
            .contains("failed?reloaded=true") && uri.getQueryParameter("deviceId").isNullOrEmpty()

    }

    fun mainFailed(): Boolean {
        return uri.getQueryParameter("ref").toString().contains("mainpage?reloaded=true")

    }
    fun backToContact(url: String): Boolean {
        return url.contains("&backToContact") && urlRefState == UrlRefState.SEND_MONEY
    }

    fun backToQr(url: String): Boolean {
        return url.contains("&backToQr") && urlRefState == UrlRefState.QR_ODE
    }

    fun isLoginQrOpen(message: String): Boolean {
        urlRefState = UrlRefState.QR_ODE
        return message == "qrIleOde"
    }

    fun parseReference() {
        if (!uri.getQueryParameter("ref").isNullOrEmpty() &&
            !uri.getQueryParameter("ref").toString().contains("mainpage")
        ) {
            REFERENCE = "/" + uri.getQueryParameter("ref").toString().replace("?reloaded=true", "")
            var resultReference = uri.getQueryParameter("ref").toString()
            if (resultReference.contains("sendmoney")) {
                urlRefState = UrlRefState.SEND_MONEY
            } else if (resultReference.contains("uploadMoney")) {
                urlRefState = UrlRefState.UPLOAD_MONEY
            } else if (resultReference.contains("hazirLimit")) {
                urlRefState = UrlRefState.HAZIR_LIMIT
            } else if (resultReference.contains("qrOde")) {
                urlRefState = UrlRefState.QR_ODE
            } else if (resultReference.contains("ibanTransfer")) {
                urlRefState = UrlRefState.IBAN_TRANSFER
            }
        }
        else if(uri.toString().contains("failed")){
            urlRefState = UrlRefState.EYEICON
        }
    }

    fun openIstanbulkart(ok: Boolean) {
        if (ok) {
            requireActivity().openLink(Constant.ISTANBUL_KART)
        }

    }

    fun openFaturaPage(ok: Boolean) {
        if (ok) {
            requireActivity().openLink(Constant.FATURA_ODEME)
        }
    }

    fun openKyc(ok: Boolean) {
        if (ok) {
            requireActivity().openLink(Constant.KYC_DEEPLINK)
        }
    }

    companion object {
        lateinit var webview: BaseWebView
        var BASE_URL = BASE_URL_WEBVIEW

        // var BASE_URL = "https://websdk_test.turkcell.com.tr"
        //    var BASE_URL = "https://externalsdk.paycell.com.tr"
        //   var BASE_URL = "http://68.183.4.240:3000"
        // var BASE_URL = "http://0.0.0.0:3000"
        var DETAIL_URL = ""
        var AUTH_TOKEN = "?auth_token="
        val NO_LOGIN_FAILED = "/failed"
        var REFERENCE = ""
        var MSISDN = "&msisdn="
        var NAME = "&name="
        var QR_CODE = "&qrCode="
        var STEP_TWO = "&setStepTwo"
        var DCB_MSISDN = "&dcbMsisdn="
        var LOGIN_URL = ""
        lateinit var urlRefState: UrlRefState

        fun calculateUrlSendMoney(phone: String, name: String, qrCode: String?) {
            var resultName = if (name.isNullOrEmpty()) phone else name
            var resultQrCode = if (qrCode.isNullOrEmpty()) "" else QR_CODE + qrCode
            DETAIL_URL = BASE_URL +
                    "/sendmoney" +
                    AUTH_TOKEN +
                    PrefUtils.authToken.encode() +
                    MSISDN +
                    phone +
                    NAME +
                    resultName.encode() +
                    getHeader() + resultQrCode

            println("DETAIL_URL=> " + DETAIL_URL)
        }

        fun calculateUrlUploadMoney(isLogin: Boolean) {
            var resultStep = if (isLogin) STEP_TWO else ""
            DETAIL_URL =
                BASE_URL + "/uploadMoney" + AUTH_TOKEN + PrefUtils.authToken.encode() + getHeader() + resultStep
            println("DETAIL_URL_UPLOADMONEY=> " + DETAIL_URL)
        }

        fun calculaterIbanTransfer(isLogin: Boolean) {
            var resultStep = if (isLogin) STEP_TWO else ""
            DETAIL_URL =
                BASE_URL + "/ibanTransfer" + AUTH_TOKEN + PrefUtils.authToken.encode() + getHeader() + resultStep
            println("DETAIL_URL_IBANTRANSFER=>" + DETAIL_URL)
        }

        fun calculaterMainPage(isLogin: Boolean) {
            var resultStep = if (isLogin) STEP_TWO else ""
            DETAIL_URL =
                BASE_URL + AUTH_TOKEN + PrefUtils.authToken.encode() + getHeader() + resultStep
            println("DETAIL_URL_Failed=>" + DETAIL_URL)
        }

        fun loginUrl(): String {
            LOGIN_URL = BASE_URL + AUTH_TOKEN + PrefUtils.authToken.encode() + getHeader()
            println("LOGIN_URL=>" + LOGIN_URL)
            return LOGIN_URL
        }

        fun getHeader(): String {
            var deviceManufacturer = "&deviceManufacturer="
            var deviceId = "&deviceId="
            var deviceOs = "&deviceOs="
            var deviceModel = "&deviceModel="
            var deviceOsVersion = "&deviceOsVersion="

            var header = RequestHeaderManager.getRequestHeader().deviceInfo
            return deviceManufacturer + header?.deviceManufacturer?.encode() +
                    deviceId + header?.deviceId?.encode() +
                    deviceOs + header?.deviceOs?.encode() +
                    deviceModel + header?.deviceModel?.encode() +
                    deviceOsVersion + header?.deviceOsVersion?.encode()
        }

        fun getMainUrl(phone: String): String {
            return BASE_URL + AUTH_TOKEN + getHeader() + DCB_MSISDN + phone
        }

    }

    fun backWebview(): Boolean {
        if (webview.canGoBack()) {
            webview.goBack()
            return true
        } else {
            return false
        }
    }

    private fun connection(): Boolean {
        if (NetworkUtils.hasInternet(PaycellSdkApp.context)) {
            return true
        } else {
            showInternetDialog()
            // mFragment.calculatePage(DetailFragment.INTERNET,"")
            return false
        }
    }

    private fun showInternetDialog(callback: (() -> Unit?)? = null) {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_progress_message)
        val tvDescription = dialog.findViewById(R.id.descriptionSuspendedWarningTv) as TextView
        val ok = dialog.findViewById(R.id.renewPasswordBtnSuspendedWarning) as Button
        val close = dialog.findViewById(R.id.closeBtnSuspendedWarningIv) as ImageView

        ok.setOnClickListener {
            dialog.dismiss()
            callback?.let {
                it.invoke()
            }
        }

        close.setOnClickListener {
            dialog.dismiss()
            callback?.let {
                it.invoke()
            }
        }
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        dialog.window?.attributes = lp
        dialog.show()

    }

    class JsObject {
        @JavascriptInterface // must be added for API 17 or higher
        fun postMessage(toast: String?) {
            println("CUMA_SOYAK" + toast)
        }
    }

    interface IListener {
        fun openSplash(ok: Boolean) {}
        fun sendMoneyCallBack(ok: Boolean) {}
        fun uploadMoneyCallBack(ok: Boolean) {}
        fun ibanTransferCallBack(ok: Boolean) {}
        fun mainCallBack(ok: Boolean) {}
        fun backToContact(ok: Boolean) {}
        fun openQrPage(ok: Boolean) {}
        fun openEye(ok: Boolean) {}

    }
}