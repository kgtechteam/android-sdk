package com.paycell.base


interface IBasePresenter {
    fun showLoading()
    fun hideLoading()
    fun onServerError(errorMessage: String, errorCode: String)
    fun onWarning(errorMessage: String, errorCode: String){}
    fun onTimeOutError(serviceMessage: String, errorCode: String)
    fun onServiceSuccess(serviceMessage: String, successCode: String)
    fun onSpecialError(message: String, type: String){}

}