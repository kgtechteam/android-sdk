package com.paycell.base

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.BoolRes
import androidx.annotation.LayoutRes
import androidx.fragment.app.DialogFragment
 import com.paycell.helper.ProgressLoadingDialog
import com.paycellsdk.R

@SuppressLint("SupportAnnotationUsage")
abstract class BaseDialogFragment : DialogFragment(), IBasePresenter {

    @get:LayoutRes
    protected abstract val layoutId: Int?

    @get:BoolRes
    abstract val fullScreen: Boolean

    protected abstract fun initUI()

    protected abstract fun initListener()

    protected abstract fun initPresenter()

    lateinit var  progress: ProgressLoadingDialog

    lateinit var progressModel: ProgressLoadingDialog.ProgreessModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (fullScreen) {
            setStyle(STYLE_NO_TITLE, R.style.Widget_Paycell_Dialog_FullScreen)
        } else {
            setStyle(STYLE_NO_TITLE, R.style.Widget_Paycell_Dialog)
        }
    }

    override fun onStart() {
        super.onStart()
        if (fullScreen) {
            val searchDialog: Dialog? = dialog
            if (searchDialog != null) {
                val width = ViewGroup.LayoutParams.MATCH_PARENT
                val height = ViewGroup.LayoutParams.MATCH_PARENT
                searchDialog.window?.setLayout(width, height)
            }
        }

    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return if (layoutId != null) {
            inflater.inflate(layoutId!!, container, false)
        } else {
            initBinding(inflater, container)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initPresenter()
        initUI()
        initListener()
    }

    open fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): View? {
        return null
    }

    override fun showLoading() {
        progressModel= ProgressLoadingDialog.ProgreessModel(context = requireContext(),isProgress = true )
        progress= ProgressLoadingDialog(progressModel)
        progress.toggle(status = true)
    }

    override fun hideLoading() {
        progress.release()

    }

    override fun onServerError(errorMessage: String, errorCode: String) {
        progressModel= ProgressLoadingDialog.ProgreessModel(context =requireContext(),isProgress = false,title = errorMessage)
        progress= ProgressLoadingDialog(progressModel)
    }

    override fun onTimeOutError(serviceMessage: String, errorCode: String) {
        progressModel= ProgressLoadingDialog.ProgreessModel(context = requireContext(),isProgress = false,title = serviceMessage)
        progress= ProgressLoadingDialog(progressModel)
    }

    override fun onServiceSuccess(serviceMessage: String, successCode: String) {

    }


}