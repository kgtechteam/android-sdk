package com.paycell.extensions

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat
import java.util.*

fun Double.formatToTl(): String {
    val numberFormat = NumberFormat.getCurrencyInstance()
    numberFormat.currency = Currency.getInstance("TRY")
    numberFormat.minimumFractionDigits = 0
    numberFormat.maximumFractionDigits = 3

    return numberFormat.format(this)
}

fun Double.formatToBtc(): String {
    val numberFormat = NumberFormat.getCurrencyInstance()
    numberFormat.currency = Currency.getInstance("USD")
    numberFormat.minimumFractionDigits = 8
    numberFormat.maximumFractionDigits = 8
    return numberFormat.format(this).replace(',', '.')
}

fun Double.amountString(currency: String = "TL"): String {
    val symbols = DecimalFormatSymbols(Locale("tr"))
    symbols.decimalSeparator = ','
    symbols.groupingSeparator = '.'

    val formatter = DecimalFormat("#,##0.00")
    val fractionDigits = when (this.rem(1.00)) {
        0.00 -> 0
        else -> 2
    }
    formatter.maximumFractionDigits = fractionDigits
    formatter.minimumFractionDigits = fractionDigits
    formatter.groupingSize = 3
    formatter.decimalFormatSymbols = symbols

    val builder = StringBuilder()
    builder.append(formatter.format(this))
    builder.append(" ")
    builder.append(currency)

    return builder.toString()
}

fun Double.formatMoney(): String {
    val symbols = DecimalFormatSymbols.getInstance(Locale("tr", "TR"))
    symbols.groupingSeparator = '.'
    symbols.decimalSeparator = ','
    val df = DecimalFormat("#,##", symbols)
    df.groupingSize = 3
    df.isGroupingUsed = true
    df.maximumFractionDigits = 2
    df.minimumFractionDigits = 2
    if (this == 0.0) {
        return "0,00"
    } else {
        return df.format(this)
    }
}

fun Double.formatMoney2(): String {
    val otherSymbols = DecimalFormatSymbols(Locale.US)
    otherSymbols.decimalSeparator = '.'
    var newMoney = DecimalFormat("0.00", otherSymbols).format(this)
    newMoney = newMoney.replace(".", ",")

    if (this >= 1000) {
        val second = newMoney.substring(newMoney.length - 6, newMoney.length)
        val first = newMoney.substring(0, newMoney.length - 6)
        newMoney = "$first.$second"
    }
    return "$newMoney"
}