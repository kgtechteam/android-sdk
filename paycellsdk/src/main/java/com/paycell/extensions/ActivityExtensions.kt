package com.paycell.extensions


import android.app.Activity
import android.content.Context
import android.view.inputmethod.InputMethodManager
import android.widget.Toast

fun Activity.getString(id: Int): String {
    return resources.getString(id)
}

fun Activity.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}


fun Activity.longToast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

fun Activity.hideKeyboard() {
    val service = this.getSystemService(Context.INPUT_METHOD_SERVICE)
    val inputManager = service as InputMethodManager
    this.currentFocus?.let { focusedView ->
        val windowToken = focusedView.windowToken
        inputManager.hideSoftInputFromWindow(windowToken, 0)
    }
}