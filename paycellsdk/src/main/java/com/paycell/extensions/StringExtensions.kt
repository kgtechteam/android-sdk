package com.paycell.extensions

import android.os.Build
import android.text.Html
import android.text.Spanned
import androidx.annotation.RequiresApi
import java.net.MalformedURLException
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
 import android.graphics.Color
import android.telephony.PhoneNumberUtils
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import com.paycell.utils.PrefUtils
import java.net.URLEncoder

fun String.toSpanned(): Spanned {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY)
    } else {
        Html.fromHtml(this)
    }
}

fun String.toTurkishLira(): String {
    return "₺$this"
}

fun String.clearSpaces(): String {
    return this.replace("\\s+".toRegex(), "")
}

val String.isPhone get() = matches("^[0-9+]*$".toRegex())

fun String.IbanFormatted(): String {
    val strings: MutableList<String> =
        ArrayList()
    var index = 0
    while (index < this.length) {
        strings.add(this.substring(index, Math.min(index + 4, this.length)))
        index += 4
    }

    var result = ""
    for (s in strings) {
        result += "$s "
    }
    return result
}

fun String.toMyDate(): String {
    var date = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").parse(this)
    return SimpleDateFormat("dd.MM.yyyy - HH:mm", Locale("tr", "TR")).format(
        date
    ).toString()

}

fun String.encode():String{
    return URLEncoder.encode(this, "utf-8")
}

fun String.toDate(): String {
    var date = SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(this)
    return SimpleDateFormat("dd MMMM yyyy", Locale("tr", "TR")).format(date)
        .toString()
}

fun String.toDateFormat(): String {
    var date = SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(this)
    return SimpleDateFormat("dd.MM.yyyy", Locale("tr", "TR")).format(date)
        .toString()
}

fun String.toDateTimeFormat(): String {
    var date = SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(this)
    return SimpleDateFormat("dd.MM.yyyy hh:mm", Locale("tr", "TR")).format(date)
        .toString()
}

fun String.toDateMonthDay(): String {
    var date = SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(this)
    return SimpleDateFormat("dd MMMM", Locale("tr", "TR")).format(date)
        .toString()
}

fun String.toDateYear(): String {
    var date = SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(this)
    return SimpleDateFormat("yyyy", Locale("tr", "TR")).format(date).toString()
}

fun String.toHours(): String {
    var date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(this)
    return SimpleDateFormat("HH:mm", Locale("tr", "TR")).format(date).toString()
}

fun String.getHour(): String {
    var date = SimpleDateFormat("HH:mm").parse(this)
    return SimpleDateFormat("HH", Locale("tr", "TR")).format(date).toString()
}

fun String.onlyDigits(): String = replace(Regex("\\D*"), "")

/**
 * Using UTF-8 encoding
 */
val String.base64Decoded
    @RequiresApi(Build.VERSION_CODES.O)
    get() = try {
        String(Base64.getDecoder().decode(toByteArray()))
    } catch (e: IllegalArgumentException) {
        null
    }

/**
 * Using UTF-8 encoding
 */
val String.base64Encoded: String
    @RequiresApi(Build.VERSION_CODES.O)
    get() = Base64.getEncoder().encodeToString(toByteArray())

/**
 * Splits by spaces, newlines, and tabs only
 */
val String.camelCased: String
    get() {
        val split = toLowerCase().split(' ', '\n', '\t').toMutableList()
        if (split.size > 1) {
            for (i in 1..split.size - 1) {
                if (split[i].length > 1) {
                    val charArray = split[i].toCharArray()
                    charArray[0] = charArray[0].toUpperCase()
                    split[i] = String(charArray)
                }
            }
        }
        return split.joinToString("")
    }

val String.containsLetters get() = matches(".*[a-zA-Z].*".toRegex())

val String.containsNumbers get() = matches(".*[0-9].*".toRegex())

/**
 * Does not allow whitespace or symbols
 * Allows empty string
 */
val String.isAlphanumeric get() = matches("^[a-zA-Z0-9]*$".toRegex())

/**
 * Does not allow whitespace or symbols
 * Allows empty string
 */
val String.isAlphabetic get() = matches("^[a-zA-Z]*$".toRegex())

/**
 * Does not allow whitespace or symbols
 * Allows empty string
 */
fun String.isNumeric() = matches("^[0-9]*$".toRegex())

val String.isEmail get() = matches("[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}".toRegex())

/**
 * Only matches URLs starting with a scheme (e.g. http://, file://, ftp://, etc)
 */
val String.isUrl
    get() = try {
        URL(this) != null
    } catch (e: MalformedURLException) {
        false
    }

/**
 * If there is more than one most common character, this returns the character that occurred first in the String
 */
val String.mostCommonCharacter: Char?
    get() {
        if (length == 0) return null
        val map = HashMap<Char, Int>()
        for (char in toCharArray()) map.put(char, (map[char] ?: 0) + 1)
        var maxEntry = map.entries.elementAt(0)
        for (entry in map) maxEntry =
            if (entry.value > maxEntry.value) entry else maxEntry
        return maxEntry.key
    }

fun reformatPhoneNumber(phone: String): String {
    // (555) 555-5555 to 55555555
    return phone.replace("[+[0-9]]".toRegex(), "");
}
fun isEmailValid(email: String): Boolean {
    return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
}

fun String.spanableText(findedString:String) {
    val spannable = SpannableString(this)
    spannable.setSpan(
        ForegroundColorSpan(Color.RED),
        36, // start
        44, // end
        Spannable.SPAN_EXCLUSIVE_INCLUSIVE
    )
}
fun String.phoneNumber():String{
    return "("+this.substring(0,3)+") "+ this.substring(3,6)+" "+this.substring(6,8)+" "+this.substring(8,10)
}
fun String.formatedNumber(): String {
    return PhoneNumberUtils.formatNumber(this, Locale.getDefault().country)
}
