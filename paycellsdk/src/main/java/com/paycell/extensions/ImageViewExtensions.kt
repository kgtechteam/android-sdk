package com.paycell.extensions

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.widget.ImageView
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.graphics.drawable.TintAwareDrawable
import androidx.fragment.app.FragmentActivity
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat

import com.paycellsdk.R


fun ImageView.tintSrc(@ColorRes colorRes: Int) {
    val drawable = DrawableCompat.wrap(drawable)
    DrawableCompat.setTint(drawable, ContextCompat.getColor(context, colorRes))
    setImageDrawable(drawable)
    if (drawable is TintAwareDrawable) invalidate() // Because in this case setImageDrawable will not call invalidate()
}




fun Context.setVectorForPreLollipop(resourceId:Int): VectorDrawableCompat? {

    return VectorDrawableCompat.create(this.resources, resourceId, this.theme)
}
