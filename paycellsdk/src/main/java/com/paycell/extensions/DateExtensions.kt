package com.paycell.extensions

import android.text.format.DateUtils
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


fun Date.toString(format: String): String = SimpleDateFormat(format).format(this)


fun Date.isToday(): Boolean {
    return DateUtils.isToday(this.time)
}

fun Date.isYesterday(): Boolean {
    return DateUtils.isToday(this.time + DateUtils.DAY_IN_MILLIS)
}

fun Date.dMMMM(): String {
    return SimpleDateFormat("d MMMM", Locale("tr", "TR"))
        .format(this)
}

fun Date.HHmm(): String {
    return SimpleDateFormat("HH:mm", Locale("tr", "TR"))
        .format(this)
}

fun Date.ddMMMMyyyy(): String {
    return SimpleDateFormat("dd MMMM yyyy", Locale("tr", "TR"))
        .format(this)
}

fun otpDateFormat(): SimpleDateFormat {
       return SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
}
fun getCurrentYear()=Calendar.getInstance().get(Calendar.YEAR)


fun getTimerOtp(millisUntilFinished: Long):String {
    val text = String.format(
        Locale.getDefault(), "%02d : %02d",
        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
        (TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
            TimeUnit.MILLISECONDS.toMinutes(
                millisUntilFinished
            )
        ))
    )
    return text
}
val transactionDateFormat by lazy { SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.getDefault()) }
