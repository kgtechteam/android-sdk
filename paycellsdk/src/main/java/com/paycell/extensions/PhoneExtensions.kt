package com.paycell.extensions

import android.content.Context
import android.net.Uri
import android.telephony.PhoneNumberUtils
import android.text.TextUtils
import android.util.Log
import androidx.core.content.ContextCompat
import com.github.tamir7.contacts.Contact
import com.github.tamir7.contacts.Contacts
import com.github.tamir7.contacts.PhoneNumber
import com.google.android.material.internal.ViewUtils
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.paycellsdk.R
import com.paycell.helper.ListItemViewModel
import com.paycell.helper.qrcode.toStringOrEmpty
import com.paycell.utils.Other.getJsonDataFromAsset
import java.math.BigDecimal
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*
import java.util.Arrays.asList
import kotlin.collections.ArrayList


fun Context.tamirCalucalate(): ArrayList<ContactItemViewState> {
    val resultList: ArrayList<ContactItemViewState> = ArrayList()
    var contactItem: ContactItemViewState? = null
   // Contacts.getQuery().find()
    Contacts.getQuery().find().forEachIndexed { index, contact ->
        contact.phoneNumbers.forEachIndexed { index, phoneNumber ->
            contactItem = ContactItemViewState(
                  number = phoneNumber.number.serviceNumber(),
                  name = contact.displayName.displayName().nameOrNumber(),shortName = initials(contact.givenName,contact.familyName)
            )
            resultList.add(contactItem!!)

        }
        if (contact.phoneNumbers.size == 0) {
            contactItem = ContactItemViewState(number = "", name = contact.displayName.displayName(),shortName = initials(contact.givenName,contact.familyName))
            resultList.add(contactItem!!)
        }

    }
    return resultList.calculate()
}

fun getJsonPhone():List<Contact>{
    val gson = GsonBuilder().create()
    return  gson.fromJson<ArrayList<Contact>>(getJsonDataFromAsset("sim.json"), object :TypeToken<ArrayList<Contact>>(){}.type)
}
fun ArrayList<ContactItemViewState>.calculate(): ArrayList<ContactItemViewState> {
    val resultList: ArrayList<ContactItemViewState> = ArrayList()
    val contactList: ArrayList<String> = ArrayList()
    var name = ""
    var shortName = ""
    var number = ""
    this.groupBy { it.name }.values.forEachIndexed { index, list ->
        list.forEachIndexed { index, contactItemViewState ->
            var isEmpty = contactList.filter { it.equals(contactItemViewState.number) }.isEmpty()
            if (isEmpty) {
                contactList.add(contactItemViewState.number)
            }
            name = contactItemViewState.name
            shortName = contactItemViewState.shortName
            if (index == 0) {
                number = contactItemViewState.number
            }
        }
        var contactModel = ContactItemViewState(number = number, name = name,shortName = shortName)
        contactModel.numberArray?.addAll(contactList)
        resultList.add(contactModel)
        contactList.clear()
        name = ""
        shortName=""
        number = ""
    }
    return resultList
}

class ContactItemViewState(
    var number: String,
    var name: String,
    var numberArray: ArrayList<String>? = arrayListOf(),
    var shortName:String
) : ListItemViewModel() {

    fun formatedNumber(): String {
        return PhoneNumberUtils.formatNumber(number, Locale.getDefault().country) ?: ""
    }


}

fun List<String>.firstCharecterCalucalte(position: Int): String {
    if (!this[position].isNullOrEmpty())
        return this[position].substring(0, 1).toUpperCase()
    else
        return ""
}

fun String.serviceNumber(): String {
    if (this.clearSpaces().length >= 10) {
        var length = this.clearSpaces().length
        return this.clearSpaces().substring(length - 10, length)
    } else return this

}

fun String.nameOrNumber(): String {
    if (this.equals("")) {
        return ""
    } else if (this.clearSpaces().isNumeric())
        return ""
    else
        return this
}

fun String?.displayName(): String {
    if (this.isNullOrEmpty())
        return ""
    else
        return this
}

fun formatNumber(number:String):String {
    if (number.length != 10)
    {
        return number
    }
    return (number.substring(0, 3) + " " +
            number.substring(3, 6) + " " +
            number.substring(6, 8) + " " +
            number.substring(8, 10))
}


fun getFirstValidNumber(contact:List<PhoneNumber>):String {
    contact.forEachIndexed { index, s ->
        val num = getValidPhoneNumberString(s)
        if (!TextUtils.isEmpty(num))
        {
            return num
        }
    }
    return ""
}

fun getValidPhoneNumberString(phoneNumber: PhoneNumber):String {
    if (phoneNumber == null)
    {
        return ""
    }
    var num :String= if (TextUtils.isEmpty(phoneNumber.getNormalizedNumber())) phoneNumber.getNumber() else phoneNumber.getNormalizedNumber()
    num = trimPhoneNumber(num)
    return if (isPhoneNumberValid(num)) num else ""
}

fun trimPhoneNumber(phoneNumber:String):String {
    var phoneNumber=phoneNumber
    if (!TextUtils.isEmpty(phoneNumber))
    {
        phoneNumber = phoneNumber.trim({ it <= ' ' })
        if (phoneNumber.startsWith("+90"))
        {
            return phoneNumber.replace("+90", "")
        }
        else if (phoneNumber.startsWith("0"))
        {
            return phoneNumber.substring(1)
        }
    }
    return phoneNumber
}

fun isPhoneNumberValid(number:String):Boolean {
    var isValidNumber = false
    if (number.length != 10)
    {
        return isValidNumber
    }
    val numberCodes = asList("501", "505", "506", "507", "551", "552", "553", "554", "555",
        "559", "530", "531", "532", "533", "534", "535", "536", "537", "538", "539", "561",
        "540", "541", "542", "543", "544", "545", "546", "547", "548", "549")
    for (i in 0 until numberCodes.size)
    {
        if (number.substring(0, 3).equals(numberCodes.get(i), ignoreCase = true))
        {
            isValidNumber = true
            break
        }
    }
    return isValidNumber
}



fun phoneValidationList():ArrayList<String>{
   return arrayListOf(
        "501", "505", "506", "507", "551", "552", "553", "554", "555",
        "559", "530", "531", "532", "533", "534", "535", "536", "537", "538", "539", "561",
        "540", "541", "542", "543", "544", "545", "546", "547", "548", "549"
    )
}


val notAlphanumericRegex = "[^a-zA-ZğüşöçİĞÜŞÖÇ]".toRegex()

fun initials(name: String?, surname: String?): String {
    if (name.isNullOrBlank() || surname.isNullOrBlank()) { // 2.1, 3.7 cases
        val fullName = name.toStringOrEmpty() + surname.toStringOrEmpty()
        return if (fullName.indexOfFirstNonAlphanumericInitial() == 0) String() else fullName.initial(true)
    }

    if (name.hasNonAlphanumericInitial().and(name.initial().isEmpty())) { // 3.1
        return String()
    }
    return name.initial() + surname.initial(true)
}

fun String.hasNonAlphanumericInitial() = indexOfFirstNonAlphanumericInitial() != -1

private fun String.indexOfFirstNonAlphanumericInitial() = rawInitialList().indexOfFirst { it.contains(notAlphanumericRegex) }
private fun String.initial(surname: Boolean = false) = filteredInitialList().let { if (surname) it.firstOrNull() else it.lastOrNull() }.toStringOrEmpty()
private fun String.filteredInitialList() = rawInitialList().map { it.replace(notAlphanumericRegex, "").toUpperCase() }.filter { it.isNotBlank() }
private fun String.rawInitialList() = split(" ").filter { it.isNotBlank() }.map { it.first().toString() }
