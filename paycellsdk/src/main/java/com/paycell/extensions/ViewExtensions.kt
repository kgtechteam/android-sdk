package com.paycell.extensions

import android.os.SystemClock
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

fun ViewGroup.convertDpiToPx(dpi: Int): Int {
    var pixel = 0f
    try {
        val r = context.resources
        pixel = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, dpi.toFloat(),
            r.displayMetrics
        )
    } catch (e: Exception) {
        e.printStackTrace()
    }


    return pixel.toInt()
}

fun ViewGroup.inflate(layoutRes: Int): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, false)
}

fun View.clickWithDebounce(debounceTime: Long = 600L, action: () -> Unit) {
    this.setOnClickListener(object : View.OnClickListener {
        private var lastClickTime: Long = 0

        override fun onClick(v: View) {
            if (SystemClock.elapsedRealtime() - lastClickTime < debounceTime) return
            else action()

            lastClickTime = SystemClock.elapsedRealtime()
        }
    })
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}