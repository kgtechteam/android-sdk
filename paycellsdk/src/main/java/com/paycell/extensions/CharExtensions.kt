package com.paycell.extensions

fun Char.isPlaceHolder(): Boolean = this == '#'