package com.paycell.extensions

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.Typeface
import android.net.ConnectivityManager
import android.net.Uri
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.paycell.PaycellSdkApp
import com.paycell.helper.ListItemViewModel
import com.paycellsdk.R

fun Context.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}


fun Context.longToast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}


val Context.isConnected: Boolean
    get() {
        return (getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)
            .activeNetworkInfo?.isConnected == true
    }


fun Context.color(colorId: Int) = ContextCompat.getColor(this, colorId)

fun Context.drawable(drawableId: Int) = ContextCompat.getDrawable(this, drawableId)

fun Context.dimen(dimenId: Int) = this.resources.getDimension(dimenId)

fun Context.int(intId: Int) = this.resources.getInteger(intId)

fun Context.font(fontId: Int): Typeface = try {
    ResourcesCompat.getFont(this, fontId)!!
} catch (e: Resources.NotFoundException) {
    Typeface.DEFAULT
}

fun Context.statusBarHeight(): Int? {
    val resourceId = this.resources.getIdentifier("status_bar_height", "dimen", "android")
    return if (resourceId > 0) this.resources.getDimensionPixelSize(resourceId) else null
}

fun Context.actionBarHeight(): Int {
    val styledAttributes =
        this.theme.obtainStyledAttributes(intArrayOf(android.R.attr.actionBarSize))
    val actionBarHeight = styledAttributes.getDimension(0, 0f).toInt()
    styledAttributes.recycle()
    return actionBarHeight
}

tailrec fun Context.activity(): Activity? = when (this) {
    is Activity -> this
    else -> (this as? ContextWrapper)?.baseContext?.activity()
}

fun FragmentActivity.openLink(url:String) {
    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
    this.startActivity(intent)
}

fun Activity.showKeyboard(view: View) {
    this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
}

fun Context.showKeyboard(view: View) {
    (this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).showSoftInput(
        view,
        InputMethodManager.SHOW_IMPLICIT
    )
}

fun Context.hideKeyboard(view: View) {
    (this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(
        view.windowToken, 0
    )
}

fun View.showSoftKeyboard(force: Boolean = false) {
    val inputMethodManager =
        context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    if (force) {
        inputMethodManager.toggleSoftInput(
            InputMethodManager.SHOW_FORCED,
            InputMethodManager.HIDE_IMPLICIT_ONLY
        )
    }

    val hasHardKeyboard = true
    val hardKeyboardInUse = true
    if (hasHardKeyboard && !hardKeyboardInUse) {
        inputMethodManager.showSoftInput(this, InputMethodManager.SHOW_FORCED)
    } else if (!hasHardKeyboard) {
        inputMethodManager.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
    }
}


class ContactPopup(var number: String, var serviceNumber: String) : ListItemViewModel()


fun Activity.getPermission(permission: String, requestCode: Int): Boolean {
    var permissionGranted = this.hasPermission(permission)
    if (!permissionGranted) {
        requestPermissions(this, arrayOf(permission), requestCode)
    }
    return permissionGranted
}

fun Activity.requestPermission(permission: String, requestCode: Int) {
    requestPermissions(this, arrayOf(permission), requestCode)
}

fun Context.hasPermission(permission: String): Boolean {
    return ActivityCompat.checkSelfPermission(
        this, permission
    ) == PackageManager.PERMISSION_GRANTED

}

fun Activity.showGallery(code: Int) {
    var photoPickerIntent = Intent(Intent.ACTION_PICK);
    photoPickerIntent.type = "image/*";
    this.startActivityForResult(photoPickerIntent, code);
}

fun Fragment.showGallery(code: Int) {
    var photoPickerIntent = Intent(Intent.ACTION_PICK);
    photoPickerIntent.type = "image/*";
    this.startActivityForResult(photoPickerIntent, code);
}

fun View.backgroundPaycell() {
    this.setBackgroundDrawable(PaycellSdkApp.context.setVectorForPreLollipop(R.drawable.ic_paycell_background_white))

}


