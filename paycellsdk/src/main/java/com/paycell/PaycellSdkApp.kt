package com.paycell

import android.annotation.SuppressLint
import android.content.Context
import com.github.tamir7.contacts.Contacts
import com.paycell.di.appModule
import com.paycell.utils.PrefUtils
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin


@SuppressLint("StaticFieldLeak")
object PaycellSdkApp {

    lateinit var secureCode: String
    lateinit var context: Context
    var BASE_URL = ""
    var API_GATEWAY_BASE_URL = ""
    var BASE_URL_WEBVIEW = ""
    var isBaseUrlApiGateway = false

    @JvmStatic
    fun init(context: Context, mode: SdkRunMode) {
        modes(mode)
        this.context = context
        configureDi()
        PrefUtils.init(context)
        Contacts.initialize(context)
    }

    private fun configureDi() = startKoin {
        androidLogger()
        androidContext(context)
        androidFileProperties()
        modules(appModule)

    }

    fun modes(mode: SdkRunMode) {
        when (mode) {
            SdkRunMode.DEV -> {
                BASE_URL = "https://omccstb.turkcell.com.tr/"
                secureCode = "3132333435"
                BASE_URL_WEBVIEW = "https://externalsdktest.turkcell.com.tr"
            }
            SdkRunMode.PROD -> {
                BASE_URL = "https://services.paycell.com.tr/"
                secureCode = "6c62706b5a33487931795665797a70766b626b554256574535564245784d4736"
                BASE_URL_WEBVIEW = "https://externalsdk.paycell.com.tr"
            }
            SdkRunMode.PROD68 -> {
                BASE_URL = "https://services.paycell.com.tr/"
                secureCode = "6c62706b5a33487931795665797a70766b626b554256574535564245784d4736"
                BASE_URL_WEBVIEW = "http://68.183.4.240:3000"
            }
        }
    }

    @JvmStatic
    fun logout() {
        PrefUtils.authToken = ""
    }
}

enum class SdkRunMode { DEV, PROD, PROD68 }
