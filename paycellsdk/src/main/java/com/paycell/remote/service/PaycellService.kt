package com.paycell.remote.service

import com.paycell.remote.model.*
import com.paycell.remote.model.base.BaseRequest
import com.paycell.remote.model.base.BaseResponse
import com.paycell.remote.model.init.InitRequest
import com.paycell.remote.model.init.InitResponse
import retrofit2.http.*

interface PaycellService {

    @POST("tpay/paycell/services/sessionextsdkrs/sessionServiceExtSdk/forwardMsisdn/")
    suspend fun forwardMsisdn(@Header("x-msisdn") msisdn: String): ForwardMsisdnResponse

    @POST
    suspend fun forwardMsisdnProd(@Header("x-msisdn") msisdn: String, @Url url: String): ForwardMsisdnResponse

    @POST("tpay/paycell/services/sessionextsdkrs/sessionServiceExtSdk/init")
    suspend fun init(@Body initRequest: InitRequest): InitResponse

     @POST("tpay/paycell/services/sessionextsdkrs/sessionServiceExtSdk/getMsisdnByForwardId")
    suspend fun getMsisdnByForwardId(@Body getMsisdnByForwardIdRequest: GetMsisdnByForwardIdRequest): GetMsisdnByForwardIdResponse

    @POST("tpay/paycell/services/sessionextsdkrs/sessionServiceExtSdk/getClientParams")
    suspend fun getClientParams(@Body baseRequest: BaseRequest): GetClientParamsResponse

    @POST("tpay/paycell/services/sessionextsdkrs/sessionServiceExtSdk/sendOtp")
    suspend fun sendOtp(@Body sendOtpRequest: SendOtpRequest): SendOtpResponse

    @POST("tpay/paycell/services/customerextsdkrs/customerServiceExtSdk/getAccount")
    suspend fun getAccount(@Body getAccountRequest: GetAccountRequest): GetAccountResponse

    @POST("tpay/paycell/services/customerextsdkrs/customerServiceExtSdk/getAccountDetail/")
    suspend fun getAccountDetail(@Body getAccountDetailRequest: GetAccountDetailRequest): GetAccountDetailResponse

    @POST("tpay/paycell/services/sessionextsdkrs/sessionServiceExtSdk/checkAuthState/")
    suspend fun checkAuthState(): CheckAuthStateResponse

    @POST("tpay/paycell/services/sessionextsdkrs/sessionServiceExtSdk/validateOtp")
    suspend fun validateOtp(@Body validateOtpRequest: ValidateOtpRequest): ValidateOtpResponse

    @POST("tpay/paycell/services/customerextsdkrs/customerServiceExtSdk/registerAccount")
    suspend fun register(@Body validateOtpRequest: RegisterRequest): RegisterResponse

    @POST("tpay/paycell/services/customerextsdkrs/customerServiceExtSdk/createSingleAccount")
    suspend fun createSingleAccount(@Body createSingleAccountRequest: CreateSingleAccountRequest): BaseResponse

    @POST("tpay/paycell/services/sessionextsdkrs/sessionServiceExtSdk/validatePin")
    suspend fun login(@Body validatePinRequest: ValidatePinRequest): ValidatePinResponse

    @POST("tpay/paycell/services/customerextsdkrs/customerServiceExtSdk/sendEmailValidation")
    suspend fun sendValidationEmail(@Body sendValidationEmailRequest: SendValidationEmailRequest): SendValidationEmailResponse

    @POST("tpay/paycell/services/customerextsdkrs/customerServiceExtSdk/forgotPin/")
    suspend fun forgotPin(@Body forgotPinRequest: ForgotPinRequest): ForgotPinResponse

    @POST("tpay/paycell/services/sessionextsdkrs/sessionServiceExtSdk/logout/")
    suspend fun logout(@Body baseRequest: BaseRequest): BaseResponse

    @POST("tpay/paycell/services/sessionextsdkrs/sessionServiceExtSdk/getCards/")
    suspend fun getCards(@Body baseRequest: BaseRequest): CardsResponse

    @POST("tpay/paycell/services/customerextsdkrs/customerServiceExtSdk/cancelAccount")
    suspend fun cancelAccount(@Body baseRequest: BaseRequest): BaseResponse

    @POST("tpay/paycell/services/sessionextsdkrs/sessionServiceExtSdk/validateCard")
    suspend fun validateCard(@Body validateCardRequest: ValidateCardRequest): ValidateCardResponse

    @POST("tpay/paycell/services/customerextsdkrs/customerServiceExtSdk/changePin/")
    suspend fun changePin(@Body changePinRequest: ChangePinRequest): BaseResponse

    @POST("8443/auth/oauth/v2/token")
    suspend fun apiGateway(@Body apiGatewayRequest: ApiGatewayRequest): ApiGatewayResponse

}