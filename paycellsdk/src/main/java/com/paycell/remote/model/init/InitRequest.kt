package com.paycell.remote.model.init

import com.paycell.remote.model.base.RequestHeader
 import com.google.gson.annotations.SerializedName


data class InitRequest(
    @SerializedName("requestHeader")
    val requestHeader: RequestHeader?,
    @SerializedName("connectionType")
    var connectionType: String,
    @SerializedName("forwardId")
    var forwardId: String?=null,
    @SerializedName("hashData")
    var hashData: String,
    @SerializedName("language")
    var language: String
)