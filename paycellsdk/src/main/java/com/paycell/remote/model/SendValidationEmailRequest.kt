package com.paycell.remote.model

import android.os.Parcelable
import com.paycell.remote.model.base.RequestHeader
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SendValidationEmailRequest(@SerializedName("requestHeader") val requestHeader: RequestHeader?) : Parcelable