package com.paycell.remote.model

import android.os.Parcelable
import com.paycell.remote.model.base.ResponseHeader
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ValidatePinResponse(
    @SerializedName("responseHeader") val responseHeader: ResponseHeader,
    @SerializedName("pinValidationId") val pinValidationId: String,
    @SerializedName("remainingTryCount") val remainingTryCount: String
) : Parcelable