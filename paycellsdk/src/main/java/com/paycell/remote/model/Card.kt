package com.paycell.remote.model


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.paycell.helper.ListItemViewModel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Card(
    @SerializedName("cardId") val cardId: String,
    @SerializedName("cardNumber") val cardNumber: String,
    @SerializedName("isChecked") val isChecked: Boolean,
    @SerializedName("cardBrand") val cardBrand: String=""
) : Parcelable ,ListItemViewModel()