package com.paycell.remote.model.base

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Merchant(
    @SerializedName("merchantCode") var merchantCode: String?=null,
    @SerializedName("terminalCode") var terminalCode: String?=null,
    @SerializedName("timeoutDuration") var timeoutDuration: Int?=null,
    @SerializedName("name") var name: String?=null,
    @SerializedName("imageName") var imageName: Int?=null,
    @SerializedName("description") var description: String?=null
):Parcelable