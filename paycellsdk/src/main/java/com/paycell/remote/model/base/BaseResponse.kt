package com.paycell.remote.model.base

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BaseResponse(
    @SerializedName("responseHeader") val responseHeader: ResponseHeader
) : Parcelable