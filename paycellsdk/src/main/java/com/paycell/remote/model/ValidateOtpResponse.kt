package com.paycell.remote.model

import android.os.Parcelable
import com.paycell.remote.model.base.ResponseHeader
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ValidateOtpResponse(
    @SerializedName("responseHeader") val responseHeader: ResponseHeader,
    @SerializedName("authState") val authState: AuthState,
    @SerializedName("remainingTryCount") val remainingTryCount: Int
) : Parcelable