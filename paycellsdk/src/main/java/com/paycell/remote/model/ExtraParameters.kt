package com.paycell.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ExtraParameters(
    @SerializedName("key") val key: String?,
    @SerializedName("value") val value: String
) : Parcelable