package com.paycell.remote.model

import android.os.Parcelable
import com.paycell.remote.model.base.RequestHeader
 import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SendOtpRequest(
    @SerializedName("requestHeader") val requestHeader: RequestHeader?,
    @SerializedName("msisdn") val msisdn: String,
    @SerializedName("paymentMethodType") val paymentMethodType: String?=null,
    @SerializedName("approvalAmount") val approvalAmount: String?=null
) : Parcelable