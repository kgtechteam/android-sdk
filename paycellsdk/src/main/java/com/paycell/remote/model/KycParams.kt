package com.paycell.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class KycParams(
    @SerializedName("kycStatu") val kycStatu: String,
    @SerializedName("kycPilotList") val kycPilotList: ArrayList<String>,
    @SerializedName("lowerMargin") val lowerMargin: String,
    @SerializedName("upperMargin") val upperMargin: String,
    @SerializedName("kycLastStep") val kycLastStep: String,
    @SerializedName("kycRandomLive") val kycRandomLive: String,
    @SerializedName("similarityMinRateDecimal") val similarityMinRateDecimal: String,
    @SerializedName("similarityMaxRateDecimal") val similarityMaxRateDecimal: String
):Parcelable