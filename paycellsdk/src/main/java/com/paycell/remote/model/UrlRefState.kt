package com.paycell.remote.model

enum class UrlRefState {
    SEND_MONEY, UPLOAD_MONEY, HAZIR_LIMIT, QR_ODE, IBAN_TRANSFER, EYEICON
}