package com.paycell.remote.model
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.paycell.remote.model.base.RequestHeader
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ValidateCardRequest(
    @SerializedName("requestHeader") var requestHeader: RequestHeader?=null,
    @SerializedName("cardId") var cardId: String?=null,
    @SerializedName("expDateMonth") var expDateMonth: String?=null,
    @SerializedName("expDateYear") var expDateYear: String?=null,
    @SerializedName("cardBrand") var cardBrand: String?=null,
    @SerializedName("payeUIDLast5Digits") var payeUIDLast5Digits: String?=null
) : Parcelable