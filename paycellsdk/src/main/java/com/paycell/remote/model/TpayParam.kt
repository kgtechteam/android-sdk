package com.paycell.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TpayParam(
    @SerializedName("id") val id: Long,
    @SerializedName("name") val name: String,
    @SerializedName("valueList") val valueList: ArrayList<String>
) : Parcelable