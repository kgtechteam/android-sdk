package com.paycell.remote.model

import android.os.Parcelable
import com.paycell.remote.model.base.ResponseHeader
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SendOtpResponse(
    @SerializedName("responseHeader") val responseHeader: ResponseHeader,
    @SerializedName("expireDate") val expireDate: String,
    @SerializedName("otpValidationId") val otpValidationId: String,
    @SerializedName("reclaimDate") val reclaimDate: String,
    @SerializedName("diff") val diff: Long
) : Parcelable