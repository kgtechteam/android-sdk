package com.paycell.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.paycell.utils.PrefUtils
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Eula(
    @SerializedName("accountEulaUrl") val accountEulaUrl: String,
    @SerializedName("cardEulaUrl") val cardEulaUrl: String,
    @SerializedName("eulaId") val eulaId: String
) : Parcelable {
    fun getParameterExtensionForLanguage(defaultUrl: String): String {
        var appLanguage = PrefUtils.selectedLanguage
        if (appLanguage.equals("tr")) {
            return defaultUrl;
        } else if (appLanguage.equals("en")) {
            return defaultUrl + "&lang=en";
        } else
            return defaultUrl;
    }
}