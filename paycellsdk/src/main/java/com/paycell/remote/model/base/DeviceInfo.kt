package com.paycell.remote.model.base

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DeviceInfo(
    @SerializedName("deviceManufacturer") var deviceManufacturer: String?=null,
    @SerializedName("deviceId") var deviceId: String?=null,
    @SerializedName("deviceOs") var deviceOs: String?=null,
    @SerializedName("deviceModel") var deviceModel: String?=null,
    @SerializedName("deviceOsVersion") var deviceOsVersion: String?=null
):Parcelable