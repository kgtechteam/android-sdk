package com.paycell.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class QrBarcodeInfo(
    @SerializedName("qrMerchant") val qrMerchant: String,
    @SerializedName("qrMoneyTransfer") val qrMoneyTransfer: String
):Parcelable