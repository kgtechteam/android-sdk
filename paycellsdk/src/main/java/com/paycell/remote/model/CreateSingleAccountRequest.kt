package com.paycell.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.paycell.remote.model.base.RequestHeader
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CreateSingleAccountRequest(
    @SerializedName("requestHeader") val requestHeader: RequestHeader?,
    @SerializedName("msisdn") val msisdn: String?,
    @SerializedName("forceCreate") val forceCreate: Boolean
) : Parcelable