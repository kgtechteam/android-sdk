package com.paycell.remote.model

import android.os.Parcelable
import com.paycell.remote.model.base.RequestHeader
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RegisterRequest(
    @SerializedName("requestHeader") val requestHeader: RequestHeader?,
    @SerializedName("askPinBeforePurchase") val askPinBeforePurchase: Int,
    @SerializedName("email") val email: String,
    @SerializedName("eulaId") val eulaId: String,
    @SerializedName("pin") val pin: String,
    @SerializedName("etk") val etk: Boolean,
    @SerializedName("kvkk") val kvkk: Boolean,
    @SerializedName("eulaSource") val eulaSource: String,
    @SerializedName("cardValidationId") val cardValidationId: String
) : Parcelable