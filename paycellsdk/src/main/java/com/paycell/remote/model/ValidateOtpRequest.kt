package com.paycell.remote.model

import android.os.Parcelable
import com.paycell.remote.model.base.RequestHeader
 import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ValidateOtpRequest(
    @SerializedName("requestHeader")
    val requestHeader: RequestHeader?,
    @SerializedName("otp") val otp: String,
    @SerializedName("otpValidationId") val otpValidationId: String
) : Parcelable