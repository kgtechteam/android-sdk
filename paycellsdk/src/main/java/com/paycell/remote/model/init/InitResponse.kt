package com.paycell.remote.model.init

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.paycell.remote.model.*
import com.paycell.remote.model.base.Merchant
import com.paycell.remote.model.base.ResponseHeader
import com.paycell.utils.PrefUtils
import kotlinx.android.parcel.Parcelize

@Parcelize
data class InitResponse(
    @SerializedName("responseHeader") val responseHeader: ResponseHeader,

    @SerializedName("authState") var authState: AuthState? = null,
    @SerializedName("authToken") val authToken: String,
    @SerializedName("eula") val eula: Eula,
    @SerializedName("merchant") val merchant: Merchant,
    @SerializedName("msisdn") val msisdn: String,
    @SerializedName("allowMsisdnChange") val allowMsisdnChange: Boolean,
    @SerializedName("url") val url: String,
    @SerializedName("update") val update: Update,
    @SerializedName("globalParameter") val globalParameter: ArrayList<GlobalParameterItem>,
    @SerializedName("paycellCardBinList") val paycellCardBinList: ArrayList<String>,

    @SerializedName("isSelfIbanOnly") val isSelfIbanOnly: Boolean,
    @SerializedName("mtMessageVisible") val mtMessageVisible: Boolean,
    @SerializedName("timeoutDcb") val timeoutDcb: String,
    @SerializedName("timeoutEmoney") val timeoutEmoney: String,
    @SerializedName("timeoutPaycellCard") val timeoutPaycellCard: String,
    @SerializedName("timeoutCreditCard") val timeoutCreditCard: String,
    @SerializedName("timeoutCredits") val timeoutCredits: String,
    @SerializedName("isQrAvailable") val isQrAvailable: String,
    @SerializedName("isPermissionAvailable") val isPermissionAvailable: Boolean,

    @SerializedName("successCountForRatePopup") val successCountForRatePopup: Int,
    @SerializedName("waitingTimeForWhoLikes") val waitingTimeForWhoLikes: Int,
    @SerializedName("waitingTimeForWhoDislikes") val waitingTimeForWhoDislikes: Int,
    @SerializedName("waitingTimeForWhoDeclines") val waitingTimeForWhoDeclines: Int,

    @SerializedName("digiPaysInfo") val digiPaysInfo: DigiPaysInfo,
    @SerializedName("kycParams") val kycParams: KycParams,
    @SerializedName("qrBarcodeInfo") val qrBarcodeInfo: QrBarcodeInfo,
    @SerializedName("payeTransactionHistoryButton") val payeTransactionHistoryButton: String,
    @SerializedName("kyc") val kyc: String

) : Parcelable {
    fun getParameterNameForLanguage(parameterName: String): String? {
        val appLanguage: String = PrefUtils.selectedLanguage
        return if (appLanguage.equals("tr", ignoreCase = true)) {
            parameterName
        } else if (appLanguage.equals("en", ignoreCase = true)) {
            "$parameterName.$appLanguage"
        } else parameterName
    }
}















