package com.paycell.remote.model.base

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BaseRequest(
    @SerializedName("requestHeader") val requestHeader: RequestHeader?
) : Parcelable