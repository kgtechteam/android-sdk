package com.paycell.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.paycell.remote.model.base.ResponseHeader
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ValidateCardResponse(
    @SerializedName("responseHeader") val responseHeader: ResponseHeader,
    @SerializedName("cardValidationId") val cardValidationId: String,
    @SerializedName("remainingTryCount") val remainingTryCount: Int
) : Parcelable