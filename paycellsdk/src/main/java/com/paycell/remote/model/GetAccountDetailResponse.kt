package com.paycell.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.paycell.remote.model.base.ResponseHeader
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GetAccountDetailResponse(
    @SerializedName("responseHeader") val responseHeader: ResponseHeader,
    @SerializedName("address") val address: String,
    @SerializedName("dateOfBirth") val dateOfBirth: String,
    @SerializedName("email") val email: String,
    @SerializedName("isEmailVerified") val isEmailVerified: Boolean,
    @SerializedName("tckn") val tckn: String,
    @SerializedName("msisdn") val msisdn: String?,
    @SerializedName("responseType") val responseType: Int
) : Parcelable