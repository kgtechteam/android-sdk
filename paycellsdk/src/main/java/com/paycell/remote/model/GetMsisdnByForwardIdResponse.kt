package com.paycell.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.paycell.remote.model.base.ResponseHeader
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GetMsisdnByForwardIdResponse(
    @SerializedName("responseHeader") val responseHeader: ResponseHeader,
    @SerializedName("msisdn") val msisdn: String
) : Parcelable