package com.paycell.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.paycell.remote.model.base.RequestHeader
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GetAccountDetailRequest(
    @SerializedName("requestHeader") val requestHeader: RequestHeader?,
    @SerializedName("accountId") val accountId: String?
) : Parcelable