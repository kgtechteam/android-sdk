package com.paycell.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ApiGatewayRequest(
    @SerializedName("grant_type") val grant_type: String?,
    @SerializedName("username") val username: String?,
    @SerializedName("password") val password: String?,
    @SerializedName("client_id") val client_id: String?,
    @SerializedName("client_secret") val client_secret : String?,
    @SerializedName("scope") val scope: String?
) : Parcelable