package com.paycell.remote.model

import android.os.Parcelable
import com.paycell.remote.model.base.RequestHeader
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ValidatePinRequest(
    @SerializedName("requestHeader") val requestHeader: RequestHeader?,
    @SerializedName("pin") val pin: String,
    @SerializedName("validationType") val validationType: String?=null,
    @SerializedName("touchIdKey") val touchIdKey: String?=null
) : Parcelable