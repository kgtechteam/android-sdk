package com.paycell.remote.model.base


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseHeader(
    @SerializedName("transactionId")
    var transactionId: String,
    @SerializedName("responseDateTime")
    var responseDateTime: String,
    @SerializedName("responseCode")
    var responseCode: String,
    @SerializedName("responseDescription")
    var responseDescription: String,
    @SerializedName("displayText")
    var displayText: String
):Parcelable