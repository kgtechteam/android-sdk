package com.paycell.remote.model.base

import com.paycell.remote.model.GetAccountResponse
import com.paycell.remote.model.GlobalParameterItem
import com.paycell.remote.model.TpayParam
import com.paycell.remote.model.ValidateOtpResponse
import com.paycell.remote.model.init.InitResponse
import com.paycell.utils.GlobalDataCommon

open class GlobalData {
    companion object {
        var languages: HashMap<String, String> = HashMap()
        lateinit var validateOtpResponse: ValidateOtpResponse
        var cardValidationId: String = ""
        lateinit var getAccountResponse: GetAccountResponse
        lateinit var initResponse: InitResponse
        var getClientParams: ArrayList<TpayParam> = arrayListOf()
        var suspendMessage:String=""
        fun String.findClientParams(): ArrayList<String> {
            return getClientParams.find { it.name == this }!!.valueList
        }
    }

    fun setLanguages(globalParameter: ArrayList<GlobalParameterItem>) {
        globalParameter.forEachIndexed { index, globalParameterItem ->
            languages.put(globalParameter[index].name, globalParameter[index].value)
        }
        GlobalDataCommon.languages = languages
    }


}
