package com.paycell.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DigiPaysInfo(
    @SerializedName("url") val url: String,
    @SerializedName("username") val username: String,
    @SerializedName("password") val password: String
):Parcelable