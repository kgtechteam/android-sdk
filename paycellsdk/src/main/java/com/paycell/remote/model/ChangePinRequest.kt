package com.paycell.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.paycell.remote.model.base.RequestHeader
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ChangePinRequest(
    @SerializedName("requestHeader") val requestHeader: RequestHeader?,
    @SerializedName("oldPin") val oldPin: String,
    @SerializedName("pin") val pin: String
) : Parcelable