package com.paycell.remote.model

import android.os.Parcelable
import com.paycell.remote.model.base.RequestHeader
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GetAccountRequest(
    @SerializedName("requestHeader") val requestHeader: RequestHeader?,
    @SerializedName("extraParameters") val extraParameters: ArrayList<ExtraParameters>?=null
) : Parcelable
