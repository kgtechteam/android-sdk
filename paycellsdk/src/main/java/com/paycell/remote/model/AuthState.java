package com.paycell.remote.model;


import com.google.gson.annotations.SerializedName;

public enum AuthState {
    @SerializedName("WAITING_OTP_VALIDATION")
    WAITING_OTP_VALIDATION,
    @SerializedName("WAITING_PIN_VALIDATION")
    WAITING_PIN_VALIDATION,
    @SerializedName("WAITING_CARD_VALIDATION")
    WAITING_CARD_VALIDATION,
    @SerializedName("AUTHENTICATED")
    AUTHENTICATED,
    @SerializedName("WAITING_REGISTRATION")
    WAITING_REGISTRATION,
    @SerializedName("WAITING_EMAIL_VALIDATION")
    WAITING_EMAIL_VALIDATION,
    @SerializedName("STEP_OTP")
    STEP_OTP
}
