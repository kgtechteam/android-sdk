package com.paycell.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Update(
    @SerializedName("majorUpdate") val majorUpdate: Boolean,
    @SerializedName("minorUpdate") val minorUpdate: Boolean,
    @SerializedName("storeUrl") val storeUrl: String
) : Parcelable