package com.paycell.remote.model.base

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TransactionInfo(
    @SerializedName("application") var application: String?=null,
    @SerializedName("applicationVersion") var applicationVersion: String?=null,
    @SerializedName("authToken") var authToken: String?=null,
    @SerializedName("mode") var mode: String?=null,
    @SerializedName("version") var version: String?=null,
    @SerializedName("transactionDateTime") var transactionDateTime: String?=null,
    @SerializedName("transactionId") var transactionId: String?=null
) : Parcelable