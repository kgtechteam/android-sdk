package com.paycell.remote.model.base


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RequestHeader(
    @SerializedName("deviceInfo")
    var deviceInfo: DeviceInfo?=null,
    @SerializedName("merchant")
    var merchant: Merchant?=null,
    @SerializedName("transactionInfo")
    var transactionInfo: TransactionInfo?=null
):Parcelable