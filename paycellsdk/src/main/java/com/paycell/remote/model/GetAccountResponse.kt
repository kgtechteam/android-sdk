package com.paycell.remote.model

import android.os.Parcelable
import com.paycell.remote.model.base.ResponseHeader
import com.google.gson.annotations.SerializedName
import com.paycell.utils.PrefUtils
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GetAccountResponse(
    @SerializedName("responseHeader") val responseHeader: ResponseHeader,
    @SerializedName("maskedEmail") val maskedEmail: String,
    @SerializedName("doCardValidation") val doCardValidation: Boolean,
    @SerializedName("doPinValidation") val doPinValidation: Boolean,
    @SerializedName("showEula") val showEula: Boolean,
    @SerializedName("responseType") val responseType: Int,
    @SerializedName("operatorId") val operatorId: Int,
    @SerializedName("operatorType") val operatorType: String="",
    @SerializedName("operatorPaymentTypeId") val operatorPaymentTypeId: Int,
    @SerializedName("tcknRequired") val tcknRequired: Boolean,
    @SerializedName("showEtk") val showEtk: Boolean,
    @SerializedName("showKvkk") val showKvkk: Boolean,
    @SerializedName("msisdn") val msisdn: String,
    @SerializedName("eulaId") val eulaId: String,
    @SerializedName("eulaUrl") val eulaUrl: String,
    @SerializedName("tckn") val tckn: String,
    @SerializedName("firstName") val firstName: String,
    @SerializedName("lastName") val lastName: String,
    @SerializedName("isEmailVerified") val isEmailVerified: Boolean,
    @SerializedName("requestCode") val requestCode: String,
    @SerializedName("isVerified") val isVerified: Boolean,
    @SerializedName("customerDijitalKycStatus") val customerDijitalKycStatus: String,
    @SerializedName("customerStatus") val customerStatus: Boolean,
    @SerializedName("isClubMember") val isClubMember: Boolean,
    @SerializedName("showFinancellKvkk") val showFinancellKvkk: Boolean,
    @SerializedName("customerKycStatus") val customerKycStatus: Boolean,
    @SerializedName("accountType") val accountType: String,
    @SerializedName("externalId") val externalId: String,
    @SerializedName("dcbOpen") val dcbOpen: Boolean,
    @SerializedName("accountId") val accountId: String,
    @SerializedName("dcbUser") val dcbUser: Boolean,
    @SerializedName("showPopup") val showPopup: Boolean,
    @SerializedName("showLotteryPopup") val showLotteryPopup: Boolean,
    @SerializedName("lastValidPinLenght") val lastValidPinLenght: Int
) : Parcelable{
    fun getParameterExtensionForLanguage(): String {
        var appLanguage = PrefUtils.selectedLanguage
        if (appLanguage.equals("tr")) {
            return eulaUrl;
        } else if (appLanguage.equals("en")) {
            return eulaUrl + "&lang=en";
        } else
            return eulaUrl;
    }
}