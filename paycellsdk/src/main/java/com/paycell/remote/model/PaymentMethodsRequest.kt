package com.paycell.remote.model

import android.os.Parcelable
import com.paycell.remote.model.base.RequestHeader
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PaymentMethodsRequest(
    @SerializedName("requestHeader") val requestHeader: RequestHeader?=null,
    @SerializedName("appMerchantId") val appMerchantId: Long?=null,
    @SerializedName("cardFilter") val cardFilter: String?=null,
    @SerializedName("isWallet") val isWallet: Boolean?=null,
    @SerializedName("payeCardEnabled") val payeCardEnabled: String?=null
) : Parcelable