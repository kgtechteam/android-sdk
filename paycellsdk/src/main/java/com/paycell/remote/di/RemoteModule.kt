package com.paycell.remote.di


import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.paycell.PaycellSdkApp.API_GATEWAY_BASE_URL
import com.paycell.PaycellSdkApp.BASE_URL
import com.paycell.PaycellSdkApp.isBaseUrlApiGateway
import com.paycell.remote.service.PaycellService
import com.paycell.utils.NetworkUtils
import com.paycell.utils.PrefUtils
import com.paycellsdk.BuildConfig
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit

fun remoteModule() = module {
    var baseUrl = if (isBaseUrlApiGateway) {
        API_GATEWAY_BASE_URL
    } else {
        BASE_URL
    }
    single { File(androidContext().cacheDir, UUID.randomUUID().toString()) }

    single { Cache(get(), 5 * 1024 * 1024) }

    single {
        OkHttpClient.Builder()
            .cache(get())
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .followSslRedirects(true)
            .addInterceptor { chain ->
                var request = chain.request().newBuilder()
                request.addHeader("Content-Type", "application/json; charset=utf-8")
                if (NetworkUtils.hasInternet(androidContext())) {
                    request.addHeader("Cookie", "authToken=" + PrefUtils.authToken)
                }
                chain.proceed(request.build())
            }
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = if (BuildConfig.DEBUG)
                    HttpLoggingInterceptor.Level.BODY
                else
                    HttpLoggingInterceptor.Level.NONE
            })
            // .addInterceptor(ChuckInterceptor(androidContext()))
            .build()
    }

    single {
        Retrofit.Builder()
            .client(get())
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
    }


    factory { get<Retrofit>().create(PaycellService::class.java) }
    //  factory { get<Retrofit>().create(IPasswordService::class.java) }
    //  factory { get<Retrofit>().create(IAccountService::class.java) }
}