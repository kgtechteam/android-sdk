package com.paycell.di

import org.koin.dsl.module

val managerModule = module {
    factory {
        DataManager(
            get()
        ) as IDataManager
    }
}