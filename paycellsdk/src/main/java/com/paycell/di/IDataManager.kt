package com.paycell.di

 import com.paycell.remote.model.init.InitRequest
import com.paycell.remote.model.init.InitResponse
import com.paycell.remote.network.NetworkState
import com.paycell.remote.model.*
 import com.paycell.remote.model.base.BaseRequest
 import com.paycell.remote.model.base.BaseResponse
 import kotlinx.coroutines.flow.Flow

interface IDataManager {

    fun forwardMsisdn(msisdn:String): Flow<NetworkState<ForwardMsisdnResponse>>

    fun forwardMsisdnProd(msisdn:String,url:String): Flow<NetworkState<ForwardMsisdnResponse>>

    fun getMsisdnByForwardId(getMsisdnByForwardIdRequest: GetMsisdnByForwardIdRequest): Flow<NetworkState<GetMsisdnByForwardIdResponse>>

    fun postInit(initRequest: InitRequest): Flow<NetworkState<InitResponse>>

    fun postClientParams(baseRequest: BaseRequest): Flow<NetworkState<GetClientParamsResponse>>

    fun sendOtp(sendOtpRequest: SendOtpRequest): Flow<NetworkState<SendOtpResponse>>

    fun getAccount(getAccountRequest: GetAccountRequest): Flow<NetworkState<GetAccountResponse>>

    fun getAccountDetail(getAccountDetailRequest: GetAccountDetailRequest): Flow<NetworkState<GetAccountDetailResponse>>

    fun checkAuthState(): Flow<NetworkState<CheckAuthStateResponse>>

    fun validateOtp(validateOtpRequest: ValidateOtpRequest): Flow<NetworkState<ValidateOtpResponse>>

    fun sendRegisterRequest(registerRequest: RegisterRequest): Flow<NetworkState<RegisterResponse>>

    fun createSingleAccount(createSingleAccountRequest: CreateSingleAccountRequest): Flow<NetworkState<BaseResponse>>

    fun login(validatePinRequest: ValidatePinRequest): Flow<NetworkState<ValidatePinResponse>>

    fun sendValidationEmail(sendValidationEmailRequest: SendValidationEmailRequest): Flow<NetworkState<SendValidationEmailResponse>>

    fun forgotPin(forgotPinRequest: ForgotPinRequest): Flow<NetworkState<ForgotPinResponse>>

    fun logout(baseRequest: BaseRequest): Flow<NetworkState<BaseResponse>>

    fun getCards(baseRequest: BaseRequest):Flow<NetworkState<CardsResponse>>

    fun cancelAccount(baseRequest: BaseRequest):Flow<NetworkState<BaseResponse>>

    fun validateCard(validateCardRequest: ValidateCardRequest):Flow<NetworkState<ValidateCardResponse>>

    fun changePin(changePinRequest: ChangePinRequest):Flow<NetworkState<BaseResponse>>

    fun apiGateway(apiGatewayRequest: ApiGatewayRequest):Flow<NetworkState<ApiGatewayResponse>>

}