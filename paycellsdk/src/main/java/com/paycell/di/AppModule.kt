package com.paycell.di

 import com.paycell.PaycellSdkApp
 import com.paycell.remote.di.remoteModule
 import com.paycellsdk.BuildConfig

val appModule = listOf(
    remoteModule(),
    managerModule,
    viewModelModule
)