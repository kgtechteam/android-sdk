package com.paycell.di

 import com.paycell.remote.model.init.InitRequest
import com.paycell.remote.model.init.InitResponse


import com.paycell.remote.network.NetworkState
import com.paycell.remote.service.*
import com.paycell.remote.model.*
import com.paycell.remote.model.base.BaseRequest
import com.paycell.remote.model.base.BaseResponse
 import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class DataManager(private val generalService: PaycellService) : BaseDataManager(), IDataManager {

    override fun forwardMsisdn(msisdn: String): Flow<NetworkState<ForwardMsisdnResponse>> = flow {
        emit(apiCall { generalService.forwardMsisdn(msisdn) })
    }

    override fun forwardMsisdnProd(msisdn: String, url: String): Flow<NetworkState<ForwardMsisdnResponse>> = flow {
        emit(apiCall { generalService.forwardMsisdnProd(msisdn, url) })
    }

    override fun getMsisdnByForwardId(getMsisdnByForwardIdRequest: GetMsisdnByForwardIdRequest): Flow<NetworkState<GetMsisdnByForwardIdResponse>> = flow {
        emit(apiCall { generalService.getMsisdnByForwardId(getMsisdnByForwardIdRequest) })
    }
    override fun postInit(initRequest: InitRequest): Flow<NetworkState<InitResponse>> = flow {
        emit(apiCall { generalService.init(initRequest) })
    }

    override fun postClientParams(baseRequest: BaseRequest): Flow<NetworkState<GetClientParamsResponse>> = flow {
        emit(apiCall { generalService.getClientParams(baseRequest) })
    }

    override fun sendOtp(sendOtpRequest: SendOtpRequest): Flow<NetworkState<SendOtpResponse>> = flow {
        emit(apiCall { generalService.sendOtp(sendOtpRequest) })
    }

    override fun getAccount(getAccountRequest: GetAccountRequest): Flow<NetworkState<GetAccountResponse>> = flow {
        emit(apiCall { generalService.getAccount(getAccountRequest) })
    }

    override fun getAccountDetail(getAccountDetailRequest: GetAccountDetailRequest): Flow<NetworkState<GetAccountDetailResponse>> = flow {
        emit(apiCall { generalService.getAccountDetail(getAccountDetailRequest) })
    }
    override fun checkAuthState(): Flow<NetworkState<CheckAuthStateResponse>> = flow {
        emit(apiCall { generalService.checkAuthState() })
    }

    override fun validateOtp(validateOtpRequest: ValidateOtpRequest): Flow<NetworkState<ValidateOtpResponse>> = flow {
        emit(apiCall { generalService.validateOtp(validateOtpRequest) })
    }

    override fun sendRegisterRequest(registerRequest: RegisterRequest): Flow<NetworkState<RegisterResponse>> = flow {
        emit(apiCall { generalService.register(registerRequest) })
    }

    override fun createSingleAccount(createSingleAccountRequest: CreateSingleAccountRequest): Flow<NetworkState<BaseResponse>> = flow {
        emit(apiCall { generalService.createSingleAccount(createSingleAccountRequest) })
    }

    override fun login(validatePinRequest: ValidatePinRequest): Flow<NetworkState<ValidatePinResponse>> = flow {
        emit(apiCall { generalService.login(validatePinRequest) })
    }

    override fun sendValidationEmail(sendValidationEmailRequest: SendValidationEmailRequest): Flow<NetworkState<SendValidationEmailResponse>> = flow {
        emit(apiCall { generalService.sendValidationEmail(sendValidationEmailRequest) })
    }

    override fun forgotPin(forgotPinRequest: ForgotPinRequest): Flow<NetworkState<ForgotPinResponse>> = flow {
        emit(apiCall { generalService.forgotPin(forgotPinRequest) })
    }

    override fun logout(baseRequest: BaseRequest): Flow<NetworkState<BaseResponse>> = flow {
        emit(apiCall { generalService.logout(baseRequest) })
    }
    override fun getCards(baseRequest: BaseRequest): Flow<NetworkState<CardsResponse>> = flow {
        emit(apiCall { generalService.getCards(baseRequest) })
    }

    override fun cancelAccount(baseRequest: BaseRequest): Flow<NetworkState<BaseResponse>> = flow {
        emit(apiCall { generalService.cancelAccount(baseRequest) })
    }

    override fun validateCard(validateCardRequest: ValidateCardRequest): Flow<NetworkState<ValidateCardResponse>>  = flow {
        emit(apiCall { generalService.validateCard(validateCardRequest) })
    }

    override fun changePin(changePinRequest: ChangePinRequest): Flow<NetworkState<BaseResponse>> = flow {
        emit(apiCall { generalService.changePin(changePinRequest) })
    }

    override fun apiGateway(apiGatewayRequest: ApiGatewayRequest): Flow<NetworkState<ApiGatewayResponse>> = flow {
        emit(apiCall { generalService.apiGateway(apiGatewayRequest) })
    }
}