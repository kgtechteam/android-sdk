package com.paycell.di

import com.paycell.ui.intro.IntroViewModel
import com.paycell.ui.onboardingloginstep.LoginStepViewModel
import com.paycell.ui.sendmoney.GeneralViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { IntroViewModel(get()) }
    viewModel { LoginStepViewModel(get()) }
    viewModel { GeneralViewModel(get()) }

}