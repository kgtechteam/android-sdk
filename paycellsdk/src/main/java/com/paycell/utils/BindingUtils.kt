package com.paycell.utils

import android.net.Uri
import android.text.TextUtils
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.github.tamir7.contacts.Contact
import com.google.android.material.internal.ViewUtils.dpToPx
import com.paycell.PaycellSdkApp
import com.paycellsdk.R
import com.paycell.customviews.CVEditText
import com.paycell.extensions.color
import com.paycell.extensions.gone
 import com.paycell.extensions.setVectorForPreLollipop
import com.paycell.helper.TypeFaceCache

object BindingUtils {


    @JvmStatic
    @BindingAdapter("inputHint", "buttonText")
    fun cvEdittext(view: CVEditText, hint: String, buttonText: String?) {
        view.setHintAndText(hint, buttonText)
    }

    @JvmStatic
    @BindingAdapter("visibiltyGone")
    fun visibiltyGone(view: View, text: String?) {
        if (text.isNullOrEmpty()) {
            view.gone()
        }
    }

    @JvmStatic
    @BindingAdapter("visibiltyGone")
    fun visibiltyGone(view: View, isHave: Boolean) {
        if (isHave) {
            view.gone()
        }
    }

    @JvmStatic
    @BindingAdapter("contactNamePhoto")
    fun contactNamePhoto(view: TextView, nameShortCut: String) {
        if (nameShortCut.isNullOrEmpty()) {
            view.setBackgroundDrawable(PaycellSdkApp.context.setVectorForPreLollipop(R.drawable.ic_nouser))
         } else {
             view.text=nameShortCut
        }
    }
    @JvmStatic
    @BindingAdapter("nameisAvalible")
    fun nameisAvalible(view: TextView,nameisAvalible:String) {
        if (nameisAvalible.isNullOrEmpty()){
          //  view.textSize=14f
            view.setTextColor(view.context.color(R.color.black))
            TypeFaceCache.getFontOrGenerateOne("sfprotext_bold.ttf", context = view.context)
        }
    }


}