package com.paycell.utils;

import com.paycell.PaycellSdkApp;
import com.paycell.remote.model.base.RequestHeader;
import com.paycell.remote.model.init.InitRequest;

public class HashGenerator {
    private InitRequest initRequest;
    private RequestHeader requestHeader;
    private String authToken;

    public String generate(InitRequest initRequest, String authToken) {
        this.initRequest = initRequest;
        this.requestHeader = initRequest.getRequestHeader();
        this.authToken = authToken;
        return EncryptionUtils.encodeSha256(getHashData());
    }

    private String getHashData() {

        /**
         * hashString=deviceId
         * +deviceModel
         * +deviceManufacturer
         * +deviceOs
         * +deviceOsVersion
         * +applicationVersion
         * +application+
         * (authToken)
         * + (forwardId)
         * +connectionType
         * +securityData
         */
        return  requestHeader.getDeviceInfo().getDeviceId()
                + requestHeader.getDeviceInfo().getDeviceModel()
                + requestHeader.getDeviceInfo().getDeviceManufacturer()
                + requestHeader.getDeviceInfo().getDeviceOs()
                + requestHeader.getDeviceInfo().getDeviceOsVersion()
                + requestHeader.getTransactionInfo().getApplicationVersion()
                + requestHeader.getTransactionInfo().getApplication()
                + (authToken == null ? "" : authToken)
                + (initRequest.getForwardId() == null ? "" : initRequest.getForwardId())
                + initRequest.getConnectionType()
                + EncryptionUtils.encodeSha256(getSecData());

    }

     private String getSecData() {
        return new String(hexToByte(PaycellSdkApp.secureCode)) + requestHeader.getTransactionInfo().getApplication();  //todo buraya acil bak
    }
    public static byte[] hexToByte(final String hexstring) {
        if (hexstring == null) {
            return null;
        }
        byte[] bytes = new byte[hexstring.length() / 2];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) Integer.parseInt(hexstring.substring(2 * i, 2 * i + 2), 16);
        }
        return bytes;
    }

    public static String byteToHex(final byte hexbytes[]) {
        if (hexbytes == null) {
            return null;
        }
        StringBuffer hexbuffer = new StringBuffer(hexbytes.length * 2);
        int i;
        for (i = 0; i < hexbytes.length; i++) {
            if ((hexbytes[i] & 0xff) < 0x10) {
                hexbuffer.append('0');
            }
            hexbuffer.append(Long.toString(hexbytes[i] & 0xff, 16));
        }
        return hexbuffer.toString();
    }


}
