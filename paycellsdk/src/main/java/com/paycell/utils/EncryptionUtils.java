package com.paycell.utils;

import android.util.Base64;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class EncryptionUtils {

    private static final String TAG = "EncryptionUtils";

    public static String encodeSha256(String password) {
        //Log.i(TAG, "encodeSha256: " + password);
        String result="";
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
         }
        try {
            if (md!=null) {
                md.update(password.getBytes("iso-8859-1"), 0, password.length());
            }
        } catch (UnsupportedEncodingException e) {
        }
        if (md!=null) {
            byte[] sha256hash = md.digest();
            result = Base64.encodeToString(sha256hash, Base64.DEFAULT);
            result = result.substring(0, result.length() - 1);
        }
        return result;
    }
}
