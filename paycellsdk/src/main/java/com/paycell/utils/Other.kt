package com.paycell.utils

import android.content.Context
import com.google.gson.Gson
import com.paycell.PaycellSdkApp
import com.paycell.helper.ProgressLoadingDialog
import com.paycell.remote.model.GetAccountResponse
import com.paycell.remote.model.base.BaseRequest
import com.paycell.remote.model.base.BaseResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST
import java.io.IOException

object Other {

    inline fun <reified T : Any> dummyData(path: String): GetAccountResponse {
        return Gson().fromJson(
            getJsonDataFromAsset(
                path + ".json"
            ), GetAccountResponse::class.java
        )
    }

    fun getJsonDataFromAsset(fileName: String): String? {
        val jsonString: String
        try {
            jsonString =
                PaycellSdkApp.context.assets.open(fileName).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }

    fun showPopupInfo(context: Context, text: String, callback: () -> Unit) {
        var progress: ProgressLoadingDialog
        var progressModel: ProgressLoadingDialog.ProgreessModel =
            ProgressLoadingDialog.ProgreessModel(
                context = context,
                isProgress = false,
                title = text
            )
        progress = ProgressLoadingDialog(progressModel) {
            callback.invoke()
        }
        progress.toggle(status = true)
    }

    fun logoutApiServices(): ILogoutService {
        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(PaycellSdkApp.BASE_URL)
            .build()
        return retrofit.create<ILogoutService>(ILogoutService::class.java)
    }

    fun logout(callback: () -> Unit) {
        var request = BaseRequest(requestHeader = RequestHeaderManager.getRequestHeader())
        val apiServices = logoutApiServices()
        apiServices.logout(request).enqueue(object : Callback<BaseResponse> {
            override fun onResponse(
                call: Call<BaseResponse>,
                response: Response<BaseResponse>
            ) {
                if (response.body()?.responseHeader!!.responseCode == "0") {
                    callback.invoke()
                }
            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {

            }
        })
    }

    interface ILogoutService {
        @POST("tpay/paycell/services/sessionextsdkrs/sessionServiceExtSdk/logout/")
        fun logout(@Body baseRequest: BaseRequest): Call<BaseResponse>
    }
}