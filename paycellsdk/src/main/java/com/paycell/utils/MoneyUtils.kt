package com.paycell.utils

import java.lang.Double.parseDouble
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

object MoneyUtils {
    // başında çizgi olanlar hesaba katılmamış. // TODO: 25.09.2017 transaction history'de crash etti. "0.-0" yüzünden.
    fun getFormattedAmountInTl(amount: String): String {
        var amount=amount
        //Double amountInDouble = Double.parseDouble(amount.substring(0, amount.length() - 2) + "." + amount.substring(amount.length() - 2, amount.length()));
        if (amount.isNullOrEmpty()) {
            return ""
        }
        var signNeeded = false
        val originalAmount = amount
        if (amount == "+0" || amount == "-0") {
            signNeeded = true
            amount = "0"
        }
        if (amount.startsWith("+") || amount.startsWith("-")) {
            signNeeded = true
            amount = amount.substring(1, amount.length)
        }
        // try to parse string amount. if no parsing available, just return amount directly.
        var amountInDouble: Double
        try {
            if (amount.length >= 3) { // amount: 100
                amountInDouble = parseDouble(
                    amount.substring(
                        0,
                        amount.length - 2
                    ) + "." + amount.substring(
                        amount.length - 2,
                        amount.length
                    )
                )
            } else if (amount.length == 2) { // amount: 10
                amountInDouble = parseDouble("0." + amount)
            } else { //amount: 1
                amountInDouble = parseDouble("0.0" + amount)
            }
        } catch (nfe: NumberFormatException) {
            amountInDouble = parseDouble("0.00")
        }
        val otherSymbols = DecimalFormatSymbols(Locale.getDefault())
        otherSymbols.decimalSeparator = ','
        otherSymbols.groupingSeparator = '.'
        val dFormat = DecimalFormat("###,###,##0.00", otherSymbols)
        val str = dFormat.format(amountInDouble)
        if (signNeeded) {
            return originalAmount[0] + str
        } else {
            return str
        }
    }

}