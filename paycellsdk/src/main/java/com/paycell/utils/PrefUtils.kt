package com.paycell.utils

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.GsonBuilder

object PrefUtils {
    private const val NAME = "PaycellSdk"
    private const val MODE = Context.MODE_PRIVATE
    private lateinit var preferences: SharedPreferences


    private val IS_FIRST_RUN_PREF = Pair("is_first_run", false)
    private val SELECTED_LANGUAGE = Pair("selectedLanguage", "")
    private val AUTH_TOKEN = Pair("authToken", "")
    private val TOUCH_ID_KEY = Pair("touchIdKey", "")
    private val SIM_CONTROL_VERIFY = Pair("SIM_CONTROL_VERIFY", "")
    private val EXAMPLE_INT = Pair("selectedLanguage", 0)
    private val MODEL = Pair("selessctedLanguage", "")


    var isLogin: Boolean
        get() = IS_FIRST_RUN_PREF.getBoolean()
        set(value) = IS_FIRST_RUN_PREF.setBoolean(value)

    var selectedLanguage: String
        get() = SELECTED_LANGUAGE.getString()
        set(value) = SELECTED_LANGUAGE.setString(value)

    var authToken: String
        get() = AUTH_TOKEN.getString()
        set(value) = AUTH_TOKEN.setString(value)

    var touchIdKey: String
        get() = TOUCH_ID_KEY.getString()
        set(value) = TOUCH_ID_KEY.setString(value)

    var simControlVerify: String
        get() = SIM_CONTROL_VERIFY.getString()
        set(value) = SIM_CONTROL_VERIFY.setString(value)

    var exampleInt: Int
        get() = EXAMPLE_INT.getInt()
        set(value) = EXAMPLE_INT.setInt(value)

    var model: Any?
        get() = MODEL.getModel<Any>()
        set(value) = MODEL.setModel(value)













    @Suppress("NON_PUBLIC_CALL_FROM_PUBLIC_INLINE")
    inline fun <reified Any> Pair<String, String>.getModel(): Any {
        val data = preferences.getString(this.first, this.second)
        return GsonBuilder().create().fromJson(data, Any::class.java)
    }


    fun Pair<String, String>.getString(): String {
        return preferences.getString(this.first, this.second).toString()
    }

    fun Pair<String, Int>.getInt(): Int {
        return preferences.getInt(this.first, this.second)
    }

    fun Pair<String, Boolean>.getBoolean(): Boolean {
        return preferences.getBoolean(this.first, this.second)
    }


    @Suppress("NON_PUBLIC_CALL_FROM_PUBLIC_INLINE")
    inline fun <reified Any> Pair<String, String>.setModel(value: Any) {
        val data = GsonBuilder().serializeNulls().create().toJson(value)
        preferences.edit { it.putString(this.first, data) }
    }

    fun Pair<String, String>.setString(value: String?) = preferences.edit { it.putString(this.first, value) }
    fun Pair<String, Int>.setInt(value: Int) = preferences.edit { it.putInt(this.first, value) }
    fun Pair<String, Boolean>.setBoolean(value: Boolean) = preferences.edit { it.putBoolean(this.first, value) }


    fun init(context: Context) {
        preferences = context.getSharedPreferences(
            NAME,
            MODE
        )
    }

    private inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
        val editor = edit()
        operation(editor)
        editor.apply()
    }

}
