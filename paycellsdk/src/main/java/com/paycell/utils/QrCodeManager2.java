package com.paycell.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.net.Uri;
import android.text.TextUtils;

import androidx.core.content.FileProvider;

import com.paycell.PaycellSdkApp;
import com.paycell.remote.model.base.GlobalData;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.journeyapps.barcodescanner.BarcodeEncoder;
 import com.paycell.helper.NewQrModel;
import com.paycellsdk.R;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class QrCodeManager2 {

    private static final String SHARED_PROVIDER_AUTHORITY = "com.paycell" + ".myfileprovider";
    private static final String SHARED_FOLDER = "shared";
    private static  QrCodeManager2 ourInstance = new QrCodeManager2();
    private QrAndBarcode qrAndBarcode = new QrAndBarcode();

    private QrCodeManager2() {

    }

    public static QrCodeManager2 getInstance() {
        return ourInstance;
    }

    public void reset() {
        qrAndBarcode.reset();
    }

    public boolean isQrAndBarcodeCreated() {
        return qrAndBarcode.isCreated();
    }

    public boolean createQrAndBarcodeBitmapsWithDefaults(int width, int height, UsageType usageType) {
        String barcodeText = getTextForUsageType(usageType, false);
        String qrText = getTextForUsageType(usageType, true);
        boolean isSuccessful = qrAndBarcode.generateBothAndSave(qrText, barcodeText, width, height, usageType);
        return isSuccessful;
    }

    private String getTextForUsageType(UsageType usageType, boolean withWrapperDeeplink) {
        String accountId = GlobalData.Companion.getGetAccountResponse().getAccountId();
        String usageTypeAsString = "";
        if (usageType == UsageType.MONEY_TRANSFER) {
            usageTypeAsString = GlobalData.Companion.getInitResponse().getQrBarcodeInfo().getQrMoneyTransfer();
        } else if (usageType == UsageType.MERCHANT) {
            usageTypeAsString = GlobalData.Companion.getInitResponse().getQrBarcodeInfo().getQrMerchant();
        }

        if (withWrapperDeeplink) {
            String qrCodeLink = GlobalData.Companion.getLanguages().get("paycell_qr_code_url")
                    .replace("[x]", usageTypeAsString)
                    .replace("[y]", accountId);
            return qrCodeLink;

        } else {
            return usageTypeAsString + accountId;
        }
    }

    public String getPreTextForQrInfo(String qrInfo) {
        if (TextUtils.isEmpty(qrInfo)) return null;
        if (GlobalData.Companion.getInitResponse().getQrBarcodeInfo().getQrMoneyTransfer().equals(qrInfo)) {
            return "MONEY_TRANSFER";
        } else if (GlobalData.Companion.getInitResponse().getQrBarcodeInfo().getQrMerchant().equals(qrInfo)) {
            return "MERCHANT";
        } else {
            return null;
        }
    }

    public Bitmap getQrBitmap() {
        return qrAndBarcode.newQrCodeBitmap;
    }

    public Bitmap getBarcodeBitmap() {
        return qrAndBarcode.newBarcodeBitmap;
    }

    public Uri createQrCodeUri(Context context) {
        if (qrAndBarcode.newQrCodeBitmap != null) {
            File sharedFile = null;
            try {
                final File sharedFolder = new File(context.getFilesDir(), SHARED_FOLDER);
                sharedFolder.mkdirs();

                sharedFile = File.createTempFile("qr_code", ".png", sharedFolder);
                sharedFile.createNewFile();

                java.io.OutputStream out = new java.io.FileOutputStream(sharedFile);
                qrAndBarcode.newQrCodeBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                out.flush();
                out.close();

            } catch (Exception e) {
                e.toString();
            }

            Uri uri = null;
            if (sharedFile != null) {
                uri = FileProvider.getUriForFile(context, SHARED_PROVIDER_AUTHORITY, sharedFile);
            }
            return uri;
        }
        return null;
    }

    public NewQrModel parseNewQr(String encryptedQrCode) {
        try {
            NewQrModel parseBarcode = parseBarcode(encryptedQrCode);
            if (parseBarcode(encryptedQrCode) != null) {
                return parseBarcode;
            }

            Uri qrUri = Uri.parse(encryptedQrCode);
            String isQr = qrUri.getQueryParameter("isQr");
            String qrInfo = qrUri.getQueryParameter("qrInfo");
            String accountId = qrUri.getQueryParameter("accountId");
            if (TextUtils.isEmpty(isQr) || TextUtils.isEmpty(qrInfo) || TextUtils.isEmpty(accountId)) {
                return null;
            } else {
                return new NewQrModel(isQr, qrInfo, accountId);
            }
        } catch (Exception e) {
            return null;
        }
    }

    private NewQrModel parseBarcode(String encryptedQrCode) {
        try {
            if (TextUtils.isEmpty(encryptedQrCode)) {
                return null;
            }
            if (encryptedQrCode.startsWith(GlobalData.Companion.getInitResponse().getQrBarcodeInfo().getQrMoneyTransfer()) ||
                    encryptedQrCode.startsWith(GlobalData.Companion.getInitResponse().getQrBarcodeInfo().getQrMerchant())) {
                String qrInfo = encryptedQrCode.substring(0, 4);
                String accountId = encryptedQrCode.substring(4);
                return new NewQrModel("YES", qrInfo, accountId);
            } else {
                return null;
            }
        } catch (Exception e) { return null; }
    }

    public enum UsageType {
        MONEY_TRANSFER,
        MERCHANT,
        UNKNOWN
    }

    private class QrAndBarcode {
        Bitmap newQrCodeBitmap = null;
        Bitmap newBarcodeBitmap = null;
        UsageType usageType = UsageType.UNKNOWN;

        void reset() {
            this.newQrCodeBitmap = null;
            this.newBarcodeBitmap = null;
        }

        boolean isCreated() {
            return newQrCodeBitmap != null && newBarcodeBitmap != null;
        }

        boolean generateBothAndSave(String qrText, String barcodeText, int width, int height, UsageType usageType) {
            if (TextUtils.isEmpty(qrText) || TextUtils.isEmpty(barcodeText) || usageType == UsageType.UNKNOWN) {
                return false;
            }
            Bitmap qrBitmap = generateQrCodeWithLogo(qrText, width, height);
            Bitmap barcodeBitmap = generateBarcode(barcodeText, width, height / 2);
            this.newQrCodeBitmap = qrBitmap;
            this.newBarcodeBitmap = barcodeBitmap;
            this.usageType = usageType;
            return isCreated();
        }

        // 600, 600
        private Bitmap generateQrCode(String text, int width, int height) {
            try {
                MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
                final Map<EncodeHintType, Object> encodingHints = getEncodeHints();
                BitMatrix bitMatrix = null;

                bitMatrix = multiFormatWriter.encode(text, BarcodeFormat.QR_CODE, width, height, encodingHints);

                BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                final Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
                return bitmap;
            } catch (Exception e) {
                return null;
            }
        }

        private Bitmap generateQrCodeWithLogo(String text, int width, int height) {
            Bitmap bitmap = generateQrCode(text, width, height);
            Bitmap overlay = BitmapFactory.decodeResource(PaycellSdkApp.context.getResources(), R.drawable.qrlogo);
            Bitmap scaledOverlay = Bitmap.createScaledBitmap(overlay, 210, 90, false);
            Bitmap mergedBitmap = mergeBitmaps(scaledOverlay, bitmap);
            return mergedBitmap;
        }

        // 600,300
        private Bitmap generateBarcode(String text, int width, int height) {
            try {
                MultiFormatWriter writer = new MultiFormatWriter();
                final Map<EncodeHintType, Object> encodingHints = getEncodeHints();
                // Use 1 as the height of the matrix as this is a 1D Barcode.
                BitMatrix bm = writer.encode(text, BarcodeFormat.CODE_128, width, 1, encodingHints);
                int bmWidth = bm.getWidth();

                Bitmap imageBitmap = Bitmap.createBitmap(bmWidth, height, Bitmap.Config.ARGB_8888);

                for (int i = 0; i < bmWidth; i++) {
                    // Paint columns of width 1
                    int[] column = new int[height];
                    Arrays.fill(column, bm.get(i, 0) ? Color.BLACK : Color.WHITE);
                    imageBitmap.setPixels(column, 0, 1, i, 0, 1, height);
                }

                return imageBitmap;
            } catch (Exception e) {
                return null;
            }
        }

        private Map<EncodeHintType, Object> getEncodeHints() {
            final Map<EncodeHintType, Object> encodingHints = new HashMap<>();
            encodingHints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
            encodingHints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
            encodingHints.put(EncodeHintType.MARGIN, 0);
            return encodingHints;
        }

        Bitmap mergeBitmaps(Bitmap overlay, Bitmap bitmap) {

            int height = bitmap.getHeight();
            int width = bitmap.getWidth();

            Bitmap combined = Bitmap.createBitmap(width, height, bitmap.getConfig());
            Canvas canvas = new Canvas(combined);
            int canvasWidth = canvas.getWidth();
            int canvasHeight = canvas.getHeight();

            canvas.drawBitmap(bitmap, new Matrix(), null);

            int centreX = (canvasWidth - overlay.getWidth()) / 2;
            int centreY = (canvasHeight - overlay.getHeight()) / 2;
            canvas.drawBitmap(overlay, centreX, centreY, null);

            return combined;
        }
    }
}
