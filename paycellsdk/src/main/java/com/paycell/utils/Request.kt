package com.paycell.utils

import com.paycell.remote.model.base.RequestHeader

object Request {
    var headerManager: RequestHeader? = null

    fun getHeader():RequestHeader {
        if (headerManager == null) {
            headerManager=RequestHeaderManager.getRequestHeader()
            return headerManager as RequestHeader
        }
        else{
          return  headerManager as RequestHeader
        }
    }
}