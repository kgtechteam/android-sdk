package com.paycell.utils

import android.os.Build
import android.provider.Settings
import android.provider.Settings.System.getString
import com.paycell.PaycellSdkApp
import com.paycell.remote.model.ExtraParameters
import com.paycell.remote.model.base.DeviceInfo
import com.paycell.remote.model.base.Merchant
import com.paycell.remote.model.base.RequestHeader
import com.paycell.remote.model.base.TransactionInfo
 import com.paycell.extensions.transactionDateFormat
import java.util.*

class RequestHeaderManager {
    companion object {
        @JvmStatic
        val deviceInfo by lazy {
            DeviceInfo().apply {
                deviceId = installationId()
                deviceOs = "ANDROID"
                deviceModel = Build.MODEL
                deviceOsVersion = Build.VERSION.RELEASE
                deviceManufacturer = Build.MANUFACTURER
            }
        }

        @JvmStatic
        fun getRequestHeader() = RequestHeader().apply {
            deviceInfo = Companion.deviceInfo
            transactionInfo = transactionInfo()
            merchant= getDefaultMerchant()
        }


        @JvmStatic
        fun getRequestHeader(merchant: Merchant?) = getRequestHeader().apply {
            this.merchant = merchant
        }

        @JvmStatic
        fun getDefaultMerchant() = Merchant().apply {
            merchantCode = "-9999"
            terminalCode = "-9999"
            timeoutDuration=0
        }

        @JvmStatic
        fun getMerchantForCode(code: String?) = Merchant().apply {
            merchantCode = code
            terminalCode = code
            timeoutDuration = 0
        }

        @JvmStatic
        fun getRequestHeaderWithMerchantCode(merchantCode: String?) = getRequestHeader(getMerchantForCode(merchantCode))

        @JvmStatic
        fun getExtraParametersForGetAccount() = arrayListOf<ExtraParameters>().apply {
            add(ExtraParameters(key ="CALL_STEP",value = "LOGIN" ))
        }

        @JvmStatic
        fun installationId() = getString(PaycellSdkApp.context.contentResolver, Settings.Secure.ANDROID_ID)

        private fun transactionInfo() = TransactionInfo().apply {
            application = "PaycellExtSdk" //PaycellExtSdk  AndroidAPP
            mode = "TEST"
            version = "50"
            applicationVersion = "4.5.0"
            authToken = PrefUtils.authToken
            transactionDateTime = transactionDateFormat.format(Date())
            transactionId = generateTransactionId()
        }

         private fun generateTransactionId() = UUID.randomUUID().mostSignificantBits.toString().replace("-", "").padStart(19, '0')
    }
}