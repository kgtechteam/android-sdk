package com.paycell.customviews

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import com.paycellsdk.R
import com.paycell.extensions.color
import com.paycell.extensions.gone
import com.paycell.extensions.tintSrc
import com.paycell.extensions.visible
import kotlinx.android.synthetic.main.cv_toolbar.view.*

class CVToolbar @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {
    init {
        LayoutInflater.from(context).inflate(R.layout.cv_toolbar, this)
        val c = context.obtainStyledAttributes(attrs, R.styleable.CVToolbar)

        var isBackButtonVisible = c.getBoolean(R.styleable.CVToolbar_isBackButton, true)
        var isLogo = c.getBoolean(R.styleable.CVToolbar_isLogo, true)

        if (!isLogo){
            ivLogo.gone()
            tvTitle.visible()
            tvIptal.visible()
            framelayout.setBackgroundColor(context.color(R.color.white))
            iv_back.tintSrc(R.color.black)
        }
        if (isBackButtonVisible)
            iv_back.visible()
        else {
            iv_back.gone()
        }
        c.recycle()
    }

    fun backButton() = iv_back
    fun iptalButton() = tvIptal

    fun backButtonVisible(){
        iv_back.visible()
    }

    fun backButtonGone(){
        iv_back.gone()
    }
}