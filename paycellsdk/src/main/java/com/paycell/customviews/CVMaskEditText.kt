package com.paycell.customviews

import android.content.Context
import android.util.AttributeSet
import com.google.android.material.textfield.TextInputEditText
import com.paycellsdk.R

class CVMaskEditText : TextInputEditText {

    var maskTextWatcher: MaskTextWatcher? = null

    var mask: String? = null
        set(value) {
            field = value
            if (value.isNullOrEmpty()) {
                removeTextChangedListener(maskTextWatcher)
            } else {
                maskTextWatcher = MaskTextWatcher(this, mask!!)
                addTextChangedListener(maskTextWatcher)
            }
        }

    val rawText: String?
        get() {
             return maskTextWatcher?.getTextPhone()
        }

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) :
            super(context, attrs, defStyleAttr) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        attrs?.let {
            val a = context.obtainStyledAttributes(it, R.styleable.MaskEditText)
            with(a) {
                mask = getString(R.styleable.MaskEditText_mask_format)
                recycle()
            }
        }
    }
}