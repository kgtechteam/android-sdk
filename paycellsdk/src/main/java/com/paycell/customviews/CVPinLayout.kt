package com.paycell.customviews

import android.content.Context
import android.os.CountDownTimer
import android.os.Handler
import android.text.Editable
import android.text.InputType
import android.text.SpannableString
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.constraintlayout.widget.ConstraintLayout
import com.paycellsdk.R
import com.paycell.extensions.*
import kotlinx.android.synthetic.main.cv_pin_layout.view.*


class CVPinLayout : FrameLayout {

    var mContext: Context? = null
    private var constKeyboard: ConstraintLayout? = null

    private var fullPin: String? = null
    private val pinArray: ArrayList<String> = ArrayList()

    private val selectedViews: ArrayList<View> = ArrayList()
    private val unselectedViews: ArrayList<View> = ArrayList()

    private var canRemove = true
    private var canInsert = true
    var lengtg: Int = 0
    lateinit var timer: CountDownTimer
     var timerStopped:Boolean=false

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        this.mContext = context
        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        this.mContext = context
        init()
    }

    fun init() {
        LayoutInflater.from(mContext).inflate(R.layout.cv_pin_layout, this)
        constKeyboard = findViewById(R.id.clPinLayoutContainer)

        selectedViews.add(ivSelected1)
        selectedViews.add(ivSelected2)
        selectedViews.add(ivSelected3)
        selectedViews.add(ivSelected4)

        unselectedViews.add(ivUnselected1)
        unselectedViews.add(ivUnselected2)
        unselectedViews.add(ivUnselected3)
        unselectedViews.add(ivUnselected4)
        lengtg = 0
        calculate()
        insertPin("")
        insertPin("")
        insertPin("")
        insertPin("")
        defaultText()

    }

    fun defaultText() {
        tvPasswordErrorShort.text = case3Spanabble()
        tvReplyButton.text = case2Spanabble()
    }

    fun calculate() {
        editText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(it: Editable?) {
                if (it.toString().isNotEmpty()) {
                    if (lengtg > it.toString().length) {
                        removePin()
                        lengtg = it.toString().length
                    } else
                        insertPin("yok")
                    lengtg = it.toString().length
                } else if (it.toString().isNullOrEmpty() && pinArray.size == 1)
                    removePin()
            }
        })
    }

    fun getPin(): String {
        return editText.text.toString()
    }

    fun fullPinEnteredListener(isFullEntered: (Boolean) -> Unit) {
        editText.afterTextChanged {
            if (it.length == 4) {
                isFullEntered(true)
            } else {
                isFullEntered(false)
            }

        }
    }

    fun replyButtonClick(callBack: () -> Unit) {
        tvReplyButton.setOnClickListener {
            inputEnableOrDisable(true)
            resetPin()
            focus()
            callBack.invoke()
        }
    }

    fun passwordErrorShortClick(callBack: () -> Unit) {
        tvPasswordErrorShort.setOnClickListener {
            callBack.invoke()
        }
    }

    fun focus() {
        editText.requestFocus()
        editText.inputType = InputType.TYPE_CLASS_NUMBER
        context.showKeyboard(editText)
    }

    fun closeFocus() {
        editText.clearFocus()
        context.hideKeyboard(editText)
        editText.setText("")
    }


    fun insertPin(character: String): Boolean {
        if (!canInsert) {
            return false
        }
        if (pinArray.size < 4) {
            pinArray.add(character)
            scaleUpWithoutAnimation(selectedViews[pinArray.size - 1])
            scaleDownWithoutAnimation(unselectedViews[pinArray.size - 1])
            //scaleUp(centeredViews[pinArray.size - 1])

            canRemove = false
            Handler().postDelayed({ canRemove = true }, 100)
        } else {
            return false
        }
        return pinArray.size == 4
    }

    fun removePin() {
        if (!canRemove) {
            return
        }
        if (pinArray.size > 0) {

            scaleDownWithoutAnimation(selectedViews[pinArray.size - 1])
            scaleUpWithoutAnimation(unselectedViews[pinArray.size - 1])

            pinArray.removeAt(pinArray.size - 1)
            canInsert = false
            Handler().postDelayed({ canInsert = true }, 100)
        }
    }

    fun resetPin() {
        for (i in 0..3) {
            canRemove = true
            removePin()
        }
        pinArray.clear()
        editText.setText("")
        lengtg = 0
    }

    fun scaleUpWithoutAnimation(view: View) {
        view.visible()
    }

    fun scaleDownWithoutAnimation(view: View) {
        view.invisible()
    }

    fun getValidateOtpResponse(
        responseCode: String,
        displayText: String?,
        remainingTryCount: Int?
    ) {
        if (responseCode.equals("0")) {
            closeFocus()
            resetPin()
        } else if (responseCode.equals("3031") || responseCode.equals("3027")) {
            if (remainingTryCount!! > 0) { //3027
                case6()
                var text = setRemainingCountErrorText(displayText!!, remainingTryCount)
                tvError.text = text
            } else {
                 case2()
            }

        } else if (responseCode.equals("1111")) {
            //setErrorUı burda bir şey yazmayacağız
        } else if (responseCode.equals("3024")) {
            //setErrorUı burda bir şey yazmayacağız
        }
    }
    //3027 Doğrulama şifresi hatalı, [X] deneme hakkın kaldı.
    //3031  Süre doldu, tekrar giriş yapmalısın.

    fun getOtpResponse(
        responseCode: String,
        reclaimDate: String?,
        displayText: String?,
        expireDate: String?,
        responseDateTime: String?
    ) {
        tvTime.text=""
        if (responseCode.equals("0")) {
            countDownTimer(expireDate!!, responseDateTime!!)
            case1()
        }
        if (responseCode.equals("9999")) {
            case4()
        } else if (responseCode.equals("3024")) {
            var date = otpDateFormat().parse(reclaimDate)
            var displayTextParts = displayText!!.split("[X]")
            var errorMessage =
                (displayTextParts[0] + date.hours + ":" + date.minutes + displayTextParts[1]).replace(
                    "[",
                    ""
                ).replace("]", "")
            tvTime.text = errorMessage
            case5()
        }
    }

    fun countDownTimer(expireDate: String, responseDateTime: String) {
        timerStopped=false
        var expireDate = otpDateFormat().parse(expireDate)
        var responseDateTime =
            otpDateFormat().parse(responseDateTime.substring(0, 14))
        var diff = expireDate.time - responseDateTime.time
        timer = object : CountDownTimer(diff, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                if (!timerStopped){
                tvTime.text = "Kodunuz " + getTimerOtp(millisUntilFinished) + " içinde numaranıza gönderilecektir."
                if (getTimerOtp(millisUntilFinished).equals("00 : 00")) {
                    case2() // kodu tekrar gönder  case2
                }
                }

            }

            override fun onFinish() {
                timerCancel()
            }
        }
        timer.start()

    }

    fun timerCancel() {
        if (this::timer.isInitialized) {
            timer.cancel()
            timerStopped=true
        }
    }

    fun case1() {
        //Kodunuz 00:50 içinde numaranıza gönderilecektir.
        tvTime.visible()
        tvError.gone()
        tvReplyButton.gone()
        tvPasswordErrorShort.gone()
        inputEnableOrDisable(true)
        println("OtpMesaj"+tvTime.text)
    }

    fun case2() {
        //Kodu tekrar gönder.
        inputEnableOrDisable(false)
        tvTime.gone()
        tvError.gone()
        tvReplyButton.visible()
        tvPasswordErrorShort.gone()
        resetPin()
        timerCancel()
        println("OtpMesaj"+tvTime.text)
    }

    fun case3() {
        /***değişti*/
        //Süre doldu Hatalı giriş yaptınız. Kodu tekrar gönder.
        tvTime.gone()
        tvError.gone()
        tvReplyButton.gone()
        tvPasswordErrorShort.visible()
        resetPin()
    }

    fun case4() {
        //Kodu 3 kez hatalı girdiğiniz için yeni kod istemelisiniz. Kodu tekrar gönder.
        tvTime.gone()
        tvError.visible()
        tvReplyButton.visible()
        tvPasswordErrorShort.gone()
        resetPin()
        timerCancel()
        println("OtpMesaj"+tvTime.text)
    }

    fun case5() {
        //Hesabınız bloke edilmiştir. 15:00 sonrasında tekrar deneyebilirsiniz.
        inputEnableOrDisable(false)
        tvTime.visible()
        tvError.gone()
        tvReplyButton.gone()
        tvPasswordErrorShort.gone()
        timerCancel()
        println("OtpMesaj"+tvTime.text)
    }

    fun case6() {
        //displayText":"Doğrulama şifresi hatalı, [X] deneme hakkın kaldı
        tvTime.gone()
        tvError.visible()
        tvReplyButton.gone()
        tvPasswordErrorShort.gone()
        resetPin()
        timerCancel()
        println("OtpMesaj"+tvTime.text)
    }

    fun case3Spanabble(): SpannableString {
        return spannable {
            color(resources.getColor(R.color.orangeyRed), "Hatalı giriş yaptınız.") +
                    underline(
                        size(1.2f, color(resources.getColor(R.color.aqua), "Kodu tekrar gönder."))
                    )
        }
    }

    fun case2Spanabble(): SpannableString {
        return spannable {
            underline(size(1.2f, color(resources.getColor(R.color.aqua), "Kodu tekrar gönder.")))
        }
    }

    fun setRemainingCountErrorText(errorText: String, remainingCount: Int): String? {
        return errorText.replace("[X]", remainingCount.toString())
    }

    fun inputEnableOrDisable(enable: Boolean) {
        if (enable) {
            editText.inputType = InputType.TYPE_CLASS_NUMBER
            focus()
        } else {
            editText.inputType = InputType.TYPE_NULL
            closeFocus()
        }


    }

}