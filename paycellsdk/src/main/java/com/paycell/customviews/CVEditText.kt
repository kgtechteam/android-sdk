package com.paycell.customviews

import android.content.Context
import android.text.InputFilter
import android.text.InputType
import android.text.method.HideReturnsTransformationMethod
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View.OnFocusChangeListener
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.paycell.extensions.*
import com.paycellsdk.R
import kotlinx.android.synthetic.main.cv_edittext.view.*


class CVEditText @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var isPhoneNumber = false
    private var isNumberFormat = false
    private var eyeIcon = false
    private var isCard = false

    init {
        LayoutInflater.from(context).inflate(R.layout.cv_edittext, this)
        val a = context.obtainStyledAttributes(attrs, R.styleable.CVEditText)

        isPhoneNumber = a.getBoolean(R.styleable.CVEditText_isPhoneNumber, false)
        isNumberFormat = a.getBoolean(R.styleable.CVEditText_isFormatNumara, false)
        eyeIcon = a.getBoolean(R.styleable.CVEditText_eyeIcon, false)
        isCard = a.getBoolean(R.styleable.CVEditText_isCard, false)

        val maxLength = a.getInt(R.styleable.CVEditText_maxLength, 0)

        if (isPhoneNumber|| isCard) {
            etPhone.visible()   // Bu gone olmuyor
            etNormal.gone()     // Bu da visible olmuyor
        } else {
            etPhone.gone()
            etNormal.visible()
        }

        if (maxLength != 0) {
            etNormal.filters = arrayOf(InputFilter.LengthFilter(maxLength))
        }
        if (eyeIcon){
            ivEye.visible()
            ivEye.setOnClickListener {
                if (eyeIcon) {
                    eyeIcon=false
                    ivEye.setImageDrawable(context.drawable(R.drawable.ic_open_eye))
                    etNormal.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_VARIATION_PASSWORD
                    etNormal.transformationMethod = HideReturnsTransformationMethod.getInstance()
                }
                else{
                    eyeIcon=true
                    ivEye.setImageDrawable(context.drawable(R.drawable.ic_eye))
                    etNormal.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_VARIATION_PASSWORD

                }
            }
        }
        if (isPhoneNumber) {
            etPhone.mask = "5## ### ## ##"
        }
        if (isCard) {
            etPhone.mask = "##/##"
        }


        editTextListener()
        calculatePaswwordCharacterLayer()
        setHasFocus(false)
        a.recycle()
    }

    fun getText(): String {
        return if (isPhoneNumber|| isCard)
            etPhone.rawText.toString()
        else
            etNormal.text.toString()
    }

    fun setMaxlength(length:Int){
        etNormal.filters = arrayOf(InputFilter.LengthFilter(length))
    }

    fun setError(errorText: String) {
        cvEditTextFailTitleTv.visible()
        cvEditTextFailTitleTv.text = errorText
        cvEditTextUnderline.setBackgroundResource(R.color.red)
    }

    fun clearError() {
        cvEditTextFailTitleTv.text = ""
        cvEditTextFailTitleTv.gone()
        cvEditTextUnderline.setBackgroundResource(R.drawable.entry_number_activated_degrade_color)
    }
    fun clearErrorNotActive() {
        cvEditTextFailTitleTv.text = ""
        cvEditTextFailTitleTv.gone()
        getEditext().setText("")
        cvEditTextUnderline.setBackgroundResource(R.color.gray)
    }

    fun editTextListener() {
        etNormal.onFocusChangeListener = OnFocusChangeListener { v, hasFocus ->
            underLineColor(hasFocus)
        }
        etPhone.onFocusChangeListener = OnFocusChangeListener { v, hasFocus ->
            underLineColor(hasFocus)
        }
        etPhone.afterTextChanged {
            clearError()
        }
        etNormal.afterTextChanged {
            clearError()
        }
    }

    fun underLineColor(hasFocus: Boolean) {
        if (hasFocus) {
            clearError()
            cvEditTextUnderline.setBackgroundResource(R.drawable.entry_number_activated_degrade_color)
        } else {
            cvEditTextUnderline.setBackgroundResource(R.color.gray)
        }
    }

    fun setHintAndText(hint: String, buttonText: String? = null) {
        etPhoneLayout.hint = hint
        tvPhoneButton.text = buttonText
        etNormalLayout.hint = hint
        tvNormalButton.hint = buttonText
    }
    fun phoneLoginHint(hint:String){
        etPhone.hint=hint
    }

    fun listenerPhoneInput(afterTextChanged: (String) -> Unit) {
        etPhone.afterTextChanged {
            afterTextChanged.invoke(etPhone.rawText.toString())
            clearError()
        }
    }
    fun listenerCardInput(cardInputListener: CardInputListener) {

        etPhone.setText("")
        etPhone.afterTextChanged {
            clearError()
            var text = etPhone.rawText.toString()
            if (cardError(text)) {
                cardInputListener.cardValidation(text.substring(0, 2), text.substring(3, 5))
            }
            else{
                cardInputListener.cardValidationError()
            }
        }
    }
    fun listenerNormalInput(afterTextChanged: (String) -> Unit) {
        etNormal.afterTextChanged {
            afterTextChanged.invoke(it)
            clearError()
        }
    }

    fun buttonsVisibilty() {
        if (isPhoneNumber) {
            tvPhoneButton.visible()
        } else {
            tvNormalButton.visible()
        }
    }

    fun cardError(text: String): Boolean {
        var cardVarifaction: Boolean = false
        if (text.length >= 2) {
            var month = text.substring(0, 2).toInt()
            if (month > 12) {
                setError("Yanlış bir tarih girdiniz.")
                cardVarifaction = false
            }
        }
        if (text.length == 5) {
            var month = text.substring(0, 2).toInt()
            var year = text.substring(3, 5).toInt()
            if (month > 12 || year < (getCurrentYear().toString().substring(2, 4)).toInt()) {
                setError("Yanlış bir tarih girdiniz.")
                cardVarifaction = false
            } else
                cardVarifaction = true
        }
        return cardVarifaction
    }

    fun setHasFocus(focus: Boolean) {
        if (focus) {
            getEditext().requestFocus()
        } else {
            getEditext().clearFocus()
        }
    }
    fun cardValidationFocus() {
        getEditext().requestFocus()
        context.showKeyboard(getEditext())
    }

     fun getEditext(): TextInputEditText {
        if (isPhoneNumber|| isCard) {
            return etPhone
        } else {
            return etNormal
        }
    }

    fun clickNormalInput() = etNormal

    fun clickPhoneInput() = etPhone

    fun calculatePaswwordCharacterLayer() {
        if (isNumberFormat) {
            etNormal.inputType =
                InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_VARIATION_PASSWORD
        }
    }

    fun isPhoneButtonVisible(visible: Boolean) {
        if (visible) {
            tvPhoneButton.visible()
        } else {
            tvPhoneButton.gone()
        }
    }
    interface CardInputListener {
        fun cardValidation(mont: String, year: String)
        fun cardValidationError()

    }
}