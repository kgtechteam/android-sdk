package com.paycell.helper

import com.paycell.remote.model.Card

interface CardValidationAdapterCallback {
    fun cardValidation(mont:String,year:String,card:Card)
    fun cardValidationError()
}