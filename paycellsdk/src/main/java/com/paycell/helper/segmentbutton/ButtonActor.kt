package com.paycell.helper.segmentbutton

import android.content.Context
import android.view.View


/**
 * Empty, transparent view used as a "dummy" or filler view
 *
 *
 * This view is used as spacers for the SegmentedButtonGroup LinearLayout that handles the dividers
 *
 *
 * Each view has a model SegmentedButton that it mimics the width & height of. The width & height is adjusted for the
 * divider width to appropriate position the dividers in between the two buttons.
 */
class ButtonActor(context: Context) : View(context) {
    // Button to mimics size of
    private var button: SegmentedButton? = null
    // Divider width
    private var dividerWidth = 0

    fun setButton(button: SegmentedButton?) {
        this.button = button
    }

    fun setDividerWidth(width: Int) {
        dividerWidth = width
    }

    override fun onMeasure(
        widthMeasureSpec: Int,
        heightMeasureSpec: Int
    ) {
        // Desired size is the suggested minimum size
        // Resolve size based on the measure spec and go from there
        // resolveSize
        // View.onMeasure uses getDefaultSize which is similar to resolveSize except in the case of
        // MeasureSpec.AT_MOST, the maximum value will be returned. In other words, View will expand to fill the
        // available area while resolveSize will only use the desired size.
        // Width and height to set the view to
        var width: Int = 0
        var height: Int = 0

        button?.let {
            // Calculate the amount to offset the model buttons parent width by
            // (dividerWidth / 2) is subtracted from the width for each divider present
            // Buttons with neighboring buttons on both sides, this adds up to dividerWidth
            // Left-most and right-most buttons only have one divider, so this is (dividerWidth / 2)
            // And if there is EXACTLY one button being shown, no dividers are present and so the offset is 0
            val widthOffset: Int =
                if (it.isLeftButton() && it.isRightButton())
                    0
                else if (it.isLeftButton() || it.isRightButton())
                    dividerWidth / 2 else dividerWidth
            // Calculate the width & height based on the desired width & height and the measure specs
            width = resolveSize(it.measuredWidth - widthOffset, widthMeasureSpec)
            height = resolveSize(it.measuredHeight, heightMeasureSpec)
        } ?: run {
            // Fallback option to calculate the size based on suggested minimum width & height
            width = resolveSize(suggestedMinimumWidth, widthMeasureSpec)
            height = resolveSize(suggestedMinimumHeight, heightMeasureSpec)
        }

        setMeasuredDimension(width, height)
    }
}
