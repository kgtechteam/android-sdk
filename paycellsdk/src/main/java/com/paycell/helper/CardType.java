package com.paycell.helper;


import java.util.regex.Pattern;


public enum CardType {

    UNKNOWN,
    PAYCELLCARD("^462276[0-9]{10}$"),
    VISA("^4[0-9]{12}(?:[0-9]{3})?$"),
    MASTERCARD("^5[1-5][0-9]{14}$"),
    AMERICAN_EXPRESS("^3[47][0-9]{14}$"),
    MAESTRO("^(?:5[0678]\\d\\d|6304|6390|67\\d\\d)\\d{8,15}$"),
    TROY("^(9792[0-9]{2,}|36577[0-3]{1}[0-9]{0,}|650052[0-9]{0,}|654997[0-9]{0,}|657366[0-9]{0,}|657998[0-9]{0,})$");

    private Pattern pattern;

    CardType() {
        this.pattern = null;
    }

    CardType(String pattern) {
        this.pattern = Pattern.compile(pattern);
    }

    @Deprecated
    public static  CardType detect(String cardNumber) {

        for ( CardType cardType :  CardType.values()) {
            if (null == cardType.pattern) continue;
            if (cardType.pattern.matcher(cardNumber).matches()) return cardType;
        }

        return UNKNOWN;
    }

}