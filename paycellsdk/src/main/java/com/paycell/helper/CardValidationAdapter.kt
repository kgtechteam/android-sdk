package com.paycell.helper

import android.annotation.SuppressLint
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.appcompat.widget.AppCompatImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.paycell.customviews.CVEditText
import com.paycell.extensions.inflate
import com.paycell.remote.model.Card
import com.paycellsdk.R

class CardValidationAdapter(
    private val items: ArrayList<Card> = arrayListOf(),
    private val cardValidationAdapterCallback: CardValidationAdapterCallback,
    private val paycellCardBinList: ArrayList<String> = arrayListOf()
) :
    RecyclerView.Adapter<CardValidationAdapter.CreditCardViewHolder>(),
    CVEditText.CardInputListener {
    var boolean: Boolean = true
    var SELECTED_POSITION = -1
    lateinit var selectCard: Card

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CreditCardViewHolder =
        CreditCardViewHolder(parent.inflate(R.layout.item_card_validation))

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: CreditCardViewHolder, position: Int) {
        val creditCard = getItem(position)

        holder.tvCardNumber.text = StringUtils.getFilteredCardNumber(creditCard.cardNumber)

        holder.rb.setChecked(position == SELECTED_POSITION)
        if (SELECTED_POSITION!=-1) {
            opacityVisibilty(holder,position == SELECTED_POSITION)
        }
        if (position == SELECTED_POSITION) {
            cardValidationAdapterCallback.cardValidationError()
            /**diğer karta geçince buton rengi değişsin diye**/
            validationInputOpen(true, holder)
            holder.cvEtValidation.listenerCardInput(this);
            selectCard = creditCard
         } else {
            validationInputOpen(false, holder)

        }

        holder.llRoot.setOnClickListener {
            changeStatu(position, holder)
        }
        holder.rb.setOnClickListener {
            changeStatu(position, holder)
        }



        holder.cvEtValidation.setHintAndText("Kart üzerindeki son kullanma tarihini girin", "");
        holder.cvEtValidation.phoneLoginHint("aa/yy");
        val cardNumber = creditCard.cardNumber.replace('*', '0')
        when (StringUtils.detectCardType(cardNumber, paycellCardBinList)) {
            CardType.MAESTRO -> holder.ivBrand.setImageResource(R.drawable.maestroinactive)
            CardType.AMERICAN_EXPRESS -> holder.ivBrand.setImageResource(R.drawable.amexinactive)
            CardType.MASTERCARD -> holder.ivBrand.setImageResource(R.drawable.mastercardinactive)
            CardType.VISA -> holder.ivBrand.setImageResource(R.drawable.visainactive)
            else -> holder.ivBrand.setImageResource(R.drawable.ico_kart_bos_inaktif)
        }
    }

    fun changeStatu(position: Int, holder: CreditCardViewHolder) {
        if (position == SELECTED_POSITION) {
            validationInputOpen(false, holder)
            holder.rb.isChecked = false
            SELECTED_POSITION = -1
            cardValidationAdapterCallback.cardValidationError()
         } else {
             SELECTED_POSITION = position
            notifyDataSetChanged()
        }
    }

    @SuppressLint("Range")
    fun opacityVisibilty(holder: CreditCardViewHolder, opacitiy: Boolean) {
        if (opacitiy) {
            holder.tvCardNumber.alpha = 1F
            holder.rb.alpha = 1F
            holder.llRoot.alpha = 1F
            holder.ivBrand.alpha = 1F
        } else {
            holder.tvCardNumber.alpha = 0.5F
            holder.rb.alpha =  0.5F
            holder.llRoot.alpha = 0.5F
            holder.ivBrand.alpha =  0.5F
        }
    }

    fun validationInputOpen(open: Boolean, viewHolder: CreditCardViewHolder) {
        if (open) {
            viewHolder.cvEtValidation.visibility = View.VISIBLE
            viewHolder.cvEtValidation.cardValidationFocus()
        } else {
            viewHolder.cvEtValidation.visibility = View.GONE
        }
    }

    fun getItem(position: Int): Card = items[position]

    fun add(list: ArrayList<Card>, selectedPosition: Int? = null) {
        if (selectedPosition != null) {
            this.SELECTED_POSITION = selectedPosition
        }
        items.addAll(list)
        notifyDataSetChanged()
    }

    override fun cardValidation(mont: String, year: String) {
        cardValidationAdapterCallback.cardValidation(mont = mont, year = year, card = selectCard)
    }

    override fun cardValidationError() {
        cardValidationAdapterCallback.cardValidationError()
    }

    fun clear() {
        items.clear()
        notifyDataSetChanged()
    }

    class CreditCardViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvCardNumber: TextView = view.findViewById(R.id.itemCardValidation_tvCardNumber);
        val rb: AppCompatCheckBox = view.findViewById(R.id.itemCardValidation_rb);
        val llRoot: ConstraintLayout = view.findViewById(R.id.itemCardValidation_llRoot);
        val ivBrand: AppCompatImageView = view.findViewById(R.id.itemCardValidation_ivCard);
        val cvEtValidation: CVEditText = view.findViewById(R.id.cvEtValidation);


    }


}