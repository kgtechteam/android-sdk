package com.paycell.helper

import android.content.Context
import android.graphics.Typeface

/**
 * Created by Akın DEMİR on 23.10.2019.
 */
object TypeFaceCache {

    var typefaceCache: HashMap<String, Typeface> = HashMap()


    fun getFontOrGenerateOne(typefaceName: String, context: Context): Typeface? {
        var typeface: Typeface? = null
        if (typefaceCache.containsKey(typefaceName)) {
            return typefaceCache[typefaceName]
        } else {
            try {
                typeface = Typeface.createFromAsset(context.assets, "fonts/$typefaceName")
            } catch (e: Exception) {
                return typeface
            }
            typefaceCache[typefaceName] = typeface
        }
        return typeface
    }
}