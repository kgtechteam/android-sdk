package com.paycell.helper;

import android.text.TextUtils;
import android.util.Base64;

import androidx.annotation.NonNull;

import java.util.List;
import java.util.Locale;


public class StringUtils {

    private static final char[] turkishChars = new char[]{0x131, 0x130, 0xFC, 0xDC, 0xF6, 0xD6, 0x15F, 0x15E, 0xE7, 0xC7, 0x11F, 0x11E};
    private static final char[] englishChars = new char[]{'i', 'I', 'u', 'U', 'o', 'O', 's', 'S', 'c', 'C', 'g', 'G'};

    public static String join(String delimiter, int[] items) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < items.length; i++) {
            int item = items[i];
            sb.append(Integer.toString(item));
            if (i < items.length - 1) {
                sb.append(delimiter);
            }
        }
        return sb.toString();
    }

    public static Boolean nullOrEmpty(String value) {
        if (value == null) {
            return true;
        }
        return value.trim().isEmpty();
    }

    public static boolean contains(String sourceText, String searchText) {
        return  StringUtils.searchContains(sourceText, searchText);
    }

    public static String base64Encode(byte[] bytes) {
        return Base64.encodeToString(bytes, Base64.NO_WRAP);
    }

    public static byte[] base64Decode(String base64) {
        return Base64.decode(base64, Base64.NO_WRAP);
    }

    public static boolean isEmail(String email) {
        if (email == null || TextUtils.isEmpty(email)) {
            return false;
        }
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    /***
     * String.valueOf() metodundan farkı String.valueOf() metodunda null ise geriye string olarak "null" değeri döndürülürken bu metodda "" döndürülüyor.
     * @param object object
     * @return toString() hali
     */
    public static String toString(Object object) {
        if (object == null) {
            return "";
        }
        return object.toString();
    }

    public static String toString(Object object, String defaultValue) {
        if (object == null) {
            return defaultValue;
        }
        final String result = object.toString();
        return result.isEmpty() ? defaultValue : result;
    }


    public static String upperCaseAllFirst(String value) {
        if (value == null || value.isEmpty()) {
            return value;
        }
        char[] array = value.toCharArray();
        // Uppercase first letter.
        array[0] = Character.toUpperCase(array[0]);

        // Uppercase all letters that follow a whitespace character.
        for (int i = 1; i < array.length; i++) {
            if (Character.isWhitespace(array[i - 1])) {
                array[i] = Character.toUpperCase(array[i]);
            }
        }
        // Result.
        return new String(array);
    }

    public static String toTitleCase(String value) {

        if (value == null) {
            return null;
        }

        boolean space = true;
        StringBuilder builder = new StringBuilder(value);
        final int length = builder.length();

        for (int i = 0; i < length; ++i) {
            char c = builder.charAt(i);
            if (space) {
                if (!Character.isWhitespace(c)) {
                    // Convert to title case and switch out of whitespace mode.
                    builder.setCharAt(i, Character.toTitleCase(c));
                    space = false;
                }
            } else if (Character.isWhitespace(c)) {
                space = true;
            } else {
                builder.setCharAt(i, Character.toLowerCase(c));
            }
        }

        return builder.toString();
    }

    public static boolean search(String sourceText, String searchText) {
        return  StringUtils.searchContains(sourceText, searchText);
    }

    private static boolean searchContains(String sourceText, String searchText) {
        if (sourceText == null || searchText == null) {
            return false;
        }

        sourceText = sourceText.toLowerCase();
        searchText = searchText.toLowerCase();

        return clearTurkishChars(sourceText).contains(clearTurkishChars(searchText));
    }

    private static String clearTurkishChars(String str) {
        String ret = str;
        int length = turkishChars.length;
        for (int i = 0; i < length; i++) {
            ret = ret.replace(turkishChars[i], englishChars[i]);
        }
        return ret;
    }

    public static String toUpperCase(String s) {
        return TextUtils.isEmpty(s) ? s : s.toUpperCase(new Locale("tr", "TR"));
    }

    public static String normalize(String regex) {
        if (TextUtils.isEmpty(regex)) {
            return regex;
        }
        return regex.replace("\\\\","\\");
    }

    public static boolean matches(String string, String regex) {
        if (TextUtils.isEmpty(string)) {
            return false;
        }
        return string.matches(normalize(regex));
    }

    public static CharSequence nullToEmpty(String nullableString) {
        return nullableString == null ? "" : nullableString;
    }

    public static boolean isBlank(String s){
        return s.trim().isEmpty();
    }
    public static String limitString(String value, int length) {
        StringBuilder buffer = new StringBuilder(value);
        if (buffer.length() > length) {
            buffer.setLength(length);
            buffer.append("...");
        }

        return buffer.toString();
    }
    public static  String getFilteredCardNumber(String cardNo) {
        try {
            return cardNo.substring(0, 4) + " " +
                    cardNo.substring(4, 8) + " " +
                    cardNo.substring(8, 12) + " " +
                    cardNo.substring(12, cardNo.length());
        } catch (Exception ex) {
            return cardNo;
        }
    }

    public static CardType detectCardType(String cardNumber, @NonNull List<String> paycellCardAcceptanceList) {
        if (TextUtils.isEmpty(cardNumber)) {
            return CardType.UNKNOWN;
        }
        //noinspection ConstantConditions
        if (paycellCardAcceptanceList != null) {
            for (String cardAcceptance : paycellCardAcceptanceList) {
                if (cardNumber.startsWith(cardAcceptance)) {
                    return CardType.PAYCELLCARD;
                }
            }
        }
        return CardType.detect(cardNumber);
    }
}
