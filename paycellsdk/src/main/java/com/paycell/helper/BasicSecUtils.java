package com.paycell.helper;

/**
 * Created by TCRTOPUZOGLU on 5.01.2018.
 */

public class BasicSecUtils {
    private static final  BasicSecUtils secUtil = new  BasicSecUtils();

    private BasicSecUtils() {

    }
    public static  BasicSecUtils getInstance() {
        return secUtil;
    }

    public String encrypt(String str) {
        String hexed = convertStringToHex(str);
        return new StringBuffer(hexed).reverse().toString();
    }

    public String decrypt(String str) {
        String reversed =  new StringBuffer(str).reverse().toString();
        return convertHexToString(reversed);

    }

    public String convertStringToHex(String str){

        char[] chars = str.toCharArray();

        StringBuffer hex = new StringBuffer();
        for(int i = 0; i < chars.length; i++){
            hex.append(Integer.toHexString((int)chars[i]));
        }

        return hex.toString();
    }

    public String convertHexToString(String hex){

        StringBuilder sb = new StringBuilder();
        StringBuilder temp = new StringBuilder();

        //49204c6f7665204a617661 split into two characters 49, 20, 4c...
        for( int i=0; i<hex.length()-1; i+=2 ){

            //grab the hex in pairs
            String output = hex.substring(i, (i + 2));
            //convert hex to decimal
            int decimal = Integer.parseInt(output, 16);
            //convert the decimal to character
            sb.append((char)decimal);

            temp.append(decimal);
        }
        System.out.println("Decimal : " + temp.toString());

        return sb.toString();
    }
    public String toHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = bytes.length - 1; i >= 0; --i) {
            int b = bytes[i] & 0xff;
            if (b < 0x10)
                sb.append('0');
            sb.append(Integer.toHexString(b));
            if (i > 0) {
                sb.append(" ");
            }
        }
        return sb.toString();
    }

}
