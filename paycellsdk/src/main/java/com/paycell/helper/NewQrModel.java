package com.paycell.helper;

// paycellapp://deeplink?isQr=YES&qrInfo=2000&accountId=306605
// paycellapp://deeplink?isQr=YES&qrInfo=2000&accountId=306605
//https://paycell.com.tr/deeplink?isQr=YES&qrInfo=2000&accountId=306605
public class NewQrModel {
    String isQr;
    String qrInfo;
    String accountId;

    String msisdn; // accountInfo'ya karşılık gelen local alan.

    public NewQrModel(String isQr, String qrInfo, String accountId) {
        this.isQr = isQr;
        this.qrInfo = qrInfo;
        this.accountId = accountId;
    }

    public String getIsQr() {
        return isQr;
    }

    public void setIsQr(String isQr) {
        this.isQr = isQr;
    }

    public String getQrInfo() {
        return qrInfo;
    }

    public void setQrInfo(String qrInfo) {
        this.qrInfo = qrInfo;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }
}