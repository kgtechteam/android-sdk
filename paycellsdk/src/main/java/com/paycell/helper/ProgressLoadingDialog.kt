package com.paycell.helper

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.airbnb.lottie.LottieAnimationView
import com.paycellsdk.R

class ProgressLoadingDialog(model: ProgreessModel, callback: (() -> Unit?)? =null) : Dialog(model.context), LifecycleObserver {

    data class ProgreessModel(
        var context: Context,
        var isProgress: Boolean,
        var title: String? = null,
        var description: String? = null,
        var buttonText: String? = null
    )
    init {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        if (model.isProgress) {
            setCancelable(true)
            setContentView(R.layout.dialog_progress_loading)
            val btnDecline: LottieAnimationView = findViewById(R.id.progressBar)
        } else {
            setContentView(R.layout.dialog_progress_message)
            val title: TextView = findViewById(R.id.descriptionSuspendedWarningTv)
            title.text = model.title
            val close: ImageView = findViewById(R.id.closeBtnSuspendedWarningIv)
            val ok: AppCompatButton = findViewById(R.id.renewPasswordBtnSuspendedWarning)

            model.buttonText?.let {
                ok.text = it
            }

            close.setOnClickListener {
                dismiss()
                callback?.let {
                    it.invoke()
                }
            }
            ok.setOnClickListener {
                dismiss()
                callback?.let {
                    it.invoke()
                }
            }
        }
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(window?.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        window?.attributes = lp
        show()
    }

    fun toggle(status: Boolean = true) {
        if (status) {
            if (!isShowing) {
                show()
            }
        } else {
            cancel()
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun release() {
        cancel()
        dismiss()
    }
}