package com.paycell.helper.qrcode;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.SOURCE;

@Retention(SOURCE)
@IntDef({
        PaycellMerchant.MERCHANT_ID_SHELL,
        PaycellMerchant.MERCHANT_ID_BILL_TURKCELL_INVOICE,
        PaycellMerchant.MERCHANT_ID_BILL_SUPERONLINE_INVOICE,
        PaycellMerchant.MERCHANT_ID_FINANCELL,
        PaycellMerchant.MERCHANT_ID_PREPAID,
        PaycellMerchant.MERCHANT_ID_ISTANBUL_KART,
        PaycellMerchant.MERCHANT_ID_PAYCELL_TOP_UP,
        PaycellMerchant.MERCHANT_ID_ADD_ALLOWENCE_CARD,
        PaycellMerchant.MERCHANT_ID_TURKCELL_PAYMENT_ORDER,
        PaycellMerchant.MERCHANT_ID_AYTEMIZ,
        PaycellMerchant.MERCHANT_ID_ACTIVATE_CARD,
        PaycellMerchant.MERCHANT_ID_ADD_CREDIT_BANK_CARD,
        PaycellMerchant.MERCHANT_ID_GENERATE_VIRTUAL_CARD,
        PaycellMerchant.MERCHANT_ID_SGK,
        PaycellMerchant.MERCHANT_ID_REQUEST_ALLOWANCE,
        PaycellMerchant.MERCHANT_ID_QUERY_QR_DETAIL,
        PaycellMerchant.MERCHANT_ID_CREATE_FACE_INFO,
        PaycellMerchant.MONEY_TRANSFER_APP_MERCHANT_ID,
        PaycellMerchant.FINANCELL_SHOW_CREDITS_MERCHANT_ID
})
public @interface PaycellMerchant {
    int MERCHANT_ID_SHELL = 13;
    int MERCHANT_ID_BILL_TURKCELL_INVOICE = 14;
    int MERCHANT_ID_BILL_SUPERONLINE_INVOICE = 332;
    int MERCHANT_ID_FINANCELL = 19;
    int MERCHANT_ID_PREPAID = 25;
    int MERCHANT_ID_ISTANBUL_KART = 273;
    int MERCHANT_ID_PAYCELL_TOP_UP = 341;
    int MERCHANT_ID_ACTIVATE_CARD = 339;
    int MERCHANT_ID_ADD_CREDIT_BANK_CARD = 347;
    int MERCHANT_ID_GENERATE_VIRTUAL_CARD = 349;
    int MERCHANT_ID_ADD_ALLOWENCE_CARD = 344;
    int MERCHANT_ID_TURKCELL_PAYMENT_ORDER = 348;
    int MERCHANT_ID_AYTEMIZ = 345;
    int MERCHANT_ID_SGK = 350;
    int MERCHANT_ID_REQUEST_ALLOWANCE = 30;
    int MERCHANT_ID_QUERY_QR_DETAIL = 360;
    int MERCHANT_ID_ISKI = 1;
    int MERCHANT_ID_IGDAS = 44;
    int MERCHANT_ID_CREATE_FACE_INFO = 361;
    int MONEY_TRANSFER_APP_MERCHANT_ID = 366;
    int MERCHANT_ID_ADD_PAYE_CARD = 377;
    int MERCHANT_ID_ADD_IBAN = 380;
    int FINANCELL_SHOW_CREDITS_MERCHANT_ID = 383;
}



