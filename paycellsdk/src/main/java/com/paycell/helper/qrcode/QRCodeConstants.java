package com.paycell.helper.qrcode;

public class QRCodeConstants {
    public static final int PERMISSIONS_REQUEST_ACCESS_CAMERA = 0;
    public static final int QR_REQUEST_CODE = 54;
    public static final String BARCODE_TEXT_KEY = "BARCODE_TEXT_KEY";
    public static final int ISTANBULKART_GET_PAYMENT_METHOD_TYPE = 1;
    public static final int AYTEMIZ_GET_PAYMENT_METHOD_TYPE = 2;
    public static final int FACE_PAYMENT_GET_PAYMENT_METHOD_TYPE = 3;
}
