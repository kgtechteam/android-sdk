package com.paycell.helper.qrcode

 import java.util.*


class KeyManager {
    enum class Key(val default: Any? = null) {
        PAYCELL_OIL_RETRY_TIME(0),
        PAYCELL_OIL_START_TRANSACTION(false),
        KYC_MUST_LEGAL(false),
        KYC_AGENT_START_DATE(""),
        KYC_AGENT_END_DATE(""),
        QR_TRANSACTION_CODE_PREFIX(""),
        QUICK_CODE_AVAILABLE(false),
        QUICK_CODE_CHECK_TIME(0),
        REQUEST_MONEY_LINK_TYPE_FIREBASE(true),
        LOYALTY_TAB_CLOSED(false),
        LOYALTY_WHITE_LIST(""),
        KYC_OPEN_SPEECH_TO_TEXT(false),
        PREVIOUS_PAYMENTS(false),
        FACE_QR_CODE_PREFIX(""),
        FACE_ID_MENU(false),
        NETFLIX_LOGO(""),
        NETFLIX_APPLICATION_URL(""),
        NETFLIX_FORGOT_PASSWORD_URL(""),
        LOYALTY_DONATION_CATEGORY_ID(listOf("6000")),
        QR_OFFER_CATALOG_ID,
        PAYCELL_ISTANBULKART_INSTRUCTION_BUTTON(false),
        FACE_ID_MENU_SERVICE_CALL(true),
        CPGW_TURKCELL_CORP_CODE(0),
        CPGW_SUPERONLINE_CORP_CODE(481),
        CPGW_ISTANBULKART_CORP_CODE(128004),
        CPGW_FINANCELL_CORP_CODE(1),
        SUCCESS_RATE_POPUP_OPEN(true),
        AYTEMIZ_OIL_CODE_PREFIX(""),
        GET_QR_DETAIL_ERROR_CODES("4088"),
        ACCOUNT_MERCHANT_ID(375),
        WALLET_ERROR_ICON,
        WALLET_SINGLE_ACCOUNT_NO_DATA,
        WALLET_SINGLE_ACCOUNT_TRANSACTION_COUNT,
        BE_PM_SORT(false),
        DIRECTIVE_AREA_MAX_OFFER_COUNT(5),
        HOME_PAGE_TRANSACTIONS_MAX_COUNT(5),
        WALLET_CARD_NO_DATA_ICON,
        WALLET_LOADING_ICON,
        WP_URL_ANDROID("whatsapp://send?phone=+90[X]&text=[Y]"),
        BIP_URL_ANDROID("bip://send?text=[Y]"),
        RECHARGE_PACKAGE_POSTPAID_SELECTION(false),
        PREMIUM_DCB_QR_INSERT(false),
        DCB_TOPUP_APP_MERCHANT_ID(366),
        SHOW_DCB_TOPUP(true),
        MT_EXTERNAL_APP_MERCHANT(381),
        MT_EXTERNAL_READY_MESSAGES("Borcum Vardı; Kahveler Benden ; Teşekkürler!"),
        NEW_REQUEST_ALLOWENCE_FLOW(true),
        MT_EXTERNAL_3D_APP_MERCHANT(25),
        COINY_USER_AGGREMENT("https://coiny.io/user-agreement"),
        COINY_COMMERCIAL_AGGREMENT(""),
        IBAN_ALL_BALANCE_OPEN(false),
        LAST_DAY_COUNTER_CONTROL(0),
        PAYMENT_METHOD_SELECTION_NEW_CARD_ICON(""),
        WALLET_IBAN_NO_DATA_ICON,
        HIDE_MARKET_PLACE_UI(false),
        MARKETPLACE_CARDS_OPEN(false),
        CLOSED_ANDROID4_CATEGORIES("36;40"),
        CLOSED_ANDROID4_APPMERCHANTS("369"),
        REGISTERED_PCARD_NEW_APPLICATION_NO_DATA_ICON(""),
        REGISTERED_PCARD_TRACKING_NO_DATA_ICON(""),
        REGISTERED_PCARD_TRACKING_COURIER_URL(""),
        REGISTERED_PCARD_NEW_APPLICATION_CARD_IMAGE(""),
        REGISTERED_PCARD_NEW_APPLICATION_DEFAULT_CARD_NO("**** **** **** 0000"),
        PAYMENT_METHOD_SELECTION_NO_DATA_ICON,
        REGISTERED_PCARD_NEW_APPLICATION_ADDRESS_TYPE(0),
        REGISTERED_PCARD_NEW_APPLICATION_MERCHANT_ID(385),
        BASARSOFT_COUNTRY_CODE("0"),
        ADDRESS_SELECTION_ICON,
        PCARD_DISTANCE_SALES_AGGREMENT_TYPE(""),
        ORDER_ADDRESS_TYPE(0),
        ADDRESS_SELECTION_NEW_ADDRESS(""),
        REFERRAL_ACTIVATE_OFFER("REFERRAL_ACTIVATE_OFFER"),
        IS_CARD_ORDER_OPEN(true),
        REFERRAL_INVITE_MESSAGE("REFERRAL_INVITE_MESSAGE"),
        AUTOFILL_OTP_KEY_LOGIN("kod"),
        AUTOFILL_OTP_KEY_FRAUD("şifreniz"),
        DCB_PREMIUM_AGGREMENT_CONDITIONS(""),
        LOTTERY_POPUP_BUTTON_URL("https://paycell.com.tr/deeplink?screenId=19916&screenIdNew=19916&isPresenting=1"),
        PAYCELL_LOTTERY_CODE(""),
        MARKETPLACE_USER_CONTROL(368),
        PAYCELL_LOTTERY_CAMPAIGNID(""),
        MP_APP_MERCHANT(""),
        MP_BK_AGGREMENT_TYPE(""),
        BK_ADD_CARD(true);
    }

    companion object {
        private val keys = HashMap<String, List<String>>()

        @JvmStatic
        fun init(tpayParam: ArrayList<TpayParam>?) = tpayParam?.map { it.name to it.valueList }?.let { keys.putAll(it) }

        @JvmStatic
        fun setInt(key: Key, value:Integer) {
            keys[key.name] = listOf(value.toString())
        }
        @JvmStatic
        fun get(key: Key) = (keys[key.name]?.get(0) ?: key.default).toStringOrEmpty()

        @JvmStatic
        fun getBool(key: Key) = get(
            key
        ).toBoolean()

        @JvmStatic
        fun getInt(key: Key) = get(
            key
        ).toIntOrNull()
                ?: key.default?.takeIf { it is Int }?.let { return it as Int }

        @JvmStatic
        fun getList(key: Key) = keys.get(key.name)

    }
}
fun Any?.toStringOrEmpty() = this?.toString() ?: ""
