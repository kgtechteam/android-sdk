package com.paycell.helper.qrcode;

import java.util.ArrayList;

public class TpayParam {
    private long id;
    private String name;
    private ArrayList<String> valueList;

    public TpayParam() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getValueList() {
        return valueList;
    }

    public void setValueList(ArrayList<String> valueList) {
        this.valueList = valueList;
    }
}
