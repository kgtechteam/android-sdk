package com.paycell.helper.qrcode;

public class Nat {

    public static String invokeNativeFunction(int code) {

        if (code == 0) {
            return "32262a48363362447264782377437937";
        } else if (code == 1) {
            return "3636363636";
        } else if (code == 2) {
            return "ikpcyoiu3ny8";
        } else if (code == 3) {
            return "q1234433362447264782377437df4";
        } else if (code == 4) {
            return "45512ws4433362447264782377437937";
        }
        else if (code == 5) {
            return "GF3e2312fdf32f3f3333f";
        }
        else if (code == 6) {
            return "g123213121312447264782377437937";
        }
        else if (code == 7) {
            return "Fdw3joj12323*0123+";
        }
        else if (code == 8) {
            return "455223442123123ff264782377437937";
        }
        else if (code == 9) {
            return "aafdsafsafsa89a78d675699999";
        }
        else if (code == 10) {
            return "11sdfds4552234433362447264782377437937";
        }
        return "";
    }

    public static String invokeNativeFunctionP(int code) {
        if (code == 0) {
            return "5280F2491F6BB6232A24A6AD2A21B3D0";
        } else if (code == 1) {
            return "2980F4491F6BB6232A3D86AD2A29B3D0";
        } else if (code == 3) {
            return "45580F12331F6fg6232A3D86AD2A29B3g0";
        } else if (code == 4) {
            return "45580F12331F6fg3432A3D86AD2A29B3g0";
        }
        else if (code == 5) {
            return "45580F12331F6fg3232A3D86AD2A29B6g0";
        }
        else if (code == 6) {
            return "FGs80F12331F6fg6232A3D86AD2A29B3g0";
        }
        else if (code == 7) {
            return "45580F12331F!26fg6232A3D86AD2A29B3g0";
        }
        else if (code == 8) {
            return "123580F12331F6fg6232A3D86AD2A29B3g0";
        }
        else if (code == 9) {
            return "43F580F12331F6fg6232A3D86AD2A29B3g0";
        }
        else if (code == 10) {
            return "fF980F12331F6BB6232A3D86AD2A29B3D0";
        } else if (code == 23) {
            return "a4BNmms7LbkvFCHrIFUFel2TiLFMrCjB";
        } else if (code == 34) {
            return "45580F12331F6fg3432A3D86AD2A29B3g0";
        }
        else if (code == 25) {
            return "45580F12331F6fg3232A3D86AD2A29B6g0";
        }
        else if (code == 26) {
            return "HgBwImDnTR9Pl5zbKNaEi0puhJ0w6tFQ";
        }
        else if (code == 27) {
            return "45580F12331F!26fg6232A3D86AD2A29B3g0";
        }
        else if (code == 28) {
            return "4zKUjTFvwpvIugDsA7HcZXucwRhzBecr";
        }
        else if (code == 29) {
            return "fY5t7IpiXbYs1wyReFKBqY425fdWgsy7";
        }
        else if (code == 210) {
            return "fY5t7IpiXbYs1wyReFKBqY425fdWgsy7!asd2";
        }
        return "";
    }

    public static String invokeNativeFunctionC(int code) {
        if (code == 0) {
            return "123BF4491BCBB6232A3D86AD1B29B3D1";
        } else if (code == 1) {
            return "65254ae7ad8f9ffd";
        } else if (code == 2) {
            return "412B123BDACBB6232A3D86A1298B46A2";
        } else if (code == 3) {
            return "16341ea3da9d2ddf";
        }
        return  "";
    }
}
