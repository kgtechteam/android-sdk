package com.paycell.helper

import java.util.regex.Pattern

class PassCodeHelper {
    private val weakPinRegex1 = "^.*?(?:0123|1234|2345|3456|4567|5678|6789|7890).*$"
    private val weakPinRegex2 = "^.*?(?:0987|9876|8765|7654|6543|5432|4321|3210).*$"
    private val weakPinRegex3 = "^.*?(\\d)\\1{2}.*$"

    companion object PassCodeHelper{
        private val weakPinRegex1 = "^.*?(?:0123|1234|2345|3456|4567|5678|6789|7890).*$"
        private val weakPinRegex2 = "^.*?(?:0987|9876|8765|7654|6543|5432|4321|3210).*$"
        private val weakPinRegex3 = "^.*?(\\d)\\1{2}.*$"

        fun isPassCodeValidWithWeakPin(passCode:String):Boolean {
            val pattern1 = Pattern.compile(weakPinRegex1)
            val pattern2 = Pattern.compile(weakPinRegex2)
            val pattern3 = Pattern.compile(weakPinRegex3)
            val matcher1 = pattern1.matcher(passCode)
            val matcher2 = pattern2.matcher(passCode)
            val matcher3 = pattern3.matcher(passCode)
            return !(matcher1.find() || matcher2.find() || matcher3.find())
        }    }

     fun isPassCodeValidWithWeakPin(passCode:String):Boolean {
        val pattern1 = Pattern.compile(weakPinRegex1)
        val pattern2 = Pattern.compile(weakPinRegex2)
        val pattern3 = Pattern.compile(weakPinRegex3)
        val matcher1 = pattern1.matcher(passCode)
        val matcher2 = pattern2.matcher(passCode)
        val matcher3 = pattern3.matcher(passCode)
        return !(matcher1.find() || matcher2.find() || matcher3.find())
    }

    /* Paycell card şifre işlemleri için bir kontrol yok
     o nedenle always true.
     */
    fun isPasscodeValid(passcode:String):Boolean {
        return true
    }

    /* Uygulama şifresi olarak kullanılan deprecated metod.
     */
    @Deprecated("")
    fun isPasscodeValidOld(passcode:String):Boolean {
        /**
         * Tüm karakterler aynı mı kontrolü
         */
        val char1 = passcode.get(0)
        val char2 = passcode.get(1)
        val char3 = passcode.get(2)
        val char4 = passcode.get(3)
        if (char1 == char2 && char1 == char3 && char1 == char4)
        {
            return false
        }

        /**
         * Karakterler artan ardışık mı kontrolü
         */
        val int1 = Character.getNumericValue(char1)
        val int2 = Character.getNumericValue(char2)
        val int3 = Character.getNumericValue(char3)
        val int4 = Character.getNumericValue(char4)
        val intArray = intArrayOf(int1, int2, int3, int4)
        var count = 0
        for (i in 0..2)
        {
            if (intArray[i] + 1 == intArray[i + 1])
            {
                count++
            }
        }
        if (count == 3)
            return false

        /**
         * Karakterler azalan ardışık mı kontrolü
         */
        count = 0
        for (i in 3 downTo 1)
        {
            if (intArray[i] == intArray[i - 1] - 1)
            {
                count++
            }
        }
        if (count == 3)
            return false
        return true
    }
}