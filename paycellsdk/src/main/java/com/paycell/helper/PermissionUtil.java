package com.paycell.helper;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;

/**
 * Created by ServetCanAsutay on 27/03/17.
 */

public class PermissionUtil {

    public static boolean hasPermission(Context context, String permission) {

        return ActivityCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean hasAccessFineLocationPermission(Context context) {

        return hasPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
    }
}
